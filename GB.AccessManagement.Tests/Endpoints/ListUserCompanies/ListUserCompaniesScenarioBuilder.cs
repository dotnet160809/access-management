using GB.AccessManagement.Tests.Extensions;
using Microsoft.Extensions.DependencyInjection;

namespace GB.AccessManagement.Tests.Endpoints.ListUserCompanies;

public sealed class ListUserCompaniesScenarioBuilder : AbstractEndpointScenarioBuilder<ListUserCompaniesScenarioBuilder>
{
    public ListUserCompaniesScenario Build(int apiVersion, Guid userId)
    {
        _ = OpenFga()
            .CheckReturns(CompanyOwnerOrMembers)
            .ExpandReturns([])
            .ListObjectsReturns(UserCompanies(userId));

        return new ListUserCompaniesScenario(CreateClient(), apiVersion);
    }

    protected override void ConfigureServices(IServiceCollection services)
    {
        _ = services
            .StubAuthentication()
            .AddCompanyFixture()
            .StubOpenFga();
    }
}