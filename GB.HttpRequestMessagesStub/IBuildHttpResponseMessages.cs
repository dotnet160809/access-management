using System.Net;
using System.Net.Http.Json;

namespace GB.HttpRequestMessagesStub;

public delegate Task<HttpResponseMessage> HttpResponseMessageDelegate(HttpRequestMessage request);

/// <summary>
/// Build responses to stub http requests
/// </summary>
public interface IBuildHttpResponseMessages
{
    /// <summary>
    /// Return an empty http response with a 200 <see cref="HttpStatusCode.OK"/> status code
    /// </summary>
    /// <returns>An <see cref="IStubHttpRequestMessages"/> implementation to keep stubbing http requests</returns>
    IStubHttpRequestMessages With200OkResponse()
    {
        return WithResponse(_ =>
        {
            var content = new StringContent(string.Empty);
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = content
            };

            return Task.FromResult(response);
        });
    }

    /// <summary>
    /// Return the provided content with a 200 <see cref="HttpStatusCode.OK"/> status code
    /// </summary>
    /// <remarks>
    /// The provided content is serialized to JSON
    /// </remarks>
    /// <param name="content">The content to return within the response</param>
    /// <typeparam name="TContent">The content type to serialize to JSON</typeparam>
    /// <returns>An <see cref="IStubHttpRequestMessages"/> implementation to keep stubbing http requests</returns>
    IStubHttpRequestMessages With200OkResponse<TContent>(TContent content)
    {
        return WithResponse(_ =>
        {
            var jsonContent = JsonContent.Create(content);
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = jsonContent
            };

            return Task.FromResult(response);
        });
    }

    /// <summary>
    /// Execute the provided delegate to return a response
    /// </summary>
    /// <param name="function">The delegate to execute when returning a response</param>
    /// <returns>An <see cref="IStubHttpRequestMessages"/> implementation to keep stubbing http requests</returns>
    IStubHttpRequestMessages WithResponse(HttpResponseMessageDelegate function);
}