using GB.AccessManagement.Domain.Companies.Events;
using GB.AccessManagement.Domain.Companies.Messages;
using GB.AccessManagement.Domain.Messages;

namespace GB.AccessManagement.Application.Companies.DeleteCompany;

public sealed class CompanyDeletedEventHandler : IDomainEventHandler<CompanyDeletedEvent>
{
    private readonly IPublishMessage _publisher;

    public CompanyDeletedEventHandler(IPublishMessage publisher)
    {
        _publisher = publisher;
    }

    public Task Handle(CompanyDeletedEvent notification, CancellationToken cancellationToken)
    {
        return _publisher.Publish<ICompanyDeletedMessage>(new CompanyDeletedMessage((Guid)notification.Id), cancellationToken);
    }
}