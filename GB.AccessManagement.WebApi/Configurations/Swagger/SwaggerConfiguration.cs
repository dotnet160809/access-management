namespace GB.AccessManagement.WebApi.Configurations.Swagger;

public static class SwaggerConfiguration
{
    public static IServiceCollection ConfigureSwagger(this IServiceCollection services)
    {
        return services.AddSwaggerGen();
    }
}