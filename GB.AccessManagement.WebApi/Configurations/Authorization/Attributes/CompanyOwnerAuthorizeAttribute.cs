using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.WebApi.Configurations.Authorization.Attributes;

public sealed class CompanyOwnerAuthorizeAttribute : CompanyAccessAuthorizeAttribute
{
    private static readonly CompanyRelation Relation = CompanyRelation.Owner;
    
    public CompanyOwnerAuthorizeAttribute(string routeParameterName) : base(routeParameterName, Relation) { }
}