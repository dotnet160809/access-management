using GB.AccessManagement.Infrastructure.Database;
using GB.AccessManagement.Infrastructure.OpenFga;
using GB.AccessManagement.Tests.Extensions;
using GB.AccessManagement.WebApi;
using MassTransit;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;

namespace GB.AccessManagement.Tests.Fixtures;

public sealed class WebApiFixture : WebApplicationFactory<Startup>
{
    private readonly Action<IServiceCollection> _configureServices;

    public WebApiFixture(Action<IServiceCollection> configureServices)
    {
        _configureServices = configureServices;
    }

    protected override void ConfigureWebHost(IWebHostBuilder builder)
    {
        builder
            .UseSetting("POSTGRES_CONNECTION_STRING", string.Empty)
            .UseSetting("MESSAGE_QUEUE_PASSWORD", "SuperSafePassword")
            .UseSetting("MESSAGE_QUEUE_AUTHENTICATION_KEY", "AnotherSuperSafeKey")
            .ConfigureTestServices(WithRequiredServices(_configureServices));
    }

    private static Action<IServiceCollection> WithRequiredServices(Action<IServiceCollection> configureServices)
    {
        var databaseName = Guid.NewGuid().ToString("N");

        return services =>
        {
            _ = services
                .RemoveAll<IHostedService>()
                .RemoveAll<DbContextOptions<CompanyDbContext>>()
                .RemoveAll<IDbContextOptionsConfiguration<CompanyDbContext>>()
                .AddDbContext<CompanyDbContext>(InMemory(databaseName), ServiceLifetime.Scoped, ServiceLifetime.Singleton)
                .AddMassTransitTestHarness(InMemory)
                .Configure<OpenFgaOptions>(WithDummyValues)
                .AddFixture<TestOutputHelperExceptionPublisher>();
            configureServices(services);
        };
    }

    private static Action<DbContextOptionsBuilder> InMemory(string databaseName)
    {
        return builder => builder
            .UseInMemoryDatabase(databaseName)
            .ConfigureWarnings(warnings => warnings.Ignore(InMemoryEventId.TransactionIgnoredWarning));
    }

    private static void InMemory(IBusRegistrationConfigurator configurator)
    {
        _ = configurator
            .AddOptions<MassTransitHostOptions>()
            .Configure(options => options.WaitUntilStarted = false);
        configurator.UsingInMemory((context, rabbitmq) =>
        {
            rabbitmq.ConfigureEndpoints(context);
            rabbitmq.ConcurrentMessageLimit = 1;
        });
    }

    private static void WithDummyValues(OpenFgaOptions options)
    {
        options.BaseAddress = "http://openfga";
        options.StoreId = "store";
    }
}