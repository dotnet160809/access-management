namespace GB.AccessManagement.Infrastructure.Database;

public interface ICountCompanies
{
    void WhenCreated();

    void WhenUpdated();

    void WhenDeleted();
}