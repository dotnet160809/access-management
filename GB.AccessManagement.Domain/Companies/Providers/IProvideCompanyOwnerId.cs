using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Domain.Companies.Providers;

public interface IProvideCompanyOwnerId
{
    Task<UserId> GetOwnerForCompany(CompanyId companyId);
}