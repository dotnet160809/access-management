namespace GB.AccessManagement.Tests.Endpoints.ListUserCompanies;

public sealed class ListUserCompaniesScenario(HttpClient client, int apiVersion)
{
    public Task<HttpResponseMessage> Execute(Guid userId)
    {
        return client.GetAsync($"/v{apiVersion}/users/{userId}/companies");
    }
}