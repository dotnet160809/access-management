namespace GB.AccessManagement.WebApi.Controllers.Attributes;

public static class SupportedContentTypes
{
    private const string DefaultCharset = "charset=utf-8";

    public const string Json = $"application/json; {DefaultCharset}";

    public const string ProblemDetails = $"application/problem+json; {DefaultCharset}";

    public const string HalJson = $"application/hal+json; {DefaultCharset}";
}