using GB.AccessManagement.Application.Commands;

namespace GB.AccessManagement.Application.Companies.AddCompanyChild;

public sealed class AddCompanyChildCommandHandler(ICompanyStore store) : ICommandHandler<AddCompanyChildCommand>
{
    public async Task Handle(AddCompanyChildCommand request, CancellationToken cancellationToken)
    {
        var company = await store.Load(request.ChildCompanyId);
        company.AddAsChildToParent(request.ParentCompanyId);
        await store.Save(company, cancellationToken);
    }
}