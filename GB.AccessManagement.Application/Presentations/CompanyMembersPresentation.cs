using Swashbuckle.AspNetCore.Annotations;

namespace GB.AccessManagement.Application.Presentations;

[Serializable]
[SwaggerSchema(Title = nameof(CompanyMembersPresentation))]
public sealed record CompanyMembersPresentation : PaginatedHalPresentation<CompanyMembersPresentation, CompanyMemberPresentation>
{
    public CompanyMembersPresentation(CompanyMemberPresentation[] items, int total) : base(items, total)
    {
    }
}