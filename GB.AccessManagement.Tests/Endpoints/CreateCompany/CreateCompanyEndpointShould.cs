using System.Net;
using GB.AccessManagement.Domain.Companies.Messages;
using GB.AccessManagement.Infrastructure.Database;
using GB.AccessManagement.Infrastructure.MassTransit;
using GB.AccessManagement.Tests.Assertions;
using GB.AccessManagement.Tests.Extensions;
using MassTransit.Testing;
using Microsoft.Net.Http.Headers;
using NFluent;
using NFluent.Json;
using NSubstitute;
using Xunit.Abstractions;

namespace GB.AccessManagement.Tests.Endpoints.CreateCompany;

public sealed class CreateCompanyEndpointShould : AbstractFeatureShould<CreateCompanyScenarioBuilder>
{
    public CreateCompanyEndpointShould(ITestOutputHelper outputHelper, CreateCompanyScenarioBuilder builder)
        : base(outputHelper, builder)
    {
    }

    [Theory]
    [InlineData(1)]
    public async Task ReturnUnauthorizedWhenUserIsNotAuthenticated(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder.Build(apiVersion);

        // Act
        var response = await scenario.Execute("dummy");

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.Unauthorized);
    }

    [Theory]
    [InlineData(1, null)]
    [InlineData(1, "")]
    [InlineData(1, " ")]
    public async Task ReturnBadRequestWhenProvidingInvalidRequest(int apiVersion, string? companyName)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateUser(out var user)
            .AuthenticateUser(user)
            .Build(apiVersion);

        // Act
        var response = await scenario.Execute(companyName);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.BadRequest);
    }

    [Theory]
    [InlineData(1)]
    public async Task ReturnCreatedWhenCompanyHasBeenSuccessfullyCreated(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateUser(out var user)
            .AuthenticateUser(user)
            .Build(apiVersion);

        var companyName = NewCompanyName();

        // Act
        var response = await scenario.Execute(companyName);

        // Assert
        Check.That(response)
            .HasStatusCode(HttpStatusCode.Created).And
            .HasHeader(HeaderNames.Location);

        var document = await response.ReadRootJsonElement();
        Check.That(document).HasGuidProperty("id", response.ExtractGuidFromHeader(HeaderNames.Location));
        Check.That(document).HasStringProperty("name", companyName);
        Check.That(document).HasNullProperty("parentCompanyId");
    }

    [Theory]
    [InlineData(1)]
    public async Task StoreCreatedCompany(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateUser(out var user)
            .AuthenticateUser(user)
            .Build(apiVersion);

        var companyName = NewCompanyName();

        // Act
        _ = await scenario.Execute(companyName);

        // Assert
        Check.That(Companies()).HasSingleCompanyMatching(companyName);
    }

    [Theory]
    [InlineData(1)]
    public async Task PublishAuthenticatedMessage(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateUser(out var user)
            .AuthenticateUser(user)
            .Build(apiVersion);

        var companyName = NewCompanyName();

        // Act
        var response = await scenario.Execute(companyName);

        // Assert
        var options = Options<MessageQueueOptions>();
        var expectedCompanyId = response.ExtractGuidFromHeader(HeaderNames.Location);
        Check.That(PublishedMessages()).HasAuthenticatedMessage(ForCompany(expectedCompanyId), options);
    }

    [Theory]
    [InlineData(1)]
    public async Task IncreaseMetric(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateUser(out var user)
            .AuthenticateUser(user)
            .Build(apiVersion);

        // Act
        _ = await scenario.Execute(NewCompanyName());
        
        // Assert
        Metrics<ICountCompanies>()
            .Received()
            .WhenCreated();
    }

    private static Predicate<IPublishedMessage<ICompanyCreatedMessage>> ForCompany(Guid companyId)
    {
        return message => message.MessageObject is ICompanyCreatedMessage messageObject
                          && messageObject.CompanyId == companyId;
    }

    private static string NewCompanyName()
    {
        return Guid.NewGuid().ToString("N");
    }
}