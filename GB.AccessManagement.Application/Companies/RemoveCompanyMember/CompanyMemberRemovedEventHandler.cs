using GB.AccessManagement.Domain.Companies.Events;
using GB.AccessManagement.Domain.Companies.Messages;
using GB.AccessManagement.Domain.Messages;

namespace GB.AccessManagement.Application.Companies.RemoveCompanyMember;

public sealed class CompanyMemberRemovedEventHandler : IDomainEventHandler<CompanyMemberRemovedEvent>
{
    private readonly IPublishMessage _messages;

    public CompanyMemberRemovedEventHandler(IPublishMessage messages)
    {
        _messages = messages;
    }

    public Task Handle(CompanyMemberRemovedEvent @event, CancellationToken cancellationToken)
    {
        return _messages.Publish<ICompanyMemberRemovedMessage>((CompanyMemberRemovedMessage)@event, cancellationToken);
    }
}