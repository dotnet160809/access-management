using GB.AccessManagement.Infrastructure.MassTransit;
using GB.AccessManagement.Tests.Extensions;
using MassTransit.Testing;
using NFluent;
using NFluent.Extensibility;

namespace GB.AccessManagement.Tests.Assertions;

public static class PublishedMessagesAssertion
{
    public static void HasAuthenticatedMessage<TMessage>(this ICheck<IPublishedMessageList> check,
        Predicate<IPublishedMessage<TMessage>> predicate,
        MessageQueueOptions options) where TMessage : class
    {
        ExtensibilityHelper.BeginCheck(check)
            .FailWhen(messages => messages.HasNoMessage(WithAuthorizationHeader<TMessage>(options)), "No authenticated message found.")
            .FailWhen(messages => messages.SingleOrDefault(predicate) is null, "No authenticated message found matching provided predicate.")
            .OnNegate("No authenticated message found, either the message is not authenticated or no authenticated message matching the provided predicate has been found.")
            .EndCheck();

        _ = ExtensibilityHelper.BuildCheckLink(check);
    }

    private static Predicate<IPublishedMessage<TMessage>> WithAuthorizationHeader<TMessage>(MessageQueueOptions options) where TMessage : class
    {
        return message => message.HasAuthorizationHeader(options);
    }
}