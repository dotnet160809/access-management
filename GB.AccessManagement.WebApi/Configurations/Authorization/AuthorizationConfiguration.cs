namespace GB.AccessManagement.WebApi.Configurations.Authorization;

public static class AuthorizationConfiguration
{
    public static IServiceCollection ConfigureAuthorizations(this IServiceCollection services)
    {
        return services.AddAuthorization();
    }
}