namespace GB.HttpRequestMessagesStub.Internals;

public sealed class HttpResponseStubNotFoundException : Exception
{
    internal HttpResponseStubNotFoundException(HttpRequestMessageKey key) : base(BuildErrorMessage(key)) { }

    private static string? BuildErrorMessage(HttpRequestMessageKey key)
    {
        return $"No http response stub found for request '{key}'.";
    }
}