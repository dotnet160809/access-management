using GB.AccessManagement.Tests.Fixtures.OpenFga;
using GB.AccessManagement.Tests.Models;
using MassTransit.Testing;
using Xunit.Abstractions;

namespace GB.AccessManagement.Tests;

public abstract class AbstractFeatureShould<TBuilder> : IClassFixture<TBuilder> where TBuilder : AbstractScenarioBuilder<TBuilder>, new()
{
    private IProvideAssertionFixtures Assertions => ScenarioBuilder;
    protected TBuilder ScenarioBuilder { get; }

    protected AbstractFeatureShould(ITestOutputHelper outputHelper, TBuilder builder)
    {
        ScenarioBuilder = builder;
        ScenarioBuilder.SubscribeToException(exception => outputHelper.WriteLine("{0}", exception));
    }

    protected IEnumerable<TestCompany> Companies()
    {
        return Assertions.Companies();
    }

    protected TOptions Options<TOptions>()
    {
        return Assertions.Options<TOptions>();
    }

    protected IPublishedMessageList PublishedMessages()
    {
        return Assertions.PublishedMessages();
    }

    protected IAssertOpenFga Authorizations()
    {
        return Assertions.OpenFga();
    }

    protected TMetric Metrics<TMetric>() where TMetric : class
    {
        return Assertions.Metrics<TMetric>();
    }
}