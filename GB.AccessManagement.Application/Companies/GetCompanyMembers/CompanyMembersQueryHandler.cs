using GB.AccessManagement.Application.Presentations;
using GB.AccessManagement.Application.Queries;
using GB.AccessManagement.Domain.Companies.Providers;
using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Application.Companies.GetCompanyMembers;

public sealed class CompanyMembersQueryHandler : IQueryHandler<CompanyMembersQuery, CompanyMembersPresentation>
{
    private readonly IProvideCompanyMemberIds _provider;

    public CompanyMembersQueryHandler(IProvideCompanyMemberIds provider)
    {
        _provider = provider;
    }

    public async Task<CompanyMembersPresentation> Handle(CompanyMembersQuery query, CancellationToken cancellationToken)
    {
        var memberIds = await _provider.ListMembersForCompany(query.CompanyId);
        var memberPresentations = memberIds.Select(AsPresentation).ToArray();

        return new CompanyMembersPresentation(memberPresentations, memberIds.Length);
    }

    private static CompanyMemberPresentation AsPresentation(UserId memberId)
    {
        return new CompanyMemberPresentation((Guid)memberId);
    }
}