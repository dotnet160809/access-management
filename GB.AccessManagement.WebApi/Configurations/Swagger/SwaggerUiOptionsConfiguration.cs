using Asp.Versioning.ApiExplorer;
using GB.AccessManagement.Domain.Services;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace GB.AccessManagement.WebApi.Configurations.Swagger;

public sealed class SwaggerUiOptionsConfiguration : IConfigureOptions<SwaggerUIOptions>, ITransientService
{
    private readonly IServiceScopeFactory _factory;

    public SwaggerUiOptionsConfiguration(IServiceScopeFactory factory)
    {
        _factory = factory;
    }

    public void Configure(SwaggerUIOptions options)
    {
        using var scope = _factory.CreateScope();
        scope
            .ServiceProvider
            .GetRequiredService<IApiVersionDescriptionProvider>()
            .ApiVersionDescriptions
            .OrderByDescending(description => description.ApiVersion)
            .ToList()
            .ForEach(description =>
            {
                var url = $"/swagger/{description.GroupName}/swagger.json";
                var groupName = description.GroupName.ToUpperInvariant();
                options.SwaggerEndpoint(url, $"Access Management {groupName}");
            });
    }
}