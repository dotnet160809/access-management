using GB.AccessManagement.Application.Presentations;
using GB.AccessManagement.Domain.Services;

namespace GB.AccessManagement.WebApi.HalLinks.CompanyMemberHalLinks;

public sealed class CompanyMemberHalLinkBuilder : ISelfTransientService
{
    private readonly HalLinkHrefBuilder _builder;

    public CompanyMemberHalLinkBuilder(HalLinkHrefBuilder builder)
    {
        _builder = builder;
    }

    public CompanyMemberHalLinkBuilder BuildCompaniesHalLink(CompanyMemberPresentation presentation)
    {
        _ = presentation.AddLink("get_companies", _builder.BuildUserCompaniesLink(presentation.Id), HttpMethod.Get);

        return this;
    }
}