using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.WebApi.Configurations.Authorization.Attributes;

public sealed class CompanyMemberAuthorizeAttribute : CompanyAccessAuthorizeAttribute
{
    private static readonly CompanyRelation Relation = CompanyRelation.Member;
    
    public CompanyMemberAuthorizeAttribute(string routeParameterName) : base(routeParameterName, Relation) { }
}