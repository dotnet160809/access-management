using GB.AccessManagement.Infrastructure.Database;
using GB.AccessManagement.Tests.Extensions;
using GB.AccessManagement.Tests.Fixtures;
using GB.AccessManagement.Tests.Fixtures.OpenFga;
using GB.AccessManagement.Tests.Models;
using MassTransit.Testing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace GB.AccessManagement.Tests;

public abstract class AbstractScenarioBuilder<TBuilder> : IProvideAssertionFixtures
    where TBuilder : AbstractScenarioBuilder<TBuilder>
{
    private readonly WebApiFixture _fixture;
    private TestUser? _authenticatedUser;

    protected AbstractScenarioBuilder()
    {
        _fixture = new WebApiFixture(ConfigureServices);
    }

    public void SubscribeToException(Action<Exception> subscriber)
    {
        GetRequiredService<TestOutputHelperExceptionPublisher>().Subscribe(subscriber);
    }

    public TBuilder CreateUser(out TestUser user)
    {
        var userId = NewUserId();
        user = new TestUser(userId, $"{userId}@test.com");

        return (TBuilder)this;
    }

    public TBuilder CreateCompany(out TestCompany company)
    {
        var companyId = NewCompanyId();
        company = new TestCompany(NewCompanyId(), companyId.ToString(), null);

        return CreateCompany(company);
    }

    public TBuilder CreateCompany(Guid parentCompanyId, out TestCompany company)
    {
        var companyId = NewCompanyId();
        company = new TestCompany(companyId, companyId.ToString(), parentCompanyId);

        return CreateCompany(company);
    }

    public TBuilder AddCompanyOwner(Guid companyId, Guid ownerId)
    {
        GetRequiredService<CompanyFixture>().AddCompanyOwner(companyId, ownerId);

        return AddCompanyMember(companyId, ownerId);
    }

    public TBuilder AddCompanyMember(Guid companyId, Guid memberId)
    {
        GetRequiredService<CompanyFixture>().AddCompanyMember(companyId, memberId);

        return (TBuilder)this;
    }

    public TBuilder AuthenticateUser(TestUser user)
    {
        _authenticatedUser = user;

        return (TBuilder)this;
    }

    IEnumerable<TestCompany> IProvideAssertionFixtures.Companies()
    {
        return GetRequiredService<CompanyDbContext>()
            .Companies
            .Select(company => new TestCompany(company.Id, company.Name, company.ParentCompanyId));
    }

    public TOptions Options<TOptions>()
    {
        return GetRequiredService<IOptionsMonitor<TOptions>>().CurrentValue;
    }

    IPublishedMessageList IProvideAssertionFixtures.PublishedMessages()
    {
        return GetRequiredService<ITestHarness>().Published;
    }

    IAssertOpenFga IProvideAssertionFixtures.OpenFga()
    {
        return GetRequiredService<OpenFgaFixture>();
    }

    TMetric IProvideAssertionFixtures.Metrics<TMetric>() where TMetric : class
    {
        return GetRequiredService<TMetric>();
    }

    protected virtual void ConfigureServices(IServiceCollection services)
    {
        // Method intentionally left blank
    }

    protected TService GetRequiredService<TService>() where TService : notnull
    {
        return _fixture.Services.GetRequiredService<TService>();
    }

    protected HttpClient CreateClient()
    {
        var client = _fixture.CreateClient();

        if (_authenticatedUser is null)
        {
            return client;
        }

        GetRequiredService<AuthenticationFixture>().StubUser(_authenticatedUser);
        client.WithAuthenticatedUser(_authenticatedUser.Id);

        _authenticatedUser = null;

        return client;
    }

    protected static Guid NewUserId()
    {
        return Guid.NewGuid();
    }

    protected static Guid NewCompanyId()
    {
        return Guid.NewGuid();
    }

    private TBuilder CreateCompany(TestCompany company)
    {
        GetRequiredService<CompanyFixture>().AddCompany(company);

        return (TBuilder)this;
    }
}