using GB.AccessManagement.Application.Presentations;
using GB.AccessManagement.Application.Queries;
using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Application.Companies.GetCompanyMembers;

public sealed record CompanyMembersQuery : IQuery<CompanyMembersPresentation>
{
    internal readonly CompanyId CompanyId;

    public CompanyMembersQuery(Guid companyId)
    {
        CompanyId = companyId;
    }
}