using System.Text.Json.Serialization;

namespace GB.AccessManagement.Application.Presentations;

[Serializable]
public sealed record HalLink
{
    [JsonPropertyName("href")]
    public string Href { get; }
    
    [JsonPropertyName("method")]
    public string HttpMethod { get; }

    public HalLink(string href, HttpMethod httpMethod)
    {
        Href = href;
        HttpMethod = httpMethod.ToString().ToUpper();
    }
}