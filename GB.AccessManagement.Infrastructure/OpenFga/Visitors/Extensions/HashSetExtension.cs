namespace GB.AccessManagement.Infrastructure.OpenFga.Visitors.Extensions;

internal static class HashSetExtension
{
    public static void AddRange<T>(this HashSet<T> source, IEnumerable<T> items)
    {
        foreach (var item in items)
        {
            source.Add(item);
        }
    }
}