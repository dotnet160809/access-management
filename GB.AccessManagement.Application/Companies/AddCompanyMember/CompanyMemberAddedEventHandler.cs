using GB.AccessManagement.Domain.Companies.Events;
using GB.AccessManagement.Domain.Companies.Messages;
using GB.AccessManagement.Domain.Messages;

namespace GB.AccessManagement.Application.Companies.AddCompanyMember;

public sealed class CompanyMemberAddedEventHandler : IDomainEventHandler<CompanyMemberAddedEvent>
{
    private readonly IPublishMessage _messages;

    public CompanyMemberAddedEventHandler(IPublishMessage messages)
    {
        _messages = messages;
    }

    public Task Handle(CompanyMemberAddedEvent @event, CancellationToken cancellationToken)
    {
        return _messages.Publish<ICompanyMemberAddedMessage>((CompanyMemberAddedMessage)@event, cancellationToken);
    }
}