using GB.AccessManagement.Application.Commands;
using GB.AccessManagement.Domain.Companies;
using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Application.Companies.CreateCompany;

public sealed class CreateCompanyCommandHandler : ICommandHandler<CreateCompanyCommand, CompanyId>
{
    private readonly ICreateCompany _creator;
    private readonly ICompanyStore _store;

    public CreateCompanyCommandHandler(ICreateCompany creator, ICompanyStore store)
    {
        _creator = creator;
        _store = store;
    }

    public async Task<CompanyId> Handle(CreateCompanyCommand command, CancellationToken cancellationToken)
    {
        var aggregate = await _creator.Create(command.Name, command.OwnerId, command.ParentCompanyId);
        await _store.Save(aggregate, cancellationToken);

        return aggregate.Id;
    }
}