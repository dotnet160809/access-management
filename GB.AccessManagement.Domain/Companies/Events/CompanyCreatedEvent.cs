using GB.AccessManagement.Domain.Events;
using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Domain.Companies.Events;

public sealed record CompanyCreatedEvent(CompanyId Id, CompanyName Name, UserId OwnerId, CompanyId? ParentCompanyId) : DomainEvent;