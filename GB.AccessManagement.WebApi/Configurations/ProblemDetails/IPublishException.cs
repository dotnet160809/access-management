namespace GB.AccessManagement.WebApi.Configurations.ProblemDetails;

public interface IPublishException
{
    void Publish(Exception exception);
}