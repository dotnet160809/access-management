namespace GB.AccessManagement.Infrastructure.OpenFga.Visitors;

internal enum NodesVisitMode
{
    Intersect,
    Union
}