using GB.AccessManagement.Domain;
using GB.AccessManagement.Domain.Companies.Exceptions;
using GB.AccessManagement.Domain.Companies.Policies;
using GB.AccessManagement.Domain.Services;
using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Infrastructure.Database.Policies;

public sealed class CompanyOwnerPolicy : ICompanyOwnerPolicy, ITransientService
{
    private readonly IEvaluateCompanyAccesses _companyAccesses;

    public CompanyOwnerPolicy(IEvaluateCompanyAccesses companyAccesses)
    {
        _companyAccesses = companyAccesses;
    }

    public async Task EnsureUserIsCompanyOwner(UserId userId, CompanyId companyId)
    {
        if (!await _companyAccesses.ForOwner(userId, companyId))
        {
            throw new MissingCompanyAccessException(userId, "owner", companyId);
        }
    }
}