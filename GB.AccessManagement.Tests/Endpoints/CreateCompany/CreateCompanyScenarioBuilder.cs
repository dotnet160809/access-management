using GB.AccessManagement.Tests.Extensions;
using Microsoft.Extensions.DependencyInjection;

namespace GB.AccessManagement.Tests.Endpoints.CreateCompany;

public sealed class CreateCompanyScenarioBuilder : AbstractScenarioBuilder<CreateCompanyScenarioBuilder>
{
    public CreateCompanyScenario Build(int apiVersion)
    {
        return new CreateCompanyScenario(CreateClient(), apiVersion);
    }

    protected override void ConfigureServices(IServiceCollection services)
    {
        _ = services
            .StubAuthentication()
            .SubstituteCompanyMetrics();
    }
}