using System.Net.Http.Headers;

namespace GB.AccessManagement.Tests.Extensions;

public static class HttpClientExtension
{
    public static void WithAuthenticatedUser(this HttpClient client, Guid userId)
    {
        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userId.ToString());
    }
}