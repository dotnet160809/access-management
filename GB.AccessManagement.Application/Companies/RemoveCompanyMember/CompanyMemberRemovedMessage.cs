using GB.AccessManagement.Domain.Companies.Events;
using GB.AccessManagement.Domain.Companies.Messages;

namespace GB.AccessManagement.Application.Companies.RemoveCompanyMember;

[Serializable]
public sealed record CompanyMemberRemovedMessage(Guid CompanyId, string MemberId) : ICompanyMemberRemovedMessage
{
    public Guid Id { get; } = Guid.NewGuid();

    public static explicit operator CompanyMemberRemovedMessage(CompanyMemberRemovedEvent @event)
    {
        return new CompanyMemberRemovedMessage((Guid)@event.CompanyId, @event.MemberId.ToString());
    }
}