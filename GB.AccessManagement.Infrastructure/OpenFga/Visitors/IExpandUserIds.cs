using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Infrastructure.OpenFga.Visitors;

internal interface IExpandUserIds
{
    Task<string[]> Expand(ObjectType objectType, ObjectId objectId, Relation relation, bool expand);
}