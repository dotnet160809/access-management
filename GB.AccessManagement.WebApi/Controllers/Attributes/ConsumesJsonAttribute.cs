using Microsoft.AspNetCore.Mvc;

namespace GB.AccessManagement.WebApi.Controllers.Attributes;

public sealed class ConsumesJsonAttribute() : ConsumesAttribute(SupportedContentTypes.Json);