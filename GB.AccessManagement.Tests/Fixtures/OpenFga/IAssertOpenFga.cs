using GB.HttpRequestMessagesStub;

namespace GB.AccessManagement.Tests.Fixtures.OpenFga;

public interface IAssertOpenFga
{
    bool HasNotBeenCreated();

    bool HasNotBeenRemoved();

    Task HasBeenCreated(HttpRequestAssertionDelegate assertion);

    Task HasBeenRemoved(HttpRequestAssertionDelegate assertion);
}