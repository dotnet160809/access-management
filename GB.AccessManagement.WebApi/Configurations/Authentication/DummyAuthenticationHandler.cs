using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Microsoft.Net.Http.Headers;

namespace GB.AccessManagement.WebApi.Configurations.Authentication;

public sealed class DummyAuthenticationHandler : AuthenticationHandler<DummyAuthenticationOptions>
{
    private readonly IValidateAuthenticationToken _tokenValidator;
    
    public DummyAuthenticationHandler(
        IOptionsMonitor<DummyAuthenticationOptions> options,
        ILoggerFactory logger,
        UrlEncoder encoder,
        IValidateAuthenticationToken tokenValidator)
        : base(options, logger, encoder)
    {
        _tokenValidator = tokenValidator;
    }

    protected override Task<AuthenticateResult> HandleAuthenticateAsync()
    {
        if (!HasAuthorizationHeader(out var authorizationHeader))
        {
            return Task.FromResult(AuthenticateResult.NoResult());
        }

        var headerValues = authorizationHeader
            .ToArray()
            .First()!
            .Split(' ');

        if (!HasAccurateAuthenticationScheme(headerValues))
        {
            return Task.FromResult(AuthenticateResult.NoResult());
        }

        var (isValid, principal) = _tokenValidator.Validate(headerValues);

        if (!isValid)
        {
            return Task.FromResult(AuthenticateResult.Fail("Invalid token."));
        }

        var ticket = new AuthenticationTicket(principal!, DummyAuthenticationOptions.AuthenticationScheme);

        return Task.FromResult(AuthenticateResult.Success(ticket));
    }
    
    private bool HasAuthorizationHeader(out StringValues authorizationHeader)
    {
        return Context
            .Request
            .Headers
            .TryGetValue(HeaderNames.Authorization, out authorizationHeader);
    }

    private static bool HasAccurateAuthenticationScheme(string[] headerValues)
    {
        return headerValues
            .First()
            .Equals("Bearer", StringComparison.OrdinalIgnoreCase);
    }
}