using GB.AccessManagement.Application.Companies.CreateCompany;
using GB.AccessManagement.Domain.Companies.Messages;
using GB.AccessManagement.Infrastructure.MassTransit.Consumers;
using GB.AccessManagement.Tests.Extensions;
using GB.AccessManagement.Tests.Fixtures.OpenFga;
using MassTransit;
using Microsoft.Extensions.DependencyInjection;

namespace GB.AccessManagement.Tests.Consumers.CompanyCreated;

public sealed class CompanyCreatedScenarioBuilder : AbstractConsumerScenarioBuilder<CompanyCreatedScenarioBuilder>
{
    public CompanyCreatedScenarioBuilder CreateMessageContext(out ConsumeContext<ICompanyCreatedMessage> context)
    {
        var message = new CompanyCreatedMessage(NewCompanyId(), NewUserId().ToString(), null);

        return CreateMessageContext(message, out context);
    }

    public CompanyCreatedScenarioBuilder CreateForbiddenMessageContext(out ConsumeContext<ICompanyCreatedMessage> context)
    {
        var message = new CompanyCreatedMessage(NewCompanyId(), NewUserId().ToString(), null);
        
        return CreateForbiddenMessageContext(message, out context);
    }

    public CompanyCreatedScenarioBuilder CreateAuthenticatedMessageContext(Guid companyId, Guid userId, out ConsumeContext<ICompanyCreatedMessage> context)
    {
        var message = new CompanyCreatedMessage(companyId, userId.ToString(), null);
        
        return CreateAuthenticatedMessageContext(message, out context);
    }

    public CompanyCreatedScenario Build()
    {
        _ = GetRequiredService<OpenFgaFixture>().WriteReturnsOk();
        var consumer = GetRequiredService<CompanyCreatedMessageConsumer>();

        return new CompanyCreatedScenario(consumer);
    }

    protected override void ConfigureServices(IServiceCollection services)
    {
        _ = services
            .AddCompanyFixture()
            .StubOpenFga();
    }
}