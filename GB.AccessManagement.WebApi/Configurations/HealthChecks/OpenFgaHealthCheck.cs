using System.Text.Json;
using System.Text.Json.Serialization;
using GB.AccessManagement.Infrastructure.OpenFga;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Options;

namespace GB.AccessManagement.WebApi.Configurations.HealthChecks;

public sealed class OpenFgaHealthCheck : IHealthCheck
{
    private readonly IOptionsMonitor<OpenFgaOptions> _options;
    private readonly IHttpClientFactory _factory;

    public OpenFgaHealthCheck(IOptionsMonitor<OpenFgaOptions> options, IHttpClientFactory factory)
    {
        _options = options;
        _factory = factory;
    }

    public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new())
    {
        var client = _factory.CreateClient();
        client.BaseAddress = new Uri(_options.CurrentValue.BaseAddress);
        var response = await client.GetAsync("/healthz");
        
        return response.IsSuccessStatusCode
            ? await RetrieveResultFromResponse(response, cancellationToken)
            : HealthCheckResult.Unhealthy();
    }

    private static async Task<HealthCheckResult> RetrieveResultFromResponse(HttpResponseMessage response, CancellationToken cancellationToken)
    {
        var healthResponse = await DeserializeResponse(response, cancellationToken);

        return healthResponse?.Status.Equals("SERVING", StringComparison.OrdinalIgnoreCase) == true
            ? HealthCheckResult.Healthy()
            : HealthCheckResult.Degraded();
    }

    private static async Task<OpenFgaHealthResponse?> DeserializeResponse(HttpResponseMessage response, CancellationToken cancellationToken)
    {
        var responseContent = await response.Content.ReadAsStringAsync(cancellationToken);

        return JsonSerializer.Deserialize<OpenFgaHealthResponse>(responseContent);
    }
}

internal sealed record OpenFgaHealthResponse([property: JsonPropertyName("status")] string Status);