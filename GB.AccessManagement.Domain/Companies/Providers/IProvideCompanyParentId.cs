using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Domain.Companies.Providers;

public interface IProvideCompanyParentId
{
    Task<CompanyId?> FindParentForCompany(CompanyId companyId);
}