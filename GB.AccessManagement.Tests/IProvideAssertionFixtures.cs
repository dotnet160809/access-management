using GB.AccessManagement.Tests.Fixtures.OpenFga;
using GB.AccessManagement.Tests.Models;
using MassTransit.Testing;

namespace GB.AccessManagement.Tests;

public interface IProvideAssertionFixtures
{
    TOptions Options<TOptions>();

    IEnumerable<TestCompany> Companies();

    IPublishedMessageList PublishedMessages();

    IAssertOpenFga OpenFga();

    TMetric Metrics<TMetric>() where TMetric : class;
}