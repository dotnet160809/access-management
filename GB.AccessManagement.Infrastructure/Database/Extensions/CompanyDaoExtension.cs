using GB.AccessManagement.Application.Presentations;
using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Infrastructure.Database.Extensions;

public static class CompanyDaoExtension
{
    public static IQueryable<CompanyDao> Filter(this IQueryable<CompanyDao> companies, IEnumerable<CompanyId> ids)
    {
        var companyIds = ids
            .Select(id => (Guid)id)
            .ToArray();

        return companies.Where(company => companyIds.Contains(company.Id));
    }

    public static IQueryable<CompanyDao> ForCompany(this IQueryable<CompanyDao> companies, CompanyId companyId)
    {
        return companies.Where(company => company.Id == (Guid)companyId);
    }

    public static IQueryable<CompanyPresentation> AsPresentations(this IQueryable<CompanyDao> companies)
    {
        return companies.Select(company => new CompanyPresentation(company.Id, company.Name, company.ParentCompanyId));
    }
}