using System.Net;
using GB.AccessManagement.Tests.Assertions;
using GB.AccessManagement.Tests.Extensions;
using NFluent;
using NFluent.Json;
using Xunit.Abstractions;

namespace GB.AccessManagement.Tests.Endpoints.GetCompanyMembers;

public sealed class GetCompanyMembersEndpointShould : AbstractFeatureShould<GetCompanyMembersScenarioBuilder>
{
    public GetCompanyMembersEndpointShould(ITestOutputHelper outputHelper, GetCompanyMembersScenarioBuilder builder)
        : base(outputHelper, builder)
    {
    }

    [Theory]
    [InlineData(1)]
    public async Task ReturnUnauthorizedWhenNotAuthenticationIsProvided(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var company)
            .Build(apiVersion, company.Id, Guid.NewGuid());

        // Act
        var response = await scenario.Execute(company.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.Unauthorized);
    }

    [Theory]
    [InlineData(1)]
    public async Task ReturnForbiddenWhenUserIsNotCompanyMember(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var company)
            .CreateUser(out var user)
            .AuthenticateUser(user)
            .Build(apiVersion, company.Id, user.Id);

        // Act
        var response = await scenario.Execute(company.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.Forbidden);
    }

    [Theory]
    [InlineData(1, false)]
    [InlineData(1, true)]
    public async Task ReturnExpectedResponse(int apiVersion, bool isUserCompanyOwner)
    {
        // Arrange
        _ = ScenarioBuilder
            .CreateCompany(out var company)
            .CreateUser(out var user);

        _ = isUserCompanyOwner
            ? ScenarioBuilder.AddCompanyOwner(company.Id, user.Id)
            : ScenarioBuilder.AddCompanyMember(company.Id, user.Id);

        var scenario = ScenarioBuilder
            .AuthenticateUser(user)
            .Build(apiVersion, company.Id, user.Id);

        // Act
        var response = await scenario.Execute(company.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.OK);

        var document = await response.ReadRootJsonElement();
        Check.That(document).HasArrayPropertyEquivalentTo("items", [
            new
            {
                id = user.Id,
                _links = new
                {
                    get_companies = new
                    {
                        href = $"/v{apiVersion}/users/{user.Id}/companies",
                        method = "GET"
                    }
                }
            }
        ]);
        Check.That(document).HasHalPagination($"/v{apiVersion}/companies/{company.Id}/members", "GET");
    }
}