using GB.AccessManagement.WebApi.Configurations.OpenTelemetry.Metrics;
using OpenTelemetry.Metrics;
using OpenTelemetry.Resources;

namespace GB.AccessManagement.WebApi.Configurations.OpenTelemetry;

public static class OpenTelemetryConfiguration
{
    private static readonly string SourceName = AccessManagementActivity.Source.Name;
    private static readonly string? SourceVersion = AccessManagementActivity.Source.Version;

    public static IServiceCollection ConfigureTracing(this IServiceCollection services)
    {
        return services
            .AddOpenTelemetry()
            .ConfigureResource(builder => builder.AddService(SourceName, serviceVersion: SourceVersion))
            .WithMetrics(builder => builder
                .AddMeter(AccessManagementActivity.Source.Name)
                .AddMeter(new CompanyMetrics().Name)
                .AddMeter(new CompanyAccessMetrics().Name)
                .AddAspNetCoreInstrumentation()
                .AddHttpClientInstrumentation()
                .AddRuntimeInstrumentation()
                .AddProcessInstrumentation()
                .AddPrometheusExporter())
            .Services;
    }
}