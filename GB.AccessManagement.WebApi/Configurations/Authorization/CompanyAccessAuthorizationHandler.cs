using System.Net;
using System.Security.Claims;
using GB.AccessManagement.Domain;
using GB.AccessManagement.Domain.Services;
using Microsoft.AspNetCore.Authorization;

namespace GB.AccessManagement.WebApi.Configurations.Authorization;

public sealed class CompanyAccessAuthorizationHandler : AuthorizationHandler<CompanyAccessAuthorizationRequirement>,
    ISingletonService
{
    private readonly IServiceScopeFactory _factory;

    public CompanyAccessAuthorizationHandler(IServiceScopeFactory factory)
    {
        _factory = factory;
    }

    protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, CompanyAccessAuthorizationRequirement requirement)
    {
        if (IsUserNotAuthenticated(context))
        {
            return;
        }

        if (CanNotFindRouteParameterFromHttpContext(
                requirement.RouteParameterName, 
                out var objectId))
        {
            throw new HttpRequestException(
                $"Missing route parameter '{requirement.RouteParameterName}'", 
                null, 
                HttpStatusCode.BadRequest);
        }

        var userId = context.User.FindFirstValue(ClaimTypes.NameIdentifier) ?? string.Empty;

        if (await UserDoesNotHaveRelationWithCompany(userId, objectId!, requirement.Relation))
        {
            context.Fail();

            return;
        }
        
        context.Succeed(requirement);
    }

    private static bool IsUserNotAuthenticated(AuthorizationHandlerContext context)
    {
        return context.User.Identity?.IsAuthenticated == false;
    }

    private bool CanNotFindRouteParameterFromHttpContext(string routeParameterName, out string? routeParameterValue)
    {
        using var scope = _factory.CreateScope();
        var context = scope.ServiceProvider.GetRequiredService<IHttpContextAccessor>().HttpContext!;
        routeParameterValue = context.Request.RouteValues[routeParameterName]?.ToString();

        return string.IsNullOrEmpty(routeParameterName);
    }

    private async Task<bool> UserDoesNotHaveRelationWithCompany(string userId, string objectId, string relation)
    {
        using var scope = _factory.CreateScope();
        var mediator = scope.ServiceProvider.GetRequiredService<IEvaluateCompanyAccesses>();

        return !await mediator.WithRelation(userId, relation, objectId);
    }
}