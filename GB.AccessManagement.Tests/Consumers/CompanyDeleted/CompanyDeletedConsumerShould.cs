using GB.AccessManagement.Tests.Assertions;
using GB.AccessManagement.Tests.Extensions;
using GB.AccessManagement.Tests.Models;
using GB.HttpRequestMessagesStub;
using NFluent;
using Xunit.Abstractions;

namespace GB.AccessManagement.Tests.Consumers.CompanyDeleted;

public sealed class CompanyDeletedConsumerShould : AbstractFeatureShould<CompanyDeletedScenarioBuilder>
{
    public CompanyDeletedConsumerShould(ITestOutputHelper outputHelper, CompanyDeletedScenarioBuilder builder)
        : base(outputHelper, builder)
    {
    }

    [Fact]
    public async Task NotRemoveAnyAuthorizationWhenConsumingUnauthenticatedMessages()
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateMessageContext(out var context)
            .Build();

        // Act
        await scenario.Execute(context);

        // Assert
        Check.That(Authorizations()).HasNotBeenRemoved();
    }

    [Fact]
    public async Task NotRemoveAnyAuthorizationWhenConsumingForbiddenMessages()
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateForbiddenMessageContext(out var context)
            .Build();

        // Act
        await scenario.Execute(context);

        // Assert
        Check.That(Authorizations()).HasNotBeenRemoved();
    }

    [Theory]
    [InlineData(false, false, false)]
    [InlineData(false, false, true)]
    [InlineData(false, true, false)]
    [InlineData(false, true, true)]
    [InlineData(true, false, false)]
    [InlineData(true, false, true)]
    [InlineData(true, true, false)]
    [InlineData(true, true, true)]
    public async Task RemoveExpectedAuthorizationsWhenConsumingAuthenticatedMessage(
        bool companyHasParent,
        bool companyHasChildren,
        bool companyHasMembers)
    {
        // Arrange
        TestCompany? parent = null;

        _ = companyHasParent
            ? ScenarioBuilder.CreateCompany(out parent).CreateCompany(parent.Id, out var company)
            : ScenarioBuilder.CreateCompany(out company);

        TestCompany? child = null;

        if (companyHasChildren)
        {
            _ = ScenarioBuilder.CreateCompany(company.Id, out child);
        }

        TestUser? member = null;

        if (companyHasMembers)
        {
            _ = ScenarioBuilder
                .CreateUser(out member)
                .AddCompanyMember(company.Id, member.Id);
        }

        var scenario = ScenarioBuilder
            .CreateUser(out var owner)
            .AddCompanyOwner(company.Id, owner.Id)
            .CreateAuthenticatedMessageContext(company.Id, out var context)
            .Build();

        // Act
        await scenario.Execute(context);

        // Assert
        var expectedRemovedAccesses = new List<TestTupleKey>
        {
            new($"user:{owner.Id}", "owner", $"company:{company.Id}"),
            new($"user:{owner.Id}", "member", $"company:{company.Id}")
        };

        if (companyHasParent)
        {
            expectedRemovedAccesses.Add(new TestTupleKey($"company:{parent!.Id}", "parent", $"company:{company.Id}"));
        }

        if (companyHasChildren)
        {
            expectedRemovedAccesses.Add(new TestTupleKey($"company:{company.Id}", "parent", $"company:{child!.Id}"));
        }

        if (companyHasMembers)
        {
            expectedRemovedAccesses.Add(new TestTupleKey($"user:{member!.Id}", "member", $"company:{company.Id}"));
        }

        await Authorizations().HasBeenRemoved(AsExpected(expectedRemovedAccesses));
    }

    private static HttpRequestAssertionDelegate AsExpected(IEnumerable<TestTupleKey> expectedTupleKeys)
    {
        return async content =>
        {
            var document = await content.ReadAsJsonRootElement();
            Check.That(document).HasRemovedTupleKeys(expectedTupleKeys.ToArray());
        };
    }
}