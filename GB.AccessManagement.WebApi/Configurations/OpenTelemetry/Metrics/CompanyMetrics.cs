using System.Diagnostics.Metrics;
using GB.AccessManagement.Domain.Services;
using GB.AccessManagement.Infrastructure.Database;

namespace GB.AccessManagement.WebApi.Configurations.OpenTelemetry.Metrics;

public sealed class CompanyMetrics : ICountCompanies, ISingletonService
{
    private readonly Counter<int> _companyCreatedCounter;
    private readonly Counter<int> _companyUpdatedCounter;
    private readonly Counter<int> _companyRemovedCounter;
    private readonly ObservableGauge<int> _companyTotal;
    private int _total = 0;
    
    public string Name { get; }

    public CompanyMetrics()
    {
        var meter = new Meter("companies");
        Name = meter.Name;

        _companyCreatedCounter = meter.CreateCounter<int>($"{Name}.created");
        _companyUpdatedCounter = meter.CreateCounter<int>($"{Name}.updated");
        _companyRemovedCounter = meter.CreateCounter<int>($"{Name}.removed");
        _companyTotal = meter.CreateObservableGauge(
            $"{Name}.total",
            () => new Measurement<int>(_total));
    }

    public void WhenCreated()
    {
        _companyCreatedCounter.Add(1);
        _total++;
    }

    public void WhenUpdated()
    {
        _companyUpdatedCounter.Add(1);
    }

    public void WhenDeleted()
    {
        _companyRemovedCounter.Add(1);
        _total--;
    }
}