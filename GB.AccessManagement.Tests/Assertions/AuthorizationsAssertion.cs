using System.Text.Json;
using GB.AccessManagement.Tests.Models;
using NFluent;
using NFluent.Json;

namespace GB.AccessManagement.Tests.Assertions;

public static class AuthorizationsAssertion
{
    public static void HasCreatedTupleKeys(this ICheck<JsonElement> check, params TestTupleKey[] expectedTupleKeys)
    {
        check.HasExpectedTupleKeys("writes", expectedTupleKeys);
    }

    public static void HasRemovedTupleKeys(this ICheck<JsonElement> check, params TestTupleKey[] expectedTupleKeys)
    {
        check.HasExpectedTupleKeys("deletes", expectedTupleKeys);
    }

    private static void HasExpectedTupleKeys(
        this ICheck<JsonElement> check,
        string propertyName,
        params TestTupleKey[] expectedTupleKeys)
    {
        _ = check
            .HasProperty(propertyName)
            .And
            .WhichMember(element => element.GetProperty(propertyName))
            .Verifies(element => element.HasArrayPropertyEquivalentTo("tuple_keys", expectedTupleKeys));
    }
}