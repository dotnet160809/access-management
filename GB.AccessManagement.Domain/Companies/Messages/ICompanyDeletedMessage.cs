using GB.AccessManagement.Domain.Messages;

namespace GB.AccessManagement.Domain.Companies.Messages;

public interface ICompanyDeletedMessage : IMessage
{
    public Guid CompanyId { get; }
}

public sealed record CompanyDeletedMessage(Guid CompanyId) : ICompanyDeletedMessage
{
    public Guid Id { get; } = Guid.NewGuid();
}