using GB.AccessManagement.Tests.Extensions;
using GB.AccessManagement.Tests.Fixtures.OpenFga;
using Microsoft.Extensions.DependencyInjection;

namespace GB.AccessManagement.Tests.Endpoints.AddCompanyChild;

public sealed class AddCompanyChildScenarioBuilder : AbstractEndpointScenarioBuilder<AddCompanyChildScenarioBuilder>
{
    public AddCompanyChildScenario Build(int apiVersion)
    {
        _ = GetRequiredService<OpenFgaFixture>()
            .CheckReturns(CompanyOwnerOrMembers)
            .ExpandReturns(CompanyOwnerOrMemberIds)
            .ListObjectsReturns([]);

        return new AddCompanyChildScenario(CreateClient(), apiVersion);
    }

    protected override void ConfigureServices(IServiceCollection services)
    {
        _ = services
            .AddCompanyFixture()
            .StubAuthentication()
            .StubOpenFga();
    }
}