using OpenFga.Sdk.Model;

namespace GB.AccessManagement.Infrastructure.OpenFga.Visitors.Extensions;

internal static class ComputedExtension
{
    public static async Task<string[]> Accept(this Computed? computed, IVisitExpandResponse visitor)
    {
        return computed is not null
            ? await visitor.Visit(computed)
            : [];
    }
}