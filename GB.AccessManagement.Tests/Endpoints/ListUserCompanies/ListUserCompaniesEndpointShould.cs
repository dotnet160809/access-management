using System.Net;
using GB.AccessManagement.Tests.Assertions;
using GB.AccessManagement.Tests.Extensions;
using GB.AccessManagement.Tests.Models;
using NFluent;
using NFluent.Json;
using Xunit.Abstractions;

namespace GB.AccessManagement.Tests.Endpoints.ListUserCompanies;

public sealed class ListUserCompaniesEndpointShould : AbstractFeatureShould<ListUserCompaniesScenarioBuilder>
{
    public ListUserCompaniesEndpointShould(ITestOutputHelper outputHelper, ListUserCompaniesScenarioBuilder builder) :
        base(outputHelper, builder)
    {
    }

    [Theory]
    [InlineData(1)]
    public async Task ReturnUnauthorizedWhenUserIsNotAuthenticated(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateUser(out var user)
            .Build(apiVersion, user.Id);

        // Act
        var response = await scenario.Execute(user.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.Unauthorized);
    }

    [Theory]
    [InlineData(1, false)]
    [InlineData(1, true)]
    public async Task ReturnExpectedCompanies(int apiVersion, bool isUserCompanyOwner)
    {
        // Arrange
        _ = ScenarioBuilder
            .CreateUser(out var user)
            .CreateCompany(out var company);

        _ = isUserCompanyOwner
            ? ScenarioBuilder.AddCompanyOwner(company.Id, user.Id)
            : ScenarioBuilder.AddCompanyMember(company.Id, user.Id);

        var scenario = ScenarioBuilder.AuthenticateUser(user)
            .Build(apiVersion, user.Id);

        // Act
        var response = await scenario.Execute(user.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.OK);

        var document = await response.ReadRootJsonElement();
        var expectedHalLinks = new TestHalLinks
        {
            ["self"] = TestHalLink.Get($"/v{apiVersion}/companies/{company.Id}"),
            ["get_owner"] = TestHalLink.Get($"/v{apiVersion}/companies/{company.Id}/owner"),
        };

        if (isUserCompanyOwner)
        {
            expectedHalLinks.Add("add_member", TestHalLink.Post($"/v{apiVersion}/companies/{company.Id}/members"));
        }
        
        company.AddLinks(expectedHalLinks);
        
        Check.That(document).HasArrayPropertyEquivalentTo("items", [company]);
        Check.That(document).HasIntProperty("total", 1);
        Check.That(document).HasHalPagination($"/v{apiVersion}/users/{user.Id}/companies", "GET");
    }
}