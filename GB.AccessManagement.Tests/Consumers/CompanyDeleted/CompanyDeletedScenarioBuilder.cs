using GB.AccessManagement.Domain.Companies.Messages;
using GB.AccessManagement.Infrastructure.MassTransit.Consumers;
using GB.AccessManagement.Tests.Extensions;
using GB.AccessManagement.Tests.Fixtures;
using GB.AccessManagement.Tests.Fixtures.OpenFga;
using MassTransit;
using Microsoft.Extensions.DependencyInjection;

namespace GB.AccessManagement.Tests.Consumers.CompanyDeleted;

public sealed class CompanyDeletedScenarioBuilder : AbstractConsumerScenarioBuilder<CompanyDeletedScenarioBuilder>
{
    public CompanyDeletedScenarioBuilder CreateMessageContext(out ConsumeContext<ICompanyDeletedMessage> context)
    {
        var message = new CompanyDeletedMessage(NewCompanyId());

        return CreateMessageContext(message, out context);
    }

    public CompanyDeletedScenarioBuilder CreateForbiddenMessageContext(out ConsumeContext<ICompanyDeletedMessage> context)
    {
        var message = new CompanyDeletedMessage(NewCompanyId());

        return CreateForbiddenMessageContext(message, out context);
    }

    public CompanyDeletedScenarioBuilder CreateAuthenticatedMessageContext(Guid companyId, out ConsumeContext<ICompanyDeletedMessage> context)
    {
        var message = new CompanyDeletedMessage(companyId);

        return CreateAuthenticatedMessageContext(message, out context);
    }

    public CompanyDeletedScenario Build()
    {
        GetRequiredService<OpenFgaFixture>()
            .ExpandReturns(async request =>
            {
                var document = await request.ReadRootJsonElement();
                var companyId = Guid.Parse(document.GetUserId());

                return document.GetRelation() switch
                {
                    "owner" => [GetRequiredService<CompanyFixture>().GetCompanyOwner(companyId)],
                    "member" => GetRequiredService<CompanyFixture>().GetCompanyMembers(companyId),
                    _ => Array.Empty<string>()
                };
            })
            .ListObjectsReturns(async request =>
            {
                var document = await request.ReadRootJsonElement();
                var companyId = Guid.Parse(document.GetUserId());

                return document.GetRelation() switch
                {
                    "owner" => [GetRequiredService<CompanyFixture>().GetCompanyOwner(companyId)],
                    "member" => GetRequiredService<CompanyFixture>().GetCompanyMembers(companyId),
                    _ => Array.Empty<string>()
                };
            })
            .WriteReturnsOk();

        var consumer = GetRequiredService<CompanyDeletedMessageConsumer>();

        return new CompanyDeletedScenario(consumer);
    }

    protected override void ConfigureServices(IServiceCollection services)
    {
        _ = services
            .StubOpenFga()
            .AddCompanyFixture();
    }
}