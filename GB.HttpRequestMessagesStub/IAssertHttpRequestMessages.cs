namespace GB.HttpRequestMessagesStub;

public delegate Task HttpRequestAssertionDelegate(HttpContent? content);

/// <summary>
/// Assert http requests
/// </summary>
public interface IAssertHttpRequestMessages
{
    /// <summary>
    /// Assert the current http request has not been sent
    /// </summary>
    /// <returns>True if the current http request has not been sent, otherwise false</returns>
    bool HasNotBeenSent();

    /// <summary>
    /// Assert the current http request sent while matching the provided predicate
    /// </summary>
    /// <param name="assertion">The delegate to execute when asserting the sent http request</param>
    Task HasBeenSentOnce(HttpRequestAssertionDelegate assertion);
}