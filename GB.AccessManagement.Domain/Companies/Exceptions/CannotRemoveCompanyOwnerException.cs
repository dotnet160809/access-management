using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Domain.Companies.Exceptions;

[Serializable]
public sealed class CannotRemoveCompanyOwnerException : DomainException
{
    private const string MessagePattern = "User '{0}' is owner of company '{1}' and can not be removed.";
    
    public CannotRemoveCompanyOwnerException(UserId ownerId, CompanyId companyId)
        : base(string.Format(MessagePattern, ownerId, companyId)) { }
}