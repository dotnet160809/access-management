using System.Text;
using System.Text.Json.Serialization;

namespace GB.AccessManagement.Application.Presentations;

[Serializable]
public abstract record PaginatedHalPresentation<TPresentation, TItem> : HalPresentation<TPresentation>
    where TPresentation : PaginatedHalPresentation<TPresentation, TItem>
{
    [JsonPropertyName("items")]
    public TItem[] Items { get; }

    [JsonPropertyName("total")]
    public int Total { get; private set; }

    protected PaginatedHalPresentation(TItem[] items, int total)
    {
        EnsureTotalIsValid(total, items.Length);

        Items = items;
        Total = total;
    }

    public TPresentation AddPaginationLinks(string href, int currentPage, int itemsPerPage)
    {
        var method = HttpMethod.Get;

        return AddFirstLink(href, method, itemsPerPage)
            .AddPreviousLink(href, method, currentPage, itemsPerPage)
            .AddNextLink(href, method, currentPage, itemsPerPage)
            .AddLastLink(href, method, itemsPerPage);
    }

    private TPresentation AddFirstLink(string href, HttpMethod method, int itemsPerPage)
    {
        href = BuildLinkHref(href, 1, itemsPerPage);

        return AddLink("first", href, method);
    }

    private TPresentation AddPreviousLink(string href, HttpMethod method, int currentPage, int itemsPerPage)
    {
        if (!HasAnyItemToDisplay(Total) || !IsCurrentPageSuperiorToFirstOne(currentPage))
        {
            return (TPresentation)this;
        }

        href = BuildLinkHref(href, currentPage - 1, itemsPerPage);

        return AddLink("previous", href, method);
    }

    private TPresentation AddNextLink(string href, HttpMethod method, int currentPage, int itemsPerPage)
    {
        if (!HasAnyItemToDisplay(Total) || !IsCurrentPageInferiorToLastOne(currentPage, itemsPerPage, Total))
        {
            return (TPresentation)this;
        }

        href = BuildLinkHref(href, currentPage + 1, itemsPerPage);

        return AddLink("next", href, method);
    }

    private TPresentation AddLastLink(string href, HttpMethod method, int itemsPerPage)
    {
        href = BuildLinkHref(href, GetLastPage(itemsPerPage, Total), itemsPerPage);

        return AddLink("last", href, method);
    }

    private static string BuildLinkHref(string href, int page, int itemsPerPage)
    {
        StringBuilder builder = new(href);
        var firstParameterCharacter = href.Contains('?') ? '&' : '?';

        return builder
            .Append($"{firstParameterCharacter}page={page}")
            .Append($"&length={itemsPerPage}")
            .ToString();
    }

    private bool HasAnyItemToDisplay(int total)
    {
        return total > 0 && Items.Any();
    }

    private static bool IsCurrentPageSuperiorToFirstOne(int currentPage)
    {
        return currentPage - 1 > 0;
    }

    private static bool IsCurrentPageInferiorToLastOne(int currentPage, int itemsPerPage, int total)
    {
        return currentPage < GetLastPage(itemsPerPage, total);
    }

    private static int GetLastPage(int itemsPerPage, int total)
    {
        if (HasAnyItemOnLastPage(itemsPerPage, total, out var lastPageIndex))
        {
            return lastPageIndex + 1;
        }

        return RetrieveAccurateLastPageIndex(lastPageIndex);
    }

    private static bool HasAnyItemOnLastPage(int itemsPerPage, int total, out int lastPageIndex)
    {
        lastPageIndex = Math.DivRem(total, itemsPerPage, out var rest);

        return rest > 0;
    }

    private static int RetrieveAccurateLastPageIndex(int lastPageIndex)
    {
        return lastPageIndex > 0 ? lastPageIndex : 1;
    }

    private static void EnsureTotalIsValid(int total, int itemsCount)
    {
        if (total < itemsCount)
        {
            throw new ArgumentException($"Total count must be superior or equal to {itemsCount}.");
        }
    }
}