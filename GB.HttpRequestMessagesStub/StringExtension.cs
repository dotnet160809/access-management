namespace GB.HttpRequestMessagesStub;

public static class StringExtension
{
    public static bool OrdinalEquals(this string value, string other)
    {
        return string.Equals(value, other, StringComparison.Ordinal);
    }

    public static int GetOrdinalHashCode(this string value)
    {
        return string.IsNullOrEmpty(value)
            ? 0
            : StringComparer.Ordinal.GetHashCode(value);
    }
}