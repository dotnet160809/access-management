using OpenFga.Sdk.Model;

namespace GB.AccessManagement.Infrastructure.OpenFga.Visitors.Extensions;

internal static class LeafExtension
{
    public static async Task<string[]> Accept(this Leaf? leaf, IVisitExpandResponse visitor)
    {
        return leaf is not null
            ? await visitor.Visit(leaf)
            : [];
    }
}