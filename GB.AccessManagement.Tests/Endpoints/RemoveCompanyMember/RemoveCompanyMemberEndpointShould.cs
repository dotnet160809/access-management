using System.Net;
using GB.AccessManagement.Domain.Companies.Messages;
using GB.AccessManagement.Infrastructure.MassTransit;
using GB.AccessManagement.Tests.Assertions;
using GB.HttpRequestMessagesStub;
using MassTransit.Testing;
using NFluent;
using Xunit.Abstractions;

namespace GB.AccessManagement.Tests.Endpoints.RemoveCompanyMember;

public sealed class RemoveCompanyMemberEndpointShould : AbstractFeatureShould<RemoveCompanyMemberScenarioBuilder>
{
    public RemoveCompanyMemberEndpointShould(ITestOutputHelper outputHelper, RemoveCompanyMemberScenarioBuilder builder) : base(outputHelper, builder)
    {
    }

    [Theory]
    [InlineData(1)]
    public async Task ReturnUnauthorizedWhenUserIsNotAuthenticated(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var company)
            .CreateUser(out var user)
            .Build(apiVersion);

        // Act
        var response = await scenario.Execute(company.Id, user.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.Unauthorized);
    }

    [Theory]
    [InlineData(1, false)]
    [InlineData(1, true)]
    public async Task ReturnForbiddenWhenUserIsNotCompanyOwner(int apiVersion, bool addUserToCompany)
    {
        // Arrange
        _ = ScenarioBuilder
            .CreateCompany(out var company)
            .CreateUser(out var user);

        if (addUserToCompany)
        {
            _ = ScenarioBuilder.AddCompanyMember(company.Id, user.Id);
        }

        var scenario = ScenarioBuilder
            .AuthenticateUser(user)
            .Build(apiVersion);

        // Act
        var response = await scenario.Execute(company.Id, user.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.Forbidden);
    }

    [Theory]
    [InlineData(1)]
    public async Task ReturnUnprocessableEntityWhenUserIsNotCompanyMember(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var company)
            .CreateUser(out var owner)
            .AddCompanyOwner(company.Id, owner.Id)
            .CreateUser(out var user)
            .AuthenticateUser(owner)
            .Build(apiVersion);

        // Act
        var response = await scenario.Execute(company.Id, user.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.UnprocessableEntity);
    }

    [Theory]
    [InlineData(1)]
    public async Task ReturnNoContentWhenMemberHasBeenSuccessfullyRemoved(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var company)
            .CreateUser(out var owner)
            .AddCompanyOwner(company.Id, owner.Id)
            .CreateUser(out var member)
            .AddCompanyMember(company.Id, member.Id)
            .AuthenticateUser(owner)
            .Build(apiVersion);

        // Act
        var response = await scenario.Execute(company.Id, member.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.NoContent);
    }

    [Theory]
    [InlineData(1)]
    public async Task PublishAuthenticatedMessage(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var company)
            .CreateUser(out var owner)
            .AddCompanyOwner(company.Id, owner.Id)
            .CreateUser(out var member)
            .AddCompanyMember(company.Id, member.Id)
            .AuthenticateUser(owner)
            .Build(apiVersion);

        // Act
        _ = await scenario.Execute(company.Id, member.Id);

        // Assert
        var options = Options<MessageQueueOptions>();
        Check.That(PublishedMessages()).HasAuthenticatedMessage(ForCompanyMember(company.Id, member.Id), options);
    }

    private static Predicate<IPublishedMessage<ICompanyMemberRemovedMessage>> ForCompanyMember(Guid companyId, Guid memberId)
    {
        return message => message.MessageObject is ICompanyMemberRemovedMessage messageObject
                          && messageObject.CompanyId == companyId
                          && messageObject.MemberId.OrdinalEquals(memberId.ToString());
    }
}