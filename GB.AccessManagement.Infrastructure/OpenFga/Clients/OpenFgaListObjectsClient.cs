using GB.AccessManagement.Domain.Companies.Providers;
using GB.AccessManagement.Domain.Services;
using GB.AccessManagement.Domain.ValueObjects;
using Microsoft.Extensions.Options;
using OpenFga.Sdk.Model;

namespace GB.AccessManagement.Infrastructure.OpenFga.Clients;

public sealed class OpenFgaListObjectsClient : AbstractOpenFgaClient,
    IProvideUserCompanyIds,
    ITransientService
{
    public OpenFgaListObjectsClient(IHttpClientFactory factory, IOptionsMonitor<OpenFgaOptions> options)
        : base(factory, options)
    {
    }

    public async Task<CompanyId[]> ListCompaniesForUser(UserId userId)
    {
        var companyIds = await ListObjects(CompanyRelation.UserType, userId, "member", CompanyRelation.ObjectType);

        return companyIds.Select(id => (CompanyId)id.ToString()).ToArray();
    }

    private async Task<ObjectId[]> ListObjects(UserType userType, UserId userId, Relation relation, ObjectType objectType)
    {
        var response = await Post<ListObjectsResponse>(ListObjectsPath(), new ListObjectsRequest
        {
            User = $"{userType}:{userId}",
            Type = objectType.ToString(),
            Relation = relation.ToString()
        });

        return response.Objects.Select(objectId => (ObjectId)objectId).ToArray();
    }

    private string ListObjectsPath()
    {
        return $"/stores/{StoreId()}/list-objects";
    }
}