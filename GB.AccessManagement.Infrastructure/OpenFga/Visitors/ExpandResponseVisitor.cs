using System.Diagnostics.CodeAnalysis;
using GB.AccessManagement.Infrastructure.OpenFga.Visitors.Extensions;
using OpenFga.Sdk.Model;

namespace GB.AccessManagement.Infrastructure.OpenFga.Visitors;

internal sealed class ExpandResponseVisitor(IExpandUserIds expander) : IVisitExpandResponse
{
    private readonly Dictionary<string, string[]> _relationUserIds = new();
    private bool _expand;

    public Task<string[]> Visit(ExpandResponse response, bool expand)
    {
        _expand = expand;

        return response.Accept(this);
    }

    public Task<string[]> Visit(UsersetTree tree)
    {
        return tree.Root.Accept(this);
    }

    public async Task<string[]> Visit(Node node)
    {
        var result = new HashSet<string>();
        result.AddRange(await node.Difference.Accept(this));
        result.AddRange(await Intersect(node.Intersection));
        result.AddRange(await node.Leaf.Accept(this));
        result.AddRange(await Union(node.Union));

        return result.ToArray();
    }

    public async Task<string[]> Visit(UsersetTreeDifference difference)
    {
        var baseUserIds = await difference.Base.Accept(this);
        var subtractUserIds = await difference.Subtract.Accept(this);

        return baseUserIds.Except(subtractUserIds).ToArray();
    }

    public async Task<string[]> Visit(Leaf leaf)
    {
        var result = new HashSet<string>();
        result.AddRange(await leaf.Computed.Accept(this));
        result.AddRange(await leaf.TupleToUserset.Accept(this));
        result.AddRange(await leaf.Users.Accept(this));

        return result.ToArray();
    }

    public async Task<string[]> Visit(IEnumerable<Computed> list)
    {
        var result = new HashSet<string>();

        foreach (var item in list)
        {
            result.AddRange(await item.Accept(this));
        }
        
        return result.ToArray();
    }

    public Task<string[]> Visit(Computed computed)
    {
        return computed.Userset.Accept(this);
    }

    public async Task<string[]> Visit(string value)
    {
        if (_relationUserIds.TryGetValue(value, out var userIds))
        {
            return userIds;
        }

        if (!_expand || !value.Contains('#'))
        {
            return [value];
        }

        var values = value.Split('#');
        var relation = values.Last();

        var objectsValues = values.First().Split(':');
        var objectType = objectsValues.First();
        var objectId = objectsValues.Last();

        userIds = await expander.Expand(objectType, objectId, relation, _expand);
        _relationUserIds.Add(value, userIds);

        return userIds;
    }

    public async Task<string[]> Visit(UsersetTreeTupleToUserset userSet)
    {
        var result = new HashSet<string>();
        result.AddRange(await userSet.Computed.Accept(this));
        result.AddRange(await userSet.Tupleset.Accept(this));

        return result.ToArray();
    }

    public async Task<string[]> Visit(Users users)
    {
        var result = new HashSet<string>();

        foreach (var user in users._Users)
        {
            result.AddRange(await user.Accept(this));
        }
        
        return result.ToArray();
    }

    private Task<string[]> Intersect(Nodes? nodes)
    {
        return Visit(nodes, (a, b) => a.Intersect(b));
    }

    private Task<string[]> Union(Nodes? nodes)
    {
        return Visit(nodes, (a, b) => a.Union(b));
    }

    private async Task<string[]> Visit(Nodes? nodes, Func<IEnumerable<string>, IEnumerable<string>, IEnumerable<string>> function)
    {
        if (nodes is null || nodes._Nodes.Count == 0)
        {
            return [];
        }
        
        var result = await nodes._Nodes.First().Accept(this);

        foreach (var node in nodes._Nodes.Skip(1))
        {
            result = function(result, await node.Accept(this)).ToArray();
        }

        return result.ToArray();
    }
}