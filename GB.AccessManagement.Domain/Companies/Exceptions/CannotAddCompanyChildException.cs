using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Domain.Companies.Exceptions;

public sealed class CannotAddCompanyChildException : DomainException
{
    private const string MessagePattern = "Company '{0}' has already been added as child to company '{1}'.";

    public CannotAddCompanyChildException(CompanyId parentCompanyId, CompanyId childCompanyId)
        : base(string.Format(MessagePattern, parentCompanyId, childCompanyId))
    {
    }
}