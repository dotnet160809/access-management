namespace GB.HttpRequestMessagesStub.Internals;

internal sealed record HttpRequestMessageKey
{
    private readonly string _host;
    private readonly string _pathAndQuery;
    private readonly string _method;

    public HttpRequestMessageKey(string host, string pathAndQuery, HttpMethod method)
    {
        _host = host.ToLowerInvariant();
        _pathAndQuery = pathAndQuery.ToLowerInvariant();
        _method = method.ToString().ToUpperInvariant();
    }

    public bool Equals(HttpRequestMessageKey? other)
    {
        return other is not null
               && _host.OrdinalEquals(other._host)
               && _pathAndQuery.OrdinalEquals(other._pathAndQuery)
               && _method.OrdinalEquals(other._method);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            var hashCode = _host.GetOrdinalHashCode();
            hashCode = (hashCode * 397) ^ _pathAndQuery.GetHashCode();
            
            return (hashCode * 397) ^ _method.GetOrdinalHashCode();
        }
    }

    public override string ToString()
    {
        return $"{_method} {_host}{_pathAndQuery}";
    }

    public static explicit operator HttpRequestMessageKey(HttpRequestMessage request)
    {
        var requestUri = request.RequestUri!;
        var host = $"{requestUri.Scheme}://{requestUri.Authority}";
        
        return new HttpRequestMessageKey(host, requestUri.PathAndQuery, request.Method);
    }
}