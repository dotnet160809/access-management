using GB.AccessManagement.Application.Presentations;
using GB.AccessManagement.Domain.Services;

namespace GB.AccessManagement.WebApi.HalLinks.CompanyMembersHalLinks;

public sealed class CompanyMembersHalLinkBuilder : ISelfTransientService
{
    private readonly HalLinkHrefBuilder _builder;

    public CompanyMembersHalLinkBuilder(HalLinkHrefBuilder builder)
    {
        _builder = builder;
    }

    public CompanyMembersHalLinkBuilder BuildGetCompanyMembersLink(CompanyMembersPresentation presentation, Guid companyId)
    {
        _ = presentation.AddSelfLink(BuildGetCompanyMembersUri(companyId), HttpMethod.Get);

        return this;
    }

    public CompanyMembersHalLinkBuilder BuildPaginationLinks(CompanyMembersPresentation presentation, Guid companyId)
    {
        _ = presentation.AddPaginationLinks(BuildGetCompanyMembersUri(companyId), 1, 100);

        return this;
    }

    private string BuildGetCompanyMembersUri(Guid companyId)
    {
        return _builder.BuildCompanyMembersLink(companyId);
    }
}