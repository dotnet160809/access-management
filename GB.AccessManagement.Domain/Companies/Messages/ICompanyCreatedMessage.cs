using GB.AccessManagement.Domain.Messages;

namespace GB.AccessManagement.Domain.Companies.Messages;

public interface ICompanyCreatedMessage : IMessage
{
    Guid CompanyId { get; }
    
    string OwnerId { get; }
    
    string? ParentCompanyId { get; }
}