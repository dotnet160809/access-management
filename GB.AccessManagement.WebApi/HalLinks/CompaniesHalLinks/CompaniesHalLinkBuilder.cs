using GB.AccessManagement.Application.Presentations;
using GB.AccessManagement.Domain.Services;

namespace GB.AccessManagement.WebApi.HalLinks.CompaniesHalLinks;

public sealed class CompaniesHalLinkBuilder : ISelfTransientService
{
    private readonly HalLinkHrefBuilder _builder;

    public CompaniesHalLinkBuilder(HalLinkHrefBuilder builder)
    {
        _builder = builder;
    }

    public CompaniesHalLinkBuilder BuildGetCompaniesLink(CompaniesPresentation presentation, Guid companyId)
    {
        _ = presentation.AddSelfLink(BuildCompanyUri(companyId), HttpMethod.Get);

        return this;
    }

    public CompaniesHalLinkBuilder BuildGetCompaniesLink(CompaniesPresentation presentation, string userId)
    {
        _ = presentation.AddSelfLink(BuildUserCompaniesUri(userId), HttpMethod.Get);

        return this;
    }

    public CompaniesHalLinkBuilder BuildPaginationLinks(CompaniesPresentation presentation, Guid companyId, int currentPage, int itemsPerPage)
    {
        _ = presentation.AddPaginationLinks(BuildCompanyUri(companyId), currentPage, itemsPerPage);

        return this;
    }

    public CompaniesHalLinkBuilder BuildPaginationLinks(CompaniesPresentation presentation, string userId, int currentPage, int itemsPerPage)
    {
        _ = presentation.AddPaginationLinks(BuildUserCompaniesUri(userId), currentPage, itemsPerPage);

        return this;
    }

    private string BuildCompanyUri(Guid companyId)
    {
        return _builder.BuildCompanyLink(companyId);
    }

    private string BuildUserCompaniesUri(string userId)
    {
        return _builder.BuildUserCompaniesLink(userId);
    }
}