using GB.AccessManagement.Domain.Companies.Events;
using GB.AccessManagement.Domain.Companies.Messages;
using GB.AccessManagement.Domain.Messages;

namespace GB.AccessManagement.Application.Companies.CreateCompany;

public sealed class CompanyCreatedEventHandler : IDomainEventHandler<CompanyCreatedEvent>
{
    private readonly IPublishMessage _messages;

    public CompanyCreatedEventHandler(IPublishMessage messages)
    {
        _messages = messages;
    }

    public Task Handle(CompanyCreatedEvent @event, CancellationToken cancellationToken)
    {
        return _messages.Publish<ICompanyCreatedMessage>((CompanyCreatedMessage)@event, cancellationToken);
    }
}