namespace GB.AccessManagement.Tests.Endpoints.ListCompanyChildren;

public sealed class ListCompanyChildrenScenario(HttpClient client, int apiVersion)
{
    public Task<HttpResponseMessage> Execute(Guid companyId)
    {
        return client.GetAsync($"/v{apiVersion}/companies/{companyId}/children");
    }
}