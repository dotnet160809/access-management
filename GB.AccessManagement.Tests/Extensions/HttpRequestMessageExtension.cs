using System.Text.Json;

namespace GB.AccessManagement.Tests.Extensions;

public static class HttpRequestMessageExtension
{
    public static async Task<JsonElement> ReadRootJsonElement(this HttpRequestMessage request)
    {
        var content = request.Content ?? new StringContent(string.Empty);

        return JsonDocument.Parse(await content.ReadAsStringAsync()).RootElement;
    }
}