using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Domain.Companies.Exceptions;

[Serializable]
public sealed class NonExistentCompanyException : DomainException
{
    private const string MessagePattern = "Company {0} does not exist.";
    
    public NonExistentCompanyException(CompanyId id) : base(string.Format(MessagePattern, id)) { }
}