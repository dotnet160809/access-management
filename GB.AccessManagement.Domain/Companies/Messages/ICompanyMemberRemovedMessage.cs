using GB.AccessManagement.Domain.Messages;

namespace GB.AccessManagement.Domain.Companies.Messages;

public interface ICompanyMemberRemovedMessage : IMessage
{
    Guid CompanyId { get; }
    
    string MemberId { get; }
}