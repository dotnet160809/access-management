using GB.AccessManagement.Domain.Aggregates;
using GB.AccessManagement.Domain.Aggregates.Memos;
using GB.AccessManagement.Domain.Companies.Events;

namespace GB.AccessManagement.Domain.Companies;

public sealed partial class CompanyAggregate :
    IEventApplierAggregate<CompanyCreatedEvent, ICompanyMemo>,
    IEventApplierAggregate<CompanyMemberAddedEvent, ICompanyMemo>,
    IEventApplierAggregate<CompanyMemberRemovedEvent, ICompanyMemo>,
    IEventApplierAggregate<CompanyChildAddedEvent, ICompanyMemo>,
    IEventApplierAggregate<CompanyDeletedEvent, ICompanyMemo>
{
    public void Apply(CompanyCreatedEvent @event, ICompanyMemo memo)
    {
        memo.State = EMemoState.Created;
        memo.Id = @event.Id;
        memo.Name = @event.Name;
        memo.OwnerId = @event.OwnerId;
        memo.ParentCompanyId = @event.ParentCompanyId;
    }

    public void Apply(CompanyMemberAddedEvent @event, ICompanyMemo memo)
    {
        memo.State = EMemoState.Unchanged;
    }

    public void Apply(CompanyMemberRemovedEvent @event, ICompanyMemo memo)
    {
        memo.State = EMemoState.Unchanged;
    }

    public void Apply(CompanyChildAddedEvent @event, ICompanyMemo memo)
    {
        memo.State = EMemoState.Unchanged;
        memo.ParentCompanyId = @event.ParentCompanyId;
    }

    public void Apply(CompanyDeletedEvent @event, ICompanyMemo memo)
    {
        memo.State = EMemoState.Deleted;
    }
}