using System.Text.Json.Serialization;
using GB.HttpRequestMessagesStub;

namespace GB.AccessManagement.Tests.Models;

public sealed record TestHalLink([property: JsonPropertyName("href")] string Href, [property: JsonPropertyName("method")] string Method)
{
    public override int GetHashCode()
    {
        unchecked
        {
            var hashCode = Href.GetOrdinalHashCode();
            
            return (hashCode * 397) ^ Method.GetOrdinalHashCode();
        }
    }

    public bool Equals(TestHalLink? other)
    {
        return other is not null
               && Href.OrdinalEquals(other.Href)
               && Method.OrdinalEquals(other.Method);
    }

    public static TestHalLink Get(string href)
    {
        return new TestHalLink(href, "GET");
    }

    public static TestHalLink Post(string href)
    {
        return new TestHalLink(href, "POST");
    }
}