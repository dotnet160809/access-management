namespace GB.AccessManagement.Infrastructure.OpenFga.Extensions;

public static class HashSetExtension
{
    public static void AddRange<TItem>(this HashSet<TItem> source, IEnumerable<TItem> items)
    {
        foreach (var item in items)
        {
            source.Add(item);
        }
    }
}