using System.Security.Claims;
using GB.AccessManagement.Domain.Services;

namespace GB.AccessManagement.WebApi.Configurations.Authentication;

public sealed class DummyTokenValidator : IValidateAuthenticationToken, ITransientService
{
    private const string DefaultUserId = "a2f7cc44-7646-4d0d-acff-156340db62a5";

    public (bool isValid, ClaimsPrincipal? Principal) Validate(string[] headerValues)
    {
        if (!IsTokenValid(headerValues))
        {
            return (false, null);
        }

        var claims = new Claim[] { new(ClaimTypes.NameIdentifier, GetPrincipalId(headerValues.Last())) };
        var identity = new ClaimsIdentity(claims, DummyAuthenticationOptions.AuthenticationScheme);

        return (true, new ClaimsPrincipal(identity));
    }

    private static bool IsTokenValid(string[] headerValues)
    {
        return headerValues
            .Last()
            .Equals("token", StringComparison.OrdinalIgnoreCase);
    }

    private static string GetPrincipalId(string token)
    {
        return Guid.TryParse(token, out var userId)
            ? userId.ToString()
            : DefaultUserId;
    }
}