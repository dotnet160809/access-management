using System.Net;
using GB.AccessManagement.Tests.Assertions;
using GB.AccessManagement.Tests.Extensions;
using GB.AccessManagement.Tests.Models;
using NFluent;
using NFluent.Json;
using Xunit.Abstractions;

namespace GB.AccessManagement.Tests.Endpoints.GetCompanyParent;

public sealed class GetCompanyParentEndpointShould : AbstractFeatureShould<GetCompanyParentScenarioBuilder>
{
    public GetCompanyParentEndpointShould(ITestOutputHelper outputHelper, GetCompanyParentScenarioBuilder builder) : base(outputHelper, builder)
    {
    }

    [Theory]
    [InlineData(1)]
    public async Task ReturnUnauthorizedWhenNoAuthenticatedIsProvided(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var company)
            .Build(apiVersion);

        // Act
        var response = await scenario.Execute(company.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.Unauthorized);
    }

    [Theory]
    [InlineData(1)]
    public async Task ReturnForbiddenWhenUserIsNotCompanyMember(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var company)
            .CreateUser(out var user)
            .AuthenticateUser(user)
            .Build(apiVersion);
        
        // Act
        var response = await scenario.Execute(company.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.Forbidden);
    }

    [Theory]
    [InlineData(1)]
    public async Task ReturnNotFoundWhenCompanyParentHasNotBeenFound(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var company)
            .CreateUser(out var member)
            .AddCompanyMember(company.Id, member.Id)
            .AuthenticateUser(member)
            .Build(apiVersion);
        
        // Act
        var response = await scenario.Execute(company.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.NotFound);
    }

    [Theory]
    [InlineData(1, false)]
    [InlineData(1, true)]
    public async Task ReturnExpectedResponse(int apiVersion, bool isUserParentCompanyOwner)
    {
        // Arrange
        _ = ScenarioBuilder
            .CreateCompany(out var parentCompany)
            .CreateCompany(parentCompany.Id, out var company)
            .CreateUser(out var user)
            .AddCompanyMember(company.Id, user.Id);
        
        _ = isUserParentCompanyOwner
            ? ScenarioBuilder.AddCompanyOwner(parentCompany.Id, user.Id)
            : ScenarioBuilder.AddCompanyMember(parentCompany.Id, user.Id);

        var scenario = ScenarioBuilder
            .AuthenticateUser(user)
            .Build(apiVersion);

        // Act
        var response = await scenario.Execute(company.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.OK);

        var document = await response.ReadRootJsonElement();
        Check.That(document).HasGuidProperty("id", parentCompany.Id);
        Check.That(document).HasStringProperty("name", parentCompany.Name);

        var expectedLinks = new TestHalLinks
        {
            ["self"] = TestHalLink.Get($"/v{apiVersion}/companies/{parentCompany.Id}"),
            ["get_owner"] = TestHalLink.Get($"/v{apiVersion}/companies/{parentCompany.Id}/owner"),
            ["get_members"] = TestHalLink.Get($"/v{apiVersion}/companies/{parentCompany.Id}/members"),
            ["get_children"] = TestHalLink.Get($"/v{apiVersion}/companies/{parentCompany.Id}/children")
        };

        if (isUserParentCompanyOwner)
        {
            expectedLinks.Add("add_member", TestHalLink.Post($"/v{apiVersion}/companies/{parentCompany.Id}/members"));
        }
        
        Check.That(document).HasHalLinks(expectedLinks);
    }
}