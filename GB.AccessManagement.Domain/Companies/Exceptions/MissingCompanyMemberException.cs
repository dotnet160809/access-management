using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Domain.Companies.Exceptions;

[Serializable]
public sealed class MissingCompanyMemberException : DomainException
{
    private const string MessagePattern = "User '{0}' is not a member of company '{1}'.";
    
    public MissingCompanyMemberException(UserId memberId, CompanyId companyId)
        : base(string.Format(MessagePattern, memberId, companyId)) { }
}