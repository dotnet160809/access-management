using OpenFga.Sdk.Model;

namespace GB.AccessManagement.Infrastructure.OpenFga.Visitors.Extensions;

internal static class UsersExtension
{
    public static async Task<string[]> Accept(this Users? users, IVisitExpandResponse visitor)
    {
        return users is not null && users._Users.Count != 0
            ? await visitor.Visit(users)
            : [];
    }
}