using Microsoft.Extensions.Options;

namespace GB.HttpRequestMessagesStub.Internals;

internal sealed class HttpRequestMessageDelegatingHandler : DelegatingHandler
{
    private readonly IOptionsMonitor<HttpRequestMessageDelegatingHandlerOptions> _delegatingOptions;
    private readonly IOptionsMonitor<HttpRequestMessageAssertionOptions> _assertionOptions;

    public HttpRequestMessageDelegatingHandler(
        IOptionsMonitor<HttpRequestMessageDelegatingHandlerOptions> delegatingOptions,
        IOptionsMonitor<HttpRequestMessageAssertionOptions> assertionOptions)
    {
        _delegatingOptions = delegatingOptions;
        _assertionOptions = assertionOptions;
    }

    protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
    {
        var key = (HttpRequestMessageKey)request;

        if (!_delegatingOptions.CurrentValue.TryGetValue(key, out var responseDelegate))
        {
            throw new HttpResponseStubNotFoundException(key);
        }

        if (_assertionOptions.CurrentValue.TryGetValue(key, out var contents))
        {
            contents.Add(request.Content);

            return responseDelegate.Invoke(request);
        }

        _assertionOptions.CurrentValue[key] = new List<HttpContent?> { request.Content };

        return responseDelegate.Invoke(request);
    }
}