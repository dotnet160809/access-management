using System.Net;
using GB.AccessManagement.Tests.Assertions;
using GB.AccessManagement.Tests.Extensions;
using GB.AccessManagement.Tests.Models;
using NFluent;
using NFluent.Json;
using Xunit.Abstractions;

namespace GB.AccessManagement.Tests.Endpoints.GetCompany;

public sealed class GetCompanyEndpointShould : AbstractFeatureShould<GetCompanyScenarioBuilder>
{
    public GetCompanyEndpointShould(ITestOutputHelper outputHelper, GetCompanyScenarioBuilder builder)
        : base(outputHelper, builder)
    {
    }

    [Theory]
    [InlineData(1)]
    public async Task ReturnUnauthorizedWhenUserIsNotAuthenticated(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder.Build(apiVersion);

        // Act
        var response = await scenario.Execute(Guid.NewGuid());

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.Unauthorized);
    }

    [Theory]
    [InlineData(1)]
    public async Task ReturnForbiddenWhenUserIsNotCompanyMember(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var company)
            .CreateUser(out var user)
            .RemoveCompanyMember(company.Id, user.Id)
            .AuthenticateUser(user)
            .Build(apiVersion);

        // Act
        var response = await scenario.Execute(company.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.Forbidden);
    }

    [Theory]
    [InlineData(1, false)]
    [InlineData(1, true)]
    public async Task ReturnExpectedResponse(int apiVersion, bool isUserCompanyOwner)
    {
        // Arrange
        _ = ScenarioBuilder
            .CreateCompany(out var company)
            .CreateUser(out var user);

        _ = isUserCompanyOwner
            ? ScenarioBuilder.AddCompanyOwner(company.Id, user.Id)
            : ScenarioBuilder.AddCompanyMember(company.Id, user.Id);

        var scenario = ScenarioBuilder
            .AuthenticateUser(user)
            .Build(apiVersion);

        // Act
        var response = await scenario.Execute(company.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.OK);

        var document = await response.ReadRootJsonElement();
        Check.That(document).HasStringProperty("id", company.Id.ToString());
        Check.That(document).HasStringProperty("name", company.Name);

        var expectedLinks = new TestHalLinks
        {
            ["self"] = TestHalLink.Get($"/v1/companies/{company.Id}"),
            ["get_owner"] = TestHalLink.Get($"/v1/companies/{company.Id}/owner")
        };

        if (isUserCompanyOwner)
        {
            expectedLinks.Add("add_member", TestHalLink.Post($"/v1/companies/{company.Id}/members"));
        }

        Check.That(document).HasHalLinks(expectedLinks);
    }
}