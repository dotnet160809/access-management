using GB.AccessManagement.Application.Presentations;
using GB.AccessManagement.Domain.Services;

namespace GB.AccessManagement.WebApi.HalLinks.CompanyHalLinks;

public sealed class CompanyHalLinkBuilder : ISelfTransientService
{
    private readonly HalLinkHrefBuilder _builder;

    public CompanyHalLinkBuilder(HalLinkHrefBuilder builder)
    {
        _builder = builder;
    }

    public CompanyHalLinkBuilder BuildGetCompanyLink(CompanyPresentation presentation)
    {
        _ = presentation.AddSelfLink(_builder.BuildCompanyLink(presentation.Id), HttpMethod.Get);

        return this;
    }

    public CompanyHalLinkBuilder BuildGetOwnerLink(CompanyPresentation presentation)
    {
        _ = presentation.AddLink("get_owner", _builder.BuildCompanyOwnerLink(presentation.Id), HttpMethod.Get);

        return this;
    }

    public CompanyHalLinkBuilder BuildGetParentLink(CompanyPresentation presentation)
    {
        _ = presentation.AddLink("get_parent", _builder.BuildCompanyParentLink(presentation.Id), HttpMethod.Get);

        return this;
    }

    public CompanyHalLinkBuilder BuildGetChildrenLink(CompanyPresentation presentation)
    {
        _ = presentation.AddLink("get_children", _builder.BuildCompanyChildrenLink(presentation.Id), HttpMethod.Get);

        return this;
    }

    public CompanyHalLinkBuilder BuildAddMemberLink(CompanyPresentation presentation)
    {
        _ = presentation.AddLink("add_member", BuildMembersLink(presentation.Id), HttpMethod.Post);

        return this;
    }

    public CompanyHalLinkBuilder BuildGetMembersLink(CompanyPresentation presentation)
    {
        _ = presentation.AddLink("get_members", BuildMembersLink(presentation.Id), HttpMethod.Get);

        return this;
    }

    private string BuildMembersLink(Guid companyId)
    {
        return _builder.BuildCompanyMembersLink(companyId);
    }
}