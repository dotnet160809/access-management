using MediatR;

namespace GB.AccessManagement.Application.Commands;

public interface ICommand : IRequest { }

public interface ICommand<out TResponse> : IRequest<TResponse> { }