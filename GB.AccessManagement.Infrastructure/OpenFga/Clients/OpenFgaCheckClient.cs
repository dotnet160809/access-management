using GB.AccessManagement.Domain;
using GB.AccessManagement.Domain.Services;
using GB.AccessManagement.Domain.ValueObjects;
using Microsoft.Extensions.Options;
using OpenFga.Sdk.Model;

namespace GB.AccessManagement.Infrastructure.OpenFga.Clients;

public sealed class OpenFgaCheckClient : AbstractOpenFgaClient, IEvaluateCompanyAccesses, ITransientService
{
    public OpenFgaCheckClient(IHttpClientFactory factory, IOptionsMonitor<OpenFgaOptions> options)
        : base(factory, options)
    {
    }

    public async Task<bool> WithRelation(UserId userId, Relation relation, CompanyId companyId)
    {
        var access = new Access("user", userId.ToString(), relation, "company", companyId.ToString());
        var response = await Post<CheckResponse>(CheckPath(), new CheckRequest
        {
            TupleKey = new CheckRequestTupleKey
            {
                Object = access.Object,
                Relation = access.Relation,
                User = access.User
            }
        });

        return response.Allowed == true;
    }

    private string CheckPath()
    {
        return $"/stores/{StoreId()}/check";
    }
}