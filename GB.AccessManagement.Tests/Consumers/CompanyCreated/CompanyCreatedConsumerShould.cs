using GB.AccessManagement.Tests.Assertions;
using GB.AccessManagement.Tests.Extensions;
using GB.AccessManagement.Tests.Models;
using GB.HttpRequestMessagesStub;
using NFluent;
using Xunit.Abstractions;

namespace GB.AccessManagement.Tests.Consumers.CompanyCreated;

public sealed class CompanyCreatedConsumerShould : AbstractFeatureShould<CompanyCreatedScenarioBuilder>
{
    public CompanyCreatedConsumerShould(ITestOutputHelper outputHelper, CompanyCreatedScenarioBuilder builder) : base(outputHelper, builder)
    {
    }

    [Fact]
    public async Task NotCreateAnyAuthorizationWhenConsumingUnauthenticatedMessages()
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateMessageContext(out var context)
            .Build();

        // Act
        await scenario.Execute(context);

        // Assert
        Check.That(Authorizations()).HasNotBeenCreated();
    }

    [Fact]
    public async Task NotCreateAnyAuthorizationWhenConsumingForbiddenMessages()
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateForbiddenMessageContext(out var context)
            .Build();

        // Act
        await scenario.Execute(context);

        // Assert
        Check.That(Authorizations()).HasNotBeenCreated();
    }

    [Fact]
    public async Task CreateExpectedAuthorizationsWhenConsumingAuthenticatedMessage()
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateUser(out var user)
            .CreateCompany(out var company)
            .CreateAuthenticatedMessageContext(company.Id, user.Id, out var context)
            .Build();

        // Act
        await scenario.Execute(context);

        // Assert
        await Authorizations().HasBeenCreated(ForCompanyOwner(company.Id, user.Id));
    }

    private static HttpRequestAssertionDelegate ForCompanyOwner(Guid companyId, Guid userId)
    {
        return async content =>
        {
            var document = await content.ReadAsJsonRootElement();
            Check.That(document).HasCreatedTupleKeys(new TestTupleKey($"user:{userId}", "owner", $"company:{companyId}"));
        };
    }
}