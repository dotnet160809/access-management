using GB.AccessManagement.Application.Presentations;
using GB.AccessManagement.Application.Queries;
using GB.AccessManagement.Application.Users.ListUserCompanies;
using GB.AccessManagement.Domain.Services;

namespace GB.AccessManagement.WebApi.HalLinks.CompanyMemberHalLinks;

public sealed class CompanyMemberHalLinkDirector : ISelfTransientService
{
    private readonly IQueryDispatcher _dispatcher;
    private readonly CompanyMemberHalLinkBuilder _builder;

    public CompanyMemberHalLinkDirector(IQueryDispatcher dispatcher, CompanyMemberHalLinkBuilder builder)
    {
        _dispatcher = dispatcher;
        _builder = builder;
    }

    public async Task<CompanyMemberPresentation> MakeHalLinks(CompanyMemberPresentation presentation, CancellationToken cancellationToken)
    {
        var userCompanies = await GetUserCompanies(presentation.Id, cancellationToken);

        if (userCompanies.Total == 0)
        {
            return presentation;
        }

        _ = _builder.BuildCompaniesHalLink(presentation);

        return presentation;
    }

    private Task<CompaniesPresentation> GetUserCompanies(Guid userId, CancellationToken cancellationToken)
    {
        UserCompaniesQuery query = new(userId);

        return _dispatcher.Dispatch(query, cancellationToken);
    }
}