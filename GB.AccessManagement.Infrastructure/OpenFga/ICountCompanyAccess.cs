namespace GB.AccessManagement.Infrastructure.OpenFga;

public interface ICountCompanyAccess
{
    void WhenCreated();

    void WhenDeleted();
}