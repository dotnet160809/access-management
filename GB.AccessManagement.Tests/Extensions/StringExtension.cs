namespace GB.AccessManagement.Tests.Extensions;

public static class StringExtension
{
    public static string ExtractId(this string? value)
    {
        return value?.Split(':').Last() ?? string.Empty;
    }
}