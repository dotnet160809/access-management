using GB.AccessManagement.Domain;
using GB.AccessManagement.Domain.Companies.Messages;
using GB.AccessManagement.Domain.Services;
using MassTransit;
using Microsoft.Extensions.Options;

namespace GB.AccessManagement.Infrastructure.MassTransit.Consumers;

public sealed class CompanyMemberAddedMessageConsumer : AbstractConsumer<ICompanyMemberAddedMessage>, ISelfTransientService
{
    private readonly ICreateCompanyAccess _companyAccesses;

    public CompanyMemberAddedMessageConsumer(
        IOptionsMonitor<MessageQueueOptions> options,
        ICreateCompanyAccess companyAccesses)
        : base(options)
    {
        _companyAccesses = companyAccesses;
    }

    public override Task Consume(ConsumeContext<ICompanyMemberAddedMessage> context)
    {
        return ContextIsNotAuthenticated(context) 
            ? Task.CompletedTask
            : _companyAccesses.AsMember(context.Message.MemberId, context.Message.CompanyId);
    }
}