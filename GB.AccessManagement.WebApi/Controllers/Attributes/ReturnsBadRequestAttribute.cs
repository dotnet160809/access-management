namespace GB.AccessManagement.WebApi.Controllers.Attributes;

public sealed class ReturnsBadRequestAttribute() : ProblemResponseTypeAttribute(StatusCodes.Status400BadRequest);