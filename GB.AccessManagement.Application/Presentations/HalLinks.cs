using System.Collections.ObjectModel;
using Swashbuckle.AspNetCore.Annotations;

namespace GB.AccessManagement.Application.Presentations;

[Serializable]
[SwaggerSchema(Title = nameof(HalLinks))]
public sealed class HalLinks : ReadOnlyDictionary<string, HalLink>
{
    public HalLinks(IDictionary<string, HalLink> dictionary) : base(dictionary)
    {
    }
}