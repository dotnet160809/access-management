using System.Net.Http.Json;
using Microsoft.Extensions.Options;

namespace GB.AccessManagement.Infrastructure.OpenFga.Clients;

public abstract class AbstractOpenFgaClient
{
    private readonly IHttpClientFactory _factory;
    private readonly IOptionsMonitor<OpenFgaOptions> _options;

    protected AbstractOpenFgaClient(IHttpClientFactory factory, IOptionsMonitor<OpenFgaOptions> options)
    {
        _factory = factory;
        _options = options;
    }
    
    protected HttpClient CreateClient()
    {
        return _factory.CreateClient(OpenFgaOptions.HttpClientName);
    }

    protected string StoreId()
    {
        return _options.CurrentValue.StoreId;
    }

    protected async Task<TResponse> Post<TResponse>(string path, object content) where TResponse : class, new()
    {
        var client = CreateClient();
        var httpResponse = await client.PostAsync(path, JsonContent.Create(content));
        httpResponse.EnsureSuccessStatusCode();

        return await httpResponse.Content.ReadFromJsonAsync<TResponse>() ?? new TResponse();
    }
}