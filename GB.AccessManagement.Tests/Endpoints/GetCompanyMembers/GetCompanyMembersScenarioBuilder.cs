using GB.AccessManagement.Tests.Extensions;
using Microsoft.Extensions.DependencyInjection;

namespace GB.AccessManagement.Tests.Endpoints.GetCompanyMembers;

public sealed class GetCompanyMembersScenarioBuilder : AbstractEndpointScenarioBuilder<GetCompanyMembersScenarioBuilder>
{
    public GetCompanyMembersScenario Build(int apiVersion, Guid companyId, Guid userId)
    {
        _ = OpenFga()
            .CheckReturns(CompanyOwnerOrMembers)
            .ExpandReturns(CompanyMembers(companyId))
            .ListObjectsReturns(UserCompanies(userId));

        return new GetCompanyMembersScenario(CreateClient(), apiVersion);
    }

    protected override void ConfigureServices(IServiceCollection services)
    {
        _ = services
            .StubAuthentication()
            .AddCompanyFixture()
            .StubOpenFga();
    }
}