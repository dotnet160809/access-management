using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Domain.Companies.Providers;

public interface IProvideUserCompanyIds
{
    Task<CompanyId[]> ListCompaniesForUser(UserId userId);
}