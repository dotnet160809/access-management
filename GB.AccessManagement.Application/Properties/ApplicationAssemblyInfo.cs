using System.Reflection;

namespace GB.AccessManagement.Application.Properties;

public static class ApplicationAssemblyInfo
{
    public static Assembly Assembly => typeof(ApplicationAssemblyInfo).Assembly;
}