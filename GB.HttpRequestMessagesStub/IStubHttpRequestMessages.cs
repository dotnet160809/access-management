namespace GB.HttpRequestMessagesStub;

/// <summary>
/// Stub or assert http requests
/// </summary>
public interface IStubHttpRequestMessages
{
    /// <summary>
    /// Stub an http request using its host, path (inc. query parameters) and method
    /// </summary>
    /// <param name="host">The http request host</param>
    /// <param name="pathAndQuery">The http request path merged with query parameters</param>
    /// <param name="method">The http request method</param>
    /// <param name="clearPreviousStubs">Indicates if all previous calls must be cleared for assertions</param>
    /// <returns>An <see cref="IBuildHttpResponseMessages"/> implementation linked to the http request</returns>
    IBuildHttpResponseMessages StubHttpRequest(string host, string pathAndQuery, HttpMethod method, bool clearPreviousStubs = true);
    
    /// <summary>
    /// Assert http requests using its host, path (inc. query parameters) and method
    /// </summary>
    /// <param name="host">The http request host</param>
    /// <param name="pathAndQuery">The http request path merged with query parameters</param>
    /// <param name="method">The http request method</param>
    /// <returns>An <see cref="IAssertHttpRequestMessages"/> implementation linked to the http request</returns>
    IAssertHttpRequestMessages AssertHttpRequest(string host, string pathAndQuery, HttpMethod method);
}