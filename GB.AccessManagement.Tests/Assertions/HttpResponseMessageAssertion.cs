using System.Net;
using NFluent;
using NFluent.Extensibility;

namespace GB.AccessManagement.Tests.Assertions;

public static class HttpResponseMessageAssertion
{
    public static ICheckLink<ICheck<HttpResponseMessage>> HasStatusCode(this ICheck<HttpResponseMessage> check, HttpStatusCode expectedStatusCode)
    {
        ExtensibilityHelper.BeginCheck(check)
            .FailWhen(response => response.StatusCode != expectedStatusCode, $"Status code does not match expected '{expectedStatusCode}'.")
            .OnNegate("Http status code comparison failed.")
            .EndCheck();

        return ExtensibilityHelper.BuildCheckLink(check);
    }

    public static void HasHeader(this ICheck<HttpResponseMessage> check, string headerName)
    {
        ExtensibilityHelper.BeginCheck(check)
            .FailWhen(response => !response.Headers.Contains(headerName), $"Response does not contain header named '{headerName}'.")
            .OnNegate("Header search failed.")
            .EndCheck();
        
        _ = ExtensibilityHelper.BuildCheckLink(check);
    }

    public static void HasHeaderValue(this ICheck<HttpResponseMessage> check, string headerName, string headerValue)
    {
        ExtensibilityHelper.BeginCheck(check)
            .FailWhen(response => !response.Headers.Contains(headerName), $"Response does not contain header named '{headerName}'.")
            .FailWhen(
                response => !response.Headers.TryGetValues(headerName, out var values) && values?.Contains(headerValue) == true,
                $"Header value does not match expected '{headerValue}'.")
            .OnNegate("Header search failed, either header has not been found or value does not match expected one.")
            .EndCheck();
        
        _ = ExtensibilityHelper.BuildCheckLink(check);
    }
}