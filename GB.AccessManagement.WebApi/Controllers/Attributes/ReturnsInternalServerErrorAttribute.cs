namespace GB.AccessManagement.WebApi.Controllers.Attributes;

public sealed class ReturnsInternalServerErrorAttribute() : ProblemResponseTypeAttribute(StatusCodes.Status500InternalServerError);