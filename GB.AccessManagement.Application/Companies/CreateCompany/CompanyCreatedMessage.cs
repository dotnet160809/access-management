using GB.AccessManagement.Domain.Companies.Events;
using GB.AccessManagement.Domain.Companies.Messages;

namespace GB.AccessManagement.Application.Companies.CreateCompany;

[Serializable]
public sealed record CompanyCreatedMessage(Guid CompanyId, string OwnerId, string? ParentCompanyId) : ICompanyCreatedMessage
{
    public Guid Id { get; } = Guid.NewGuid();

    public static explicit operator CompanyCreatedMessage(CompanyCreatedEvent @event)
    {
        return new CompanyCreatedMessage((Guid)@event.Id, @event.OwnerId.ToString(), @event.ParentCompanyId?.ToString());
    }
}