using MassTransit;

namespace GB.AccessManagement.Infrastructure.MassTransit.ConsumeObservers;

public sealed class ConsumeMessageLogger : IConsumeObserver
{
    private readonly ILogConsumeMessage _logger;

    public ConsumeMessageLogger(ILogConsumeMessage logger)
    {
        _logger = logger;
    }

    public Task PreConsume<T>(ConsumeContext<T> context) where T : class
    {
        return Task.CompletedTask;
    }

    public Task PostConsume<T>(ConsumeContext<T> context) where T : class
    {
        _logger.MessageConsumed(context.Message);

        return Task.CompletedTask;
    }

    public Task ConsumeFault<T>(ConsumeContext<T> context, Exception exception) where T : class
    {
        _logger.UnableToConsumeMessage(context.Message);

        return Task.CompletedTask;
    }
}