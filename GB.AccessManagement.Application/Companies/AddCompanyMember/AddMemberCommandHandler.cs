using GB.AccessManagement.Application.Commands;

namespace GB.AccessManagement.Application.Companies.AddCompanyMember;

public sealed class AddMemberCommandHandler : ICommandHandler<AddMemberCommand>
{
    private readonly ICompanyStore _store;

    public AddMemberCommandHandler(ICompanyStore store)
    {
        _store = store;
    }

    public async Task Handle(AddMemberCommand command, CancellationToken cancellationToken)
    {
        var aggregate = await _store.Load(command.CompanyId);
        aggregate.AddMember(command.MemberId);
        await _store.Save(aggregate, cancellationToken);
    }
}