using GB.AccessManagement.Application.Commands;
using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Application.Companies.RemoveCompanyMember;

public sealed record RemoveMemberCommand : ICommand
{
    internal readonly CompanyId CompanyId;
    internal readonly UserId MemberId;

    public RemoveMemberCommand(Guid companyId, Guid memberId)
    {
        CompanyId = companyId;
        MemberId = memberId;
    }
}