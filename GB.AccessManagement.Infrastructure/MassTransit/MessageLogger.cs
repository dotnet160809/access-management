using System.Text.Json;
using GB.AccessManagement.Domain.Services;
using GB.AccessManagement.Infrastructure.MassTransit.ConsumeObservers;
using GB.AccessManagement.Infrastructure.MassTransit.PublishObservers;
using Microsoft.Extensions.Logging;

namespace GB.AccessManagement.Infrastructure.MassTransit;

public sealed partial class MessageLogger : ILogPublishMessage, ILogConsumeMessage, ITransientService
{
    private readonly ILogger _logger;

    public MessageLogger(ILoggerFactory factory)
    {
        _logger = factory.CreateLogger("AccessManagement.Messages");
    }

    public void MessagePublished<TMessage>(TMessage message) where TMessage : class
    {
        if (!_logger.IsEnabled(LogLevel.Information))
        {
            return;
        }
        
        MessagePublished(_logger, typeof(TMessage).Name, JsonSerializer.Serialize(message));
    }

    public void UnableToPublishMessage<TMessage>(TMessage message) where TMessage : class
    {
        if (!_logger.IsEnabled(LogLevel.Warning))
        {
            return;
        }
        
        UnableToPublishMessage(_logger, typeof(TMessage).Name, JsonSerializer.Serialize(message));
    }

    public void MessageConsumed<TMessage>(TMessage message) where TMessage : class
    {
        if (!_logger.IsEnabled(LogLevel.Information))
        {
            return;
        }
        
        MessageConsumed(_logger, typeof(TMessage).Name, JsonSerializer.Serialize(message));
    }

    public void UnableToConsumeMessage<TMessage>(TMessage message) where TMessage : class
    {
        if (!_logger.IsEnabled(LogLevel.Warning))
        {
            return;
        }
        
        UnableToConsumeMessage(_logger, typeof(TMessage).Name, JsonSerializer.Serialize(message));
    }

    [LoggerMessage(Level = LogLevel.Information, Message = "Published message of type '{Name}' with payload {Payload}")]
    private static partial void MessagePublished(ILogger logger, string name, string payload);

    [LoggerMessage(Level = LogLevel.Warning, Message = "Unable to publish message of type '{Name}' with payload {Payload}")]
    private static partial void UnableToPublishMessage(ILogger logger, string name, string payload);

    [LoggerMessage(Level = LogLevel.Information, Message = "Consumed message of type '{Name}' with payload {Payload}")]
    private static partial void MessageConsumed(ILogger logger, string name, string payload);

    [LoggerMessage(Level = LogLevel.Warning, Message = "Unable to consume message of type '{Name}' with payload {Payload}")]
    private static partial void UnableToConsumeMessage(ILogger logger, string name, string payload);
}