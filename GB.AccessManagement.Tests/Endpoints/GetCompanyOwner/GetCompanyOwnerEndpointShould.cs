using System.Net;
using GB.AccessManagement.Tests.Assertions;
using GB.AccessManagement.Tests.Extensions;
using GB.AccessManagement.Tests.Models;
using NFluent;
using NFluent.Json;
using Xunit.Abstractions;

namespace GB.AccessManagement.Tests.Endpoints.GetCompanyOwner;

public sealed class GetCompanyOwnerEndpointShould : AbstractFeatureShould<GetCompanyOwnerScenarioBuilder>
{
    public GetCompanyOwnerEndpointShould(ITestOutputHelper outputHelper, GetCompanyOwnerScenarioBuilder builder)
        : base(outputHelper, builder)
    {
    }

    [Theory]
    [InlineData(1)]
    public async Task ReturnUnauthorizedWhenNotAuthenticationIsProvided(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var company)
            .Build(apiVersion, company.Id, Guid.NewGuid());

        // Act
        var response = await scenario.Execute(company.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.Unauthorized);
    }

    [Theory]
    [InlineData(1)]
    public async Task ReturnForbiddenWhenUserIsNotCompanyMember(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var company)
            .CreateUser(out var user)
            .AuthenticateUser(user)
            .Build(apiVersion, company.Id, user.Id);

        // Act
        var response = await scenario.Execute(company.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.Forbidden);
    }

    [Theory]
    [InlineData(1, false)]
    [InlineData(1, true)]
    public async Task ReturnExpectedResponse(int apiVersion, bool isCompanyOwnerAuthenticated)
    {
        // Arrange
        _ = ScenarioBuilder
            .CreateCompany(out var company)
            .CreateUser(out var owner)
            .AddCompanyOwner(company.Id, owner.Id)
            .CreateUser(out var member)
            .AddCompanyMember(company.Id, member.Id);

        _ = isCompanyOwnerAuthenticated
            ? ScenarioBuilder.AuthenticateUser(owner)
            : ScenarioBuilder.AuthenticateUser(member);

        var scenario = ScenarioBuilder.Build(apiVersion, company.Id, owner.Id);

        // Act
        var response = await scenario.Execute(company.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.OK);

        var jsonElement = await response.ReadRootJsonElement();
        Check.That(jsonElement).HasGuidProperty("id", owner.Id);

        var expectedHalLinks = new TestHalLinks
        {
            ["get_companies"] = TestHalLink.Get($"/v{apiVersion}/users/{owner.Id}/companies")
        };
        Check.That(jsonElement).HasHalLinks(expectedHalLinks);
    }
}