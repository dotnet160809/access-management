namespace GB.AccessManagement.Tests.Models;

public sealed record TestUser(Guid Id, string Email);