using FluentMigrator;

namespace GB.AccessManagement.Infrastructure.Database.Migrations;

[TimestampedMigration(2024, 1, 15, 10, 15)]
public sealed class AddCompanyParentColumnMigration : ForwardOnlyMigration
{
    public override void Up()
    {
        Alter
            .Table(CompanyDao.TableName)
            .AddColumn(CompanyDao.ParentIdColumnName)
            .AsGuid()
            .Nullable();
    }
}