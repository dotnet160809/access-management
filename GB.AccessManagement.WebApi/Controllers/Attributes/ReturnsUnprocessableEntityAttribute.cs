namespace GB.AccessManagement.WebApi.Controllers.Attributes;

public sealed class ReturnsUnprocessableEntityAttribute() : ProblemResponseTypeAttribute(StatusCodes.Status422UnprocessableEntity);