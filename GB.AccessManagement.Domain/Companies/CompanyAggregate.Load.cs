using GB.AccessManagement.Domain.Services;

namespace GB.AccessManagement.Domain.Companies;

public sealed partial class CompanyAggregate
{
    public sealed class CompanyLoader : ILoadCompany, ITransientService
    {
        public CompanyAggregate Load(ICompanyMemo memo)
        {
            var aggregate = new CompanyAggregate(memo.Id, memo.Name, memo.OwnerId, memo.ParentCompanyId);

            if (memo.Children.Count != 0)
            {
                aggregate._children.AddRange(memo.Children);
            }
                
            if (memo.Members.Count != 0)
            {
                aggregate._members.AddRange(memo.Members);
            }

            return aggregate;
        }
    }
}