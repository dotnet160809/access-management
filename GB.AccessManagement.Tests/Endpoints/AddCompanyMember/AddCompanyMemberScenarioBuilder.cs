using GB.AccessManagement.Tests.Extensions;
using Microsoft.Extensions.DependencyInjection;

namespace GB.AccessManagement.Tests.Endpoints.AddCompanyMember;

public sealed class AddCompanyMemberScenarioBuilder : AbstractEndpointScenarioBuilder<AddCompanyMemberScenarioBuilder>
{
    public AddCompanyMemberScenario Build(int apiVersion)
    {
        _ = OpenFga()
            .CheckReturns(CompanyOwnerOrMembers)
            .ExpandReturns(CompanyOwnerOrMemberIds)
            .ListObjectsReturns([])
            .WriteReturnsOk();

        return new AddCompanyMemberScenario(CreateClient(), apiVersion);
    }

    protected override void ConfigureServices(IServiceCollection services)
    {
        _ = services
            .StubAuthentication()
            .AddCompanyFixture()
            .StubOpenFga();
    }
}