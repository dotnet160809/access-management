using System.Text.Json;
using System.Text.RegularExpressions;

namespace GB.AccessManagement.Tests.Extensions;

public static class HttpResponseExtension
{
    private static readonly Regex Expression = new("[0-9a-fA-F]{8}[-]{1}([0-9a-fA-F]{4}[-]{1}){3}[0-9a-fA-F]{12}", RegexOptions.Compiled);

    public static async Task<JsonElement> ReadRootJsonElement(this HttpResponseMessage response)
    {
        return JsonDocument.Parse(await response.Content.ReadAsStringAsync()).RootElement;
    }
    
    public static Guid ExtractGuidFromHeader(this HttpResponseMessage response, string headerName)
    {
        var values = response.Headers.GetValues(headerName);
        var match = Expression.Match(values.Last());
        
        return match.Success && Guid.TryParse(match.Value, out var value) ? value : Guid.Empty;
    }
}