using GB.AccessManagement.Tests.Extensions;
using Microsoft.Extensions.DependencyInjection;

namespace GB.AccessManagement.Tests.Endpoints.GetCompanyOwner;

public sealed class GetCompanyOwnerScenarioBuilder : AbstractEndpointScenarioBuilder<GetCompanyOwnerScenarioBuilder>
{
    public GetCompanyOwnerScenario Build(int apiVersion, Guid companyId, Guid userId)
    {
        _ = OpenFga()
            .CheckReturns(CompanyOwnerOrMembers)
            .ExpandReturns(CompanyOwner(companyId))
            .ListObjectsReturns(UserCompanies(userId));

        return new GetCompanyOwnerScenario(CreateClient(), apiVersion);
    }

    protected override void ConfigureServices(IServiceCollection services)
    {
        _ = services
            .StubAuthentication()
            .AddCompanyFixture()
            .StubOpenFga();
    }
}