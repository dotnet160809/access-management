using System.ComponentModel.DataAnnotations;

namespace GB.AccessManagement.WebApi.Controllers.Requests;

public sealed record AddMemberRequest
{
    [Required]
    public Guid MemberId { get; init; }
}