using GB.AccessManagement.Application.Presentations;
using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Application;

public interface IReadCompany
{
    Task<bool> Exist(CompanyId id);
    
    Task<CompanyPresentation[]> List(IEnumerable<CompanyId> ids);
}