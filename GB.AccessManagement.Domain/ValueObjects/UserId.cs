namespace GB.AccessManagement.Domain.ValueObjects;

public sealed record UserId
{
    private readonly Guid _value;

    private UserId(Guid value)
    {
        if (value == Guid.Empty)
        {
            throw new ArgumentNullException(nameof(value));
        }

        _value = value;
    }

    public override string ToString()
    {
        return _value.ToString();
    }

    public static implicit operator UserId(Guid value)
    {
        return new UserId(value);
    }

    public static explicit operator Guid(UserId userId)
    {
        return userId._value;
    }

    public static implicit operator UserId(string value)
    {
        value = value.Split(':').Last();

        return new UserId(Guid.Parse(value));
    }
}