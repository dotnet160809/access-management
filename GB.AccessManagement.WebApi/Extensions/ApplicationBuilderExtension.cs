using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Options;

namespace GB.AccessManagement.WebApi.Extensions;

public static class ApplicationBuilderExtension
{
    private static readonly Dictionary<HealthStatus, int> HealthStatusCodesMapping = new()
    {
        [HealthStatus.Healthy] = StatusCodes.Status200OK,
        [HealthStatus.Degraded] = StatusCodes.Status503ServiceUnavailable,
        [HealthStatus.Unhealthy] = StatusCodes.Status503ServiceUnavailable
    };

    public static IApplicationBuilder UseHealthCheckEndpoints(this IApplicationBuilder app)
    {
        return app
            .UseHealthChecks("/health/liveness", new HealthCheckOptions
            {
                Predicate = report => report.Name.Equals("self", StringComparison.OrdinalIgnoreCase),
                ResultStatusCodes = HealthStatusCodesMapping,
                ResponseWriter = WriteJsonResponse
            })
            .UseHealthChecks("/health/readiness", new HealthCheckOptions
            {
                Predicate = _ => true,
                ResultStatusCodes = HealthStatusCodesMapping,
                ResponseWriter = WriteJsonResponse
            });
    }

    private static Task WriteJsonResponse(HttpContext context, HealthReport result)
    {
        var jsonOptions = context.RequestServices.GetRequiredService<IOptionsMonitor<JsonOptions>>();

        return context
            .Response
            .WriteAsJsonAsync(
                Format(result),
                jsonOptions.CurrentValue.JsonSerializerOptions,
                context.RequestAborted);
    }

    private static object Format(HealthReport report)
    {
        return new
        {
            status = report.Status,
            results = report.Entries.ToDictionary(
                pair => pair.Key,
                pair => new
                {
                    status = pair.Value.Status,
                    description = pair.Value.Description!,
                    data = pair.Value.Data
                })
        };
    }
}