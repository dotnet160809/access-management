using GB.AccessManagement.Domain;
using GB.AccessManagement.Domain.Companies.Messages;
using GB.AccessManagement.Domain.Services;
using GB.AccessManagement.Domain.ValueObjects;
using MassTransit;
using Microsoft.Extensions.Options;

namespace GB.AccessManagement.Infrastructure.MassTransit.Consumers;

public sealed class CompanyCreatedMessageConsumer : AbstractConsumer<ICompanyCreatedMessage>, ISelfTransientService
{
    private readonly ICreateCompanyAccess _companyAccesses;

    public CompanyCreatedMessageConsumer(
        IOptionsMonitor<MessageQueueOptions> options,
        ICreateCompanyAccess companyAccesses) :
        base(options)
    {
        _companyAccesses = companyAccesses;
    }

    public override Task Consume(ConsumeContext<ICompanyCreatedMessage> context)
    {
        if (ContextIsNotAuthenticated(context))
        {
            return Task.CompletedTask;
        }

        var ownerId = context.Message.OwnerId;
        var companyId = context.Message.CompanyId.ToString();
        var accesses = new List<Access> { new("user", ownerId, "owner", "company", companyId) };

        if (Guid.TryParse(context.Message.ParentCompanyId, out var parentCompanyId))
        {
            accesses.Add(new Access("company", parentCompanyId.ToString(), "parent", "company", companyId));
        }

        return _companyAccesses.AsRequested(accesses.ToArray());
    }
}