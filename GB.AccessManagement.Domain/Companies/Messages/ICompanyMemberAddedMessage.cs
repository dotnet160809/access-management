using GB.AccessManagement.Domain.Messages;

namespace GB.AccessManagement.Domain.Companies.Messages;

public interface ICompanyMemberAddedMessage : IMessage
{
    Guid CompanyId { get; }
    
    string MemberId { get; }
}