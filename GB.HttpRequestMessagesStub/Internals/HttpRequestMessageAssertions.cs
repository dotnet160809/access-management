using Microsoft.Extensions.Options;
using NFluent;

namespace GB.HttpRequestMessagesStub.Internals;

internal sealed class HttpRequestMessageAssertions : IAssertHttpRequestMessages
{
    private readonly IOptionsMonitor<HttpRequestMessageAssertionOptions> _options;
    private readonly HttpRequestMessageKey _key;

    public HttpRequestMessageAssertions(
        IOptionsMonitor<HttpRequestMessageAssertionOptions> options,
        HttpRequestMessageKey key)
    {
        _options = options;
        _key = key;
    }

    public bool HasNotBeenSent()
    {
        return !_options.CurrentValue.ContainsKey(_key);
    }

    public async Task HasBeenSentOnce(HttpRequestAssertionDelegate assertion)
    {
        Check.That(_options.CurrentValue.TryGetValue(_key, out var contents)).IsTrue();
        Check.That(contents).HasSize(1);
        await assertion.Invoke(contents?.Single());
    }
}