using System.Text.Json;

namespace GB.AccessManagement.Tests.Extensions;

public static class HttpContentExtension
{
    public static async Task<JsonElement> ReadAsJsonRootElement(this HttpContent? content)
    {
        content ??= new StringContent(string.Empty);
        
        return JsonDocument.Parse(await content.ReadAsStringAsync()).RootElement;
    }
}