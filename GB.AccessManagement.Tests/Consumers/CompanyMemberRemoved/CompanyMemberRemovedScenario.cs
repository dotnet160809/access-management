using GB.AccessManagement.Domain.Companies.Messages;
using GB.AccessManagement.Infrastructure.MassTransit.Consumers;
using MassTransit;

namespace GB.AccessManagement.Tests.Consumers.CompanyMemberRemoved;

public sealed class CompanyMemberRemovedScenario(CompanyMemberRemovedMessageConsumer consumer)
{
    public Task Execute(ConsumeContext<ICompanyMemberRemovedMessage> context)
    {
        return consumer.Consume(context);
    }
}