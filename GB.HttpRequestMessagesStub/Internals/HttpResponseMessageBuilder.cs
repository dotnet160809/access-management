using Microsoft.Extensions.Options;

namespace GB.HttpRequestMessagesStub.Internals;

internal sealed class HttpResponseMessageBuilder : IBuildHttpResponseMessages
{
    private readonly IStubHttpRequestMessages _stub;
    private readonly IOptionsMonitor<HttpRequestMessageDelegatingHandlerOptions> _options;
    private readonly HttpRequestMessageKey _key;

    public HttpResponseMessageBuilder(
        IStubHttpRequestMessages stub,
        IOptionsMonitor<HttpRequestMessageDelegatingHandlerOptions> options,
        HttpRequestMessageKey key)
    {
        _stub = stub;
        _options = options;
        _key = key;
    }

    public IStubHttpRequestMessages WithResponse(HttpResponseMessageDelegate function)
    {
        _options.CurrentValue.TryAdd(_key, function);

        return _stub;
    }
}