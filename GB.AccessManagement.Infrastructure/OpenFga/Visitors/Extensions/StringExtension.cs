namespace GB.AccessManagement.Infrastructure.OpenFga.Visitors.Extensions;

internal static class StringExtension
{
    public static async Task<string[]> Accept(this string? value, IVisitExpandResponse visitor)
    {
        value = value?.Trim();

        return !string.IsNullOrEmpty(value) && !string.IsNullOrWhiteSpace(value)
            ? await visitor.Visit(value)
            : [];
    }
}