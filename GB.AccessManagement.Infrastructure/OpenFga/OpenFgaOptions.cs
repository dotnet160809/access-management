using System.ComponentModel.DataAnnotations;

namespace GB.AccessManagement.Infrastructure.OpenFga;

public sealed record OpenFgaOptions
{
    public const string HttpClientName = "openfga";

    [Required] [Url] public string BaseAddress { get; set; } = string.Empty;
    
    public Uri BaseUri => new(BaseAddress);

    [Required] public string StoreName { get; set; } = string.Empty;

    public OpenFgaAuthorizationModelOptions AuthorizationModel { get; init; } = new();

    public string StoreId { get; set; } = string.Empty;
}