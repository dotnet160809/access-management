using GB.AccessManagement.Tests.Fixtures.OpenFga;
using NFluent;
using NFluent.Extensibility;

namespace GB.AccessManagement.Tests.Assertions;

public static class HttpRequestMessageAssertion
{
    public static void HasNotBeenCreated(this ICheck<IAssertOpenFga> check)
    {
        ExtensibilityHelper
            .BeginCheck(check)
            .FailWhen(assert => !assert.HasNotBeenCreated(), "At least one authorization has been created.")
            .OnNegate("Http request assertion failed, either request has not been stubbed or authorizations have been created.")
            .EndCheck();

        _ = ExtensibilityHelper.BuildCheckLink(check);
    }
    
    public static void HasNotBeenRemoved(this ICheck<IAssertOpenFga> check)
    {
        ExtensibilityHelper
            .BeginCheck(check)
            .FailWhen(assert => !assert.HasNotBeenRemoved(), "At least one authorization has been removed.")
            .OnNegate("Http request assertion failed, either request has not been stubbed or authorizations have been removed.")
            .EndCheck();

        _ = ExtensibilityHelper.BuildCheckLink(check);
    }
}