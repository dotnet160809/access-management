namespace GB.AccessManagement.Domain.Messages;

public interface IMessage
{
    Guid Id { get; }
}