using OpenFga.Sdk.Model;

namespace GB.AccessManagement.Infrastructure.OpenFga.Visitors.Extensions;

internal static class UserSetTreeDifferenceExtension
{
    public static async Task<string[]> Accept(this UsersetTreeDifference? difference, IVisitExpandResponse visitor)
    {
        return difference is not null
            ? await visitor.Visit(difference)
            : [];
    }
}