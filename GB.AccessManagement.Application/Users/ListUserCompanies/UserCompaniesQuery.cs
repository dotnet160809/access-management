using GB.AccessManagement.Application.Presentations;
using GB.AccessManagement.Application.Queries;
using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Application.Users.ListUserCompanies;

public sealed record UserCompaniesQuery : IQuery<CompaniesPresentation>
{
    internal readonly UserId UserId;

    public UserCompaniesQuery(Guid userId)
    {
        UserId = userId;
    }
}