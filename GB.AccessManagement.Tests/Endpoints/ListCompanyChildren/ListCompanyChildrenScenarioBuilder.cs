using GB.AccessManagement.Tests.Extensions;
using GB.AccessManagement.Tests.Fixtures;
using GB.AccessManagement.Tests.Fixtures.OpenFga;
using Microsoft.Extensions.DependencyInjection;

namespace GB.AccessManagement.Tests.Endpoints.ListCompanyChildren;

public sealed class ListCompanyChildrenScenarioBuilder : AbstractEndpointScenarioBuilder<ListCompanyChildrenScenarioBuilder>
{
    public ListCompanyChildrenScenario Build(int apiVersion)
    {
        _ = GetRequiredService<OpenFgaFixture>()
            .CheckReturns(CompanyOwnerOrMembers)
            .ExpandReturns(async request =>
            {
                var document = await request.ReadRootJsonElement();
                var companyId = Guid.Parse(document.GetObjectId());

                return document.GetRelation() switch
                {
                    "owner" => [GetRequiredService<CompanyFixture>().GetCompanyOwner(companyId)],
                    "member" => GetRequiredService<CompanyFixture>().GetCompanyMembers(companyId),
                    _ => Array.Empty<string>()
                };
            })
            .ListObjectsReturns(async request =>
            {
                var document = await request.ReadRootJsonElement();
                var companyId = Guid.Parse(document.GetUserId());

                return document.GetRelation() switch
                {
                    "owner" => [GetRequiredService<CompanyFixture>().GetCompanyOwner(companyId)],
                    "member" => GetRequiredService<CompanyFixture>().GetCompanyMembers(companyId),
                    _ => Array.Empty<string>()
                };
            });

        return new ListCompanyChildrenScenario(CreateClient(), apiVersion);
    }

    protected override void ConfigureServices(IServiceCollection services)
    {
        _ = services
            .StubAuthentication()
            .AddCompanyFixture()
            .StubOpenFga();
    }
}