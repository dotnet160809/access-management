using Swashbuckle.AspNetCore.Annotations;

namespace GB.AccessManagement.Application.Presentations;

[Serializable]
[SwaggerSchema(Title = nameof(CompanyMemberPresentation))]
public sealed record CompanyMemberPresentation(Guid Id) : HalPresentation<CompanyMemberPresentation>;