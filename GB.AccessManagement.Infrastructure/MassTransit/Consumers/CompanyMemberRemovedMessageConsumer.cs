using GB.AccessManagement.Domain;
using GB.AccessManagement.Domain.Companies.Messages;
using GB.AccessManagement.Domain.Services;
using MassTransit;
using Microsoft.Extensions.Options;

namespace GB.AccessManagement.Infrastructure.MassTransit.Consumers;

public sealed class CompanyMemberRemovedMessageConsumer : AbstractConsumer<ICompanyMemberRemovedMessage>, ISelfTransientService
{
    private readonly IDeleteCompanyAccesses _companyAccesses;

    public CompanyMemberRemovedMessageConsumer(
        IOptionsMonitor<MessageQueueOptions> options,
        IDeleteCompanyAccesses companyAccesses)
        : base(options)
    {
        _companyAccesses = companyAccesses;
    }

    public override Task Consume(ConsumeContext<ICompanyMemberRemovedMessage> context)
    {
        return ContextIsNotAuthenticated(context)
            ? Task.CompletedTask
            : _companyAccesses.DeleteMember(context.Message.MemberId, context.Message.CompanyId);
    }
}