using System.Collections.Concurrent;

namespace GB.HttpRequestMessagesStub.Internals;

internal sealed class HttpRequestMessageAssertionOptions : ConcurrentDictionary<HttpRequestMessageKey, ICollection<HttpContent?>>;