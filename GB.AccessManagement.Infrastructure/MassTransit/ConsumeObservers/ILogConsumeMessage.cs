namespace GB.AccessManagement.Infrastructure.MassTransit.ConsumeObservers;

public interface ILogConsumeMessage
{
    void MessageConsumed<TMessage>(TMessage message) where TMessage : class;

    void UnableToConsumeMessage<TMessage>(TMessage message) where TMessage : class;
}