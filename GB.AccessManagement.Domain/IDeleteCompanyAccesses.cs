using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Domain;

public interface IDeleteCompanyAccesses
{
    Task DeleteMember(UserId userId, CompanyId companyId);

    Task AsRequested(Access[] accesses);
}