using GB.AccessManagement.Application.Commands;
using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Application.Companies.CreateCompany;

public sealed record CreateCompanyCommand : ICommand<CompanyId>
{
    internal readonly string Name;
    internal readonly UserId OwnerId;
    internal readonly CompanyId? ParentCompanyId;

    public CreateCompanyCommand(string name, Guid ownerId, Guid? parentCompanyId = null)
    {
        Name = name;
        OwnerId = ownerId;
        ParentCompanyId = parentCompanyId;
    }
}