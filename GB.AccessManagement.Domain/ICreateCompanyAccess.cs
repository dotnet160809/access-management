using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Domain;

public interface ICreateCompanyAccess
{
    Task AsMember(UserId userId, CompanyId companyId)
    {
        return WithRelation(userId, "member", companyId);
    }

    Task WithRelation(UserId userId, Relation relation, CompanyId companyId)
    {
        return AsRequested([new Access("user", userId.ToString(), relation, "company", companyId.ToString())]);
    }

    Task AsRequested(Access[] accesses);
}