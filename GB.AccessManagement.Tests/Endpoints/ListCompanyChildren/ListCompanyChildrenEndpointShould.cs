using System.Net;
using GB.AccessManagement.Tests.Assertions;
using GB.AccessManagement.Tests.Extensions;
using GB.AccessManagement.Tests.Models;
using NFluent;
using NFluent.Json;
using Xunit.Abstractions;

namespace GB.AccessManagement.Tests.Endpoints.ListCompanyChildren;

public sealed class ListCompanyChildrenEndpointShould : AbstractFeatureShould<ListCompanyChildrenScenarioBuilder>
{
    public ListCompanyChildrenEndpointShould(ITestOutputHelper outputHelper, ListCompanyChildrenScenarioBuilder builder)
        : base(outputHelper, builder)
    {
    }

    [Theory]
    [InlineData(1)]
    public async Task ReturnUnauthorizedWhenNoAuthenticationIsProvided(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var company)
            .Build(apiVersion);

        // Act
        var response = await scenario.Execute(company.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.Unauthorized);
    }

    [Theory]
    [InlineData(1)]
    public async Task ReturnForbiddenWhenUserIsNotCompanyMember(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var company)
            .CreateUser(out var user)
            .AuthenticateUser(user)
            .Build(apiVersion);

        // Act
        var response = await scenario.Execute(company.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.Forbidden);
    }

    [Theory]
    [InlineData(1, false, false)]
    [InlineData(1, false, true)]
    [InlineData(1, true, false)]
    [InlineData(1, true, true)]
    public async Task ReturnExpectedResponse(int apiVersion, bool isUserParentCompanyOwner,
        bool isUserChildCompanyOwner)
    {
        // Arrange
        _ = ScenarioBuilder
            .CreateCompany(out var parentCompany)
            .CreateCompany(parentCompany.Id, out var childCompany)
            .CreateUser(out var user);

        _ = isUserParentCompanyOwner
            ? ScenarioBuilder.AddCompanyOwner(parentCompany.Id, user.Id)
            : ScenarioBuilder.AddCompanyMember(parentCompany.Id, user.Id);

        _ = isUserChildCompanyOwner
            ? ScenarioBuilder.AddCompanyOwner(childCompany.Id, user.Id)
            : ScenarioBuilder.AddCompanyMember(childCompany.Id, user.Id);

        var scenario = ScenarioBuilder
            .AuthenticateUser(user)
            .Build(apiVersion);

        // Act
        var response = await scenario.Execute(parentCompany.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.OK);

        var document = await response.ReadRootJsonElement();

        var expectedChildCompanyLinks = new TestHalLinks
        {
            ["self"] = TestHalLink.Get($"/v{apiVersion}/companies/{childCompany.Id}"),
            ["get_owner"] = TestHalLink.Get($"/v{apiVersion}/companies/{childCompany.Id}/owner"),
            ["get_parent"] = TestHalLink.Get($"/v{apiVersion}/companies/{childCompany.Id}/parent"),
            ["get_members"] = TestHalLink.Get($"/v{apiVersion}/companies/{childCompany.Id}/members")
        };

        if (isUserChildCompanyOwner)
        {
            expectedChildCompanyLinks.Add("add_member", TestHalLink.Post($"/v{apiVersion}/companies/{childCompany.Id}/members"));
        }

        childCompany.AddLinks(expectedChildCompanyLinks);
        
        Check.That(document).HasArrayPropertyEquivalentTo("items", [childCompany]);
        Check.That(document).HasIntProperty("total", 1);
        Check.That(document).HasHalPagination($"/v{apiVersion}/companies/{parentCompany.Id}", "GET");
    }
}