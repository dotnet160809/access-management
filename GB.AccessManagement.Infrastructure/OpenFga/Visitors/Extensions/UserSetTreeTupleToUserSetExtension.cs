using OpenFga.Sdk.Model;

namespace GB.AccessManagement.Infrastructure.OpenFga.Visitors.Extensions;

internal static class UserSetTreeTupleToUserSetExtension
{
    public static async Task<string[]> Accept(this UsersetTreeTupleToUserset? userSet, IVisitExpandResponse visitor)
    {
        return userSet is not null
            ? await visitor.Visit(userSet)
            : [];
    }
}