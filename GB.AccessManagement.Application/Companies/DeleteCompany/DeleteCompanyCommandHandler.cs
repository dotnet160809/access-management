using GB.AccessManagement.Application.Commands;

namespace GB.AccessManagement.Application.Companies.DeleteCompany;

public sealed class DeleteCompanyCommandHandler(ICompanyStore store) : ICommandHandler<DeleteCompanyCommand>
{
    public async Task Handle(DeleteCompanyCommand request, CancellationToken cancellationToken)
    {
        var company = await store.Load(request.Id);
        company.Delete();
        await store.Save(company, cancellationToken);
    }
}