namespace GB.AccessManagement.Domain.Messages;

public interface IPublishMessage
{
    Task Publish<TMessage>(TMessage message, CancellationToken cancellationToken) where TMessage : class, IMessage;
}