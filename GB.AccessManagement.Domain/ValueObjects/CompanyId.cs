namespace GB.AccessManagement.Domain.ValueObjects;

public sealed record CompanyId
{
    private readonly Guid _value;

    private CompanyId(Guid value)
    {
        if (value == Guid.Empty)
        {
            throw new ArgumentNullException(nameof(value));
        }

        _value = value;
    }

    public override string ToString()
    {
        return _value.ToString();
    }

    public static implicit operator CompanyId(Guid value)
    {
        return new CompanyId(value);
    }

    public static implicit operator CompanyId?(Guid? value)
    {
        return value.HasValue ? new CompanyId(value.Value) : null;
    }

    public static explicit operator Guid(CompanyId companyId)
    {
        return companyId._value;
    }

    public static explicit operator Guid?(CompanyId? companyId)
    {
        return companyId?._value;
    }

    public static implicit operator CompanyId(string value)
    {
        if (value.StartsWith($"{CompanyRelation.ObjectType}:"))
        {
            value = value.Replace($"{CompanyRelation.ObjectType}:", string.Empty);
        }

        return new CompanyId(Guid.Parse(value));
    }
}