using Asp.Versioning;
using GB.AccessManagement.Application.Presentations;
using GB.AccessManagement.Application.Queries;
using GB.AccessManagement.Application.Users.ListUserCompanies;
using GB.AccessManagement.WebApi.Controllers.Attributes;
using GB.AccessManagement.WebApi.Extensions;
using GB.AccessManagement.WebApi.HalLinks.CompaniesHalLinks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GB.AccessManagement.WebApi.Controllers;

[ApiController]
[Route("v{version:apiVersion}/users")]
[ApiVersion("1"), Tags("Users")]
[Authorize]
[ReturnsUnauthorized, ReturnsForbidden, ReturnsInternalServerError]
public sealed class UserController : ControllerBase
{
    [HttpGet("{userId:guid}/companies")]
    [ReturnsOk(typeof(CompaniesPresentation), SupportedContentTypes.HalJson)]
    [ReturnsBadRequest, ReturnsUnprocessableEntity]
    public async Task<IActionResult> ListCompanies(
        [FromRoute] Guid userId,
        [FromServices] IQueryDispatcher dispatcher,
        [FromServices] CompaniesHalLinkDirector director,
        CancellationToken cancellationToken)
    {
        UserCompaniesQuery query = new(userId);
        var companies = await dispatcher.Dispatch(query, cancellationToken);

        return this.Hal(await director.MakeHalLinks(companies, userId.ToString(), cancellationToken));
    }
}