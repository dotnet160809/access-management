using Microsoft.AspNetCore.Authorization;

namespace GB.AccessManagement.WebApi.Configurations.Authorization;

public sealed record CompanyAccessAuthorizationRequirement(
    string RouteParameterName,
    string Relation) : IAuthorizationRequirement;