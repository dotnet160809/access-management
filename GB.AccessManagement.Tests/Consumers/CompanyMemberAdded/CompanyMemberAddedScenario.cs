using GB.AccessManagement.Domain.Companies.Messages;
using GB.AccessManagement.Infrastructure.MassTransit.Consumers;
using MassTransit;

namespace GB.AccessManagement.Tests.Consumers.CompanyMemberAdded;

public sealed class CompanyMemberAddedScenario(CompanyMemberAddedMessageConsumer consumer)
{
    public Task Execute(ConsumeContext<ICompanyMemberAddedMessage> context)
    {
        return consumer.Consume(context);
    }
}