using GB.AccessManagement.Domain.Aggregates;
using GB.AccessManagement.Domain.Companies.Events;
using GB.AccessManagement.Domain.Companies.Exceptions;
using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Domain.Companies;

public sealed partial class CompanyAggregate : AggregateRoot<CompanyAggregate, CompanyId, ICompanyMemo>
{
    private readonly CompanyName _name;
    private readonly UserId _ownerId;
    private readonly CompanyId? _parentCompanyId;
    private readonly List<CompanyId> _children;
    private readonly List<UserId> _members;

    private CompanyAggregate(CompanyId id, CompanyName name, UserId ownerId, CompanyId? parentCompanyId)
    {
        Id = id;
        _name = name;
        _ownerId = ownerId;
        _parentCompanyId = parentCompanyId;
        _children = [];
        _members = [];
    }

    public void AddMember(UserId memberId)
    {
        if (_members.Contains(memberId))
        {
            throw new MemberAlreadyAddedException(memberId, Id);
        }

        _members.Add(memberId);
        StoreEvent(new CompanyMemberAddedEvent(Id, memberId));
    }

    public void RemoveMember(UserId memberId)
    {
        if (!_members.Contains(memberId))
        {
            throw new MissingCompanyMemberException(memberId, Id);
        }

        if (memberId == _ownerId)
        {
            throw new CannotRemoveCompanyOwnerException(_ownerId, Id);
        }

        _members.Remove(memberId);
        StoreEvent(new CompanyMemberRemovedEvent(Id, memberId));
    }

    public void AddAsChildToParent(CompanyId parentCompanyId)
    {
        if (_parentCompanyId is not null)
        {
            throw new CannotAddCompanyChildException(Id, parentCompanyId);
        }
        
        StoreEvent(new CompanyChildAddedEvent(Id, parentCompanyId));
    }

    public void Delete()
    {
        if (_children.Count > 0)
        {
            throw new CannotDeleteCompanyException(Id);
        }

        StoreEvent(new CompanyDeletedEvent(Id));
    }
}