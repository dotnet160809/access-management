using GB.AccessManagement.Domain.Aggregates.Memos;
using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Domain.Companies;

public interface ICompanyMemo : IAggregateMemo
{
    CompanyId Id { get; set; }
    
    CompanyName Name { get; set; }
    
    UserId OwnerId { get; set; }
    
    CompanyId? ParentCompanyId { get; set; }
    
    ICollection<CompanyId> Children { get; set; }
    
    ICollection<UserId> Members { get; set; }
}