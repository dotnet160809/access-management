using GB.AccessManagement.Application.Presentations;
using GB.AccessManagement.Domain.Services;
using GB.AccessManagement.WebApi.HalLinks.CompanyMemberHalLinks;

namespace GB.AccessManagement.WebApi.HalLinks.CompanyMembersHalLinks;

public sealed class CompanyMembersHalLinkDirector : ISelfTransientService
{
    private readonly CompanyMembersHalLinkBuilder _builder;
    private readonly CompanyMemberHalLinkDirector _director;

    public CompanyMembersHalLinkDirector(CompanyMembersHalLinkBuilder builder, CompanyMemberHalLinkDirector director)
    {
        _builder = builder;
        _director = director;
    }

    public async Task<CompanyMembersPresentation> MakeHalLinks(CompanyMembersPresentation presentation, Guid companyId, CancellationToken cancellationToken)
    {
        _ = _builder
            .BuildGetCompanyMembersLink(presentation, companyId)
            .BuildPaginationLinks(presentation, companyId);

        foreach (var member in presentation.Items)
        {
            await _director.MakeHalLinks(member, cancellationToken);
        }

        return presentation;
    }
}