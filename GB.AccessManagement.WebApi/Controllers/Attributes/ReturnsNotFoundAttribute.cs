namespace GB.AccessManagement.WebApi.Controllers.Attributes;

public sealed class ReturnsNotFoundAttribute() : ProblemResponseTypeAttribute(StatusCodes.Status404NotFound);