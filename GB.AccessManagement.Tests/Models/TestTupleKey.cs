using System.Text.Json.Serialization;
using GB.HttpRequestMessagesStub;

namespace GB.AccessManagement.Tests.Models;

public sealed record TestTupleKey(
    [property: JsonPropertyName("user")] string User,
    [property: JsonPropertyName("relation")] string Relation,
    [property: JsonPropertyName("object")] string Object)
{
    public bool Equals(TestTupleKey? other)
    {
        return other is not null
            && User.OrdinalEquals(other.User)
            && Relation.OrdinalEquals(other.Relation)
            && Object.OrdinalEquals(other.Object);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            var hashCode = User.GetOrdinalHashCode();
            hashCode = (hashCode * 397) ^ Relation.GetOrdinalHashCode();
            
            return (hashCode * 397) ^ Object.GetOrdinalHashCode();
        }
    }
}