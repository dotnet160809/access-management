namespace GB.AccessManagement.WebApi.Configurations.Authentication;

public static class AuthenticationConfiguration
{
    private const string AuthenticationScheme = DummyAuthenticationOptions.AuthenticationScheme;

    public static IServiceCollection ConfigureAuthentication(this IServiceCollection services)
    {
        return services
            .AddAuthentication(options =>
            {
                options.DefaultScheme = AuthenticationScheme;
                options.DefaultForbidScheme = options.DefaultScheme;
                options.DefaultChallengeScheme = options.DefaultScheme;
            })
            .AddScheme<DummyAuthenticationOptions, DummyAuthenticationHandler>(AuthenticationScheme, _ => { })
            .Services;
    }
}