using GB.AccessManagement.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;

namespace GB.AccessManagement.WebApi.HalLinks;

public sealed class HalLinkHrefBuilder : ISelfTransientService
{
    private readonly IUrlHelper _urlHelper;

    public HalLinkHrefBuilder(IUrlHelperFactory factory, IActionContextAccessor accessor)
    {
        if (accessor.ActionContext is null)
        {
            throw new ArgumentNullException(nameof(accessor.ActionContext));
        }

        _urlHelper = factory.GetUrlHelper(accessor.ActionContext);
    }

    public string BuildCompanyLink(Guid companyId)
    {
        return _urlHelper.Action("Get", "Company", new { companyId }) ?? string.Empty;
    }

    public string BuildCompanyMembersLink(Guid companyId)
    {
        return _urlHelper.Action("Members", "Company", new { companyId }) ?? string.Empty;
    }

    public string BuildCompanyChildrenLink(Guid companyId)
    {
        return _urlHelper.Action("Children", "Company", new { companyId }) ?? string.Empty;
    }

    public string BuildCompanyOwnerLink(Guid companyId)
    {
        return _urlHelper.Action("Owner", "Company", new { companyId }) ?? string.Empty;
    }

    public string BuildCompanyParentLink(Guid companyId)
    {
        return _urlHelper.Action("Parent", "Company", new { companyId }) ?? string.Empty;
    }

    public string BuildUserCompaniesLink(Guid userId)
    {
        return BuildUserCompaniesLink(userId.ToString());
    }

    public string BuildUserCompaniesLink(string userId)
    {
        return _urlHelper.Action("ListCompanies", "User", new { userId }) ?? string.Empty;
    }
}