using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace GB.AccessManagement.WebApi.Controllers.Attributes;

public abstract class ProblemResponseTypeAttribute(int statusCode) : SwaggerResponseAttribute(statusCode, type: typeof(ProblemDetails), contentTypes: SupportedContentTypes.ProblemDetails);