using GB.AccessManagement.Domain.Messages;
using GB.AccessManagement.Infrastructure.MassTransit;
using MassTransit.Testing;
using Microsoft.Net.Http.Headers;

namespace GB.AccessManagement.Tests.Extensions;

public static class PublishedMessageListExtension
{
    public static bool HasNoMessage<TMessage>(
        this IPublishedMessageList publishedMessages,
        Predicate<IPublishedMessage<TMessage>> predicate)
        where TMessage : class
    {
        return !publishedMessages.HasAnyMessage(predicate);
    }

    public static IPublishedMessage<TMessage>? SingleOrDefault<TMessage>(
        this IPublishedMessageList publishedMessages,
        Predicate<IPublishedMessage<TMessage>> predicate)
        where TMessage : class
    {
        return publishedMessages
            .Select<TMessage>()
            .SingleOrDefault(message => predicate(message));
    }

    public static bool HasAuthorizationHeader<T>(this IPublishedMessage<T> publishedMessage, MessageQueueOptions options) where T : class
    {
        return publishedMessage.MessageObject is IMessage message
               && publishedMessage.Context.Headers.TryGetHeader(HeaderNames.Authorization, out var headerValue)
               && options.Match(message.Id.ToString(), headerValue.ToString());
    }

    private static bool HasAnyMessage<TMessage>(
        this IPublishedMessageList publishedMessages,
        Predicate<IPublishedMessage<TMessage>> predicate)
        where TMessage : class
    {
        return publishedMessages
            .Select<TMessage>()
            .Any(predicate.Invoke);
    }
}