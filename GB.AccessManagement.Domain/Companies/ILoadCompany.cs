using GB.AccessManagement.Domain.Aggregates;
using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Domain.Companies;

public interface ILoadCompany : IAggregateRootLoader<CompanyAggregate, CompanyId, ICompanyMemo> { }