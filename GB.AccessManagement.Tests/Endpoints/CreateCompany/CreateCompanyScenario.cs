using System.Net.Http.Json;

namespace GB.AccessManagement.Tests.Endpoints.CreateCompany;

public sealed class CreateCompanyScenario(HttpClient client, int apiVersion)
{
    public Task<HttpResponseMessage> Execute(string? companyName)
    {
        return client.PostAsync($"/v{apiVersion}/companies", JsonContent.Create(new
        {
            name = companyName
        }));
    }
}