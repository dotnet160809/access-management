using GB.AccessManagement.Domain.Companies.Messages;
using GB.AccessManagement.Infrastructure.MassTransit.Consumers;
using MassTransit;

namespace GB.AccessManagement.Tests.Consumers.CompanyDeleted;

public sealed class CompanyDeletedScenario(CompanyDeletedMessageConsumer consumer)
{
    public Task Execute(ConsumeContext<ICompanyDeletedMessage> context)
    {
        return consumer.Consume(context);
    }
}