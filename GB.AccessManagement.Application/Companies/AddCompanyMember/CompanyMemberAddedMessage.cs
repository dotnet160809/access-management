using GB.AccessManagement.Domain.Companies.Events;
using GB.AccessManagement.Domain.Companies.Messages;

namespace GB.AccessManagement.Application.Companies.AddCompanyMember;

[Serializable]
public sealed record CompanyMemberAddedMessage(Guid CompanyId, string MemberId) : ICompanyMemberAddedMessage
{
    public Guid Id { get; } = Guid.NewGuid();
    
    public static explicit operator CompanyMemberAddedMessage(CompanyMemberAddedEvent @event)
    {
        return new CompanyMemberAddedMessage((Guid)@event.CompanyId, @event.MemberId.ToString());
    }
}