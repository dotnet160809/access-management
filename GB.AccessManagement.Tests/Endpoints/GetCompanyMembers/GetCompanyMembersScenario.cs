namespace GB.AccessManagement.Tests.Endpoints.GetCompanyMembers;

public sealed class GetCompanyMembersScenario(HttpClient client, int apiVersion)
{
    public Task<HttpResponseMessage> Execute(Guid companyId)
    {
        return client.GetAsync($"/v{apiVersion}/companies/{companyId}/members");
    }
}