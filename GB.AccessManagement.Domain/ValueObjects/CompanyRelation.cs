namespace GB.AccessManagement.Domain.ValueObjects;

public sealed record CompanyRelation
{
    private readonly string _value;

    private CompanyRelation(string value)
    {
        if (string.IsNullOrEmpty(value))
        {
            throw new ArgumentNullException(nameof(value));
        }

        _value = value;
    }

    public const string UserType = "user";

    public const string ObjectType = "company";

    public static CompanyRelation Owner => new("owner");

    public static CompanyRelation Member => new("member");

    public static CompanyRelation Parent => new("parent");

    public static implicit operator string(CompanyRelation relation)
    {
        return relation._value;
    }
}