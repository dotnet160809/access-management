using MediatR;

namespace GB.AccessManagement.Application.Queries;

public interface IQuery<out TResponse> : IRequest<TResponse> { }