using GB.AccessManagement.Application;
using GB.AccessManagement.Application.Companies;
using GB.AccessManagement.Application.Presentations;
using GB.AccessManagement.Domain.Aggregates.Memos;
using GB.AccessManagement.Domain.Companies;
using GB.AccessManagement.Domain.Companies.Providers;
using GB.AccessManagement.Domain.Events;
using GB.AccessManagement.Domain.Services;
using GB.AccessManagement.Domain.ValueObjects;
using GB.AccessManagement.Infrastructure.Database.Extensions;
using Microsoft.EntityFrameworkCore;

namespace GB.AccessManagement.Infrastructure.Database;

public sealed class CompanyStore : ICompanyStore, IReadCompany, ITransientService
{
    private readonly CompanyDbContext _dbContext;
    private readonly IDomainEventPublisher _publisher;
    private readonly ILoadCompany _loader;
    private readonly ICountCompanies _metrics;
    private readonly IProvideCompanyMemberIds _companyMembersProvider;
    private readonly IProvideCompanyChildrenIds _companyChildrenProvider;

    public CompanyStore(
        CompanyDbContext dbContext,
        IDomainEventPublisher publisher,
        ILoadCompany loader,
        ICountCompanies metrics,
        IProvideCompanyMemberIds companyMembersProvider,
        IProvideCompanyChildrenIds companyChildrenProvider)
    {
        _dbContext = dbContext;
        _loader = loader;
        _publisher = publisher;
        _metrics = metrics;
        _companyMembersProvider = companyMembersProvider;
        _companyChildrenProvider = companyChildrenProvider;
    }

    public async Task<CompanyAggregate> Load(CompanyId id)
    {
        ICompanyMemo memo = await FindAsync(id);
        Task[] tasks =
        [
            _companyMembersProvider.ListMembersForCompany(id).ContinueWith(task => memo.Members = task.Result),
            _companyChildrenProvider.ForCompany(id).ContinueWith(task => memo.Children = task.Result),
        ];
        await Task.WhenAll(tasks);

        return _loader.Load(memo);
    }

    public async Task Save(CompanyAggregate aggregate, CancellationToken cancellationToken)
    {
        var dao = await FindAsync(aggregate.Id);

        foreach (dynamic @event in aggregate.UncommittedEvents)
        {
            aggregate.Save(@event, dao);
        }

        await Save(dao, cancellationToken);
        await _publisher.Publish(aggregate.UncommittedEvents, cancellationToken);
    }

    public Task<bool> Exist(CompanyId id)
    {
        return _dbContext
            .Companies
            .AsNoTracking()
            .AnyAsync(company => company.Id == (Guid)id);
    }

    public Task<CompanyPresentation[]> List(IEnumerable<CompanyId> ids)
    {
        return _dbContext
            .Companies
            .AsNoTracking()
            .Filter(ids)
            .AsPresentations()
            .ToArrayAsync();
    }

    private async Task<CompanyDao> FindAsync(CompanyId companyId)
    {
        var dao = await _dbContext.Companies
            .Join(_dbContext.Companies,
                child => child.ParentCompanyId,
                company => company.Id,
                (child, _) => child)
            .SingleOrDefaultAsync(company => company.Id == (Guid)companyId);

        return dao ?? new CompanyDao();
    }

    private async Task Save(IAggregateMemo memo, CancellationToken cancellationToken)
    {
        switch (memo.State)
        {
            case EMemoState.Created:
                await _dbContext.AddAsync(memo, cancellationToken);
                _metrics.WhenCreated();
                break;

            case EMemoState.Updated:
                _dbContext.Update(memo);
                _metrics.WhenUpdated();
                break;

            case EMemoState.Deleted:
                _dbContext.Remove(memo);
                _metrics.WhenDeleted();
                break;

            case EMemoState.Unchanged:
            default:
                break;
        }

        await _dbContext.SaveChangesAsync(cancellationToken);
    }
}