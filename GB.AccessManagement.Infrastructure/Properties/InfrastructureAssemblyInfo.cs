using System.Reflection;

namespace GB.AccessManagement.Infrastructure.Properties;

public static class InfrastructureAssemblyInfo
{
    public static Assembly Assembly => typeof(InfrastructureAssemblyInfo).Assembly;
}