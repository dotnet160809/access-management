using GB.AccessManagement.Application.Presentations;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace GB.AccessManagement.WebApi.Configurations.Swagger.SchemaFilters;

public sealed class HalLinkSchemaFilter
{
    public void Apply(OpenApiSchema schema, string linkName, SchemaFilterContext context)
    {
        if (!context.SchemaRepository.TryLookupByType(typeof(HalLink), out var halLinkOpenApiSchema))
        {
            return;
        }

        schema.AdditionalPropertiesAllowed = false;
        schema.Properties.Add(linkName, halLinkOpenApiSchema);
    }

}