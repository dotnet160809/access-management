using System.Net;
using System.Net.Http.Json;
using GB.AccessManagement.Tests.Extensions;
using GB.AccessManagement.Tests.Fixtures;
using GB.AccessManagement.Tests.Fixtures.OpenFga;
using OpenFga.Sdk.Model;

namespace GB.AccessManagement.Tests.Endpoints;

public abstract class AbstractEndpointScenarioBuilder<TBuilder> : AbstractScenarioBuilder<TBuilder>
    where TBuilder : AbstractScenarioBuilder<TBuilder>, new()
{
    protected OpenFgaFixture OpenFga()
    {
        return GetRequiredService<OpenFgaFixture>();
    }

    protected async Task<HttpResponseMessage> CompanyOwnerOrMembers(HttpRequestMessage request)
    {
        var document = await request.ReadRootJsonElement();
        var companyId = Guid.Parse(document.GetObjectId());
        var userId = Guid.Parse(document.GetUserId());

        var fixture = GetRequiredService<CompanyFixture>();

        var allowed = document.GetRelation() switch
        {
            "owner" => fixture.IsCompanyOwner(companyId, userId),
            "member" => fixture.IsCompanyMember(companyId, userId),
            _ => false
        };

        return new HttpResponseMessage(HttpStatusCode.OK)
        {
            Content = JsonContent.Create(new CheckResponse
            {
                Allowed = allowed
            })
        };
    }

    protected async Task<IEnumerable<string>> CompanyOwnerOrMemberIds(HttpRequestMessage request)
    {
        var document = await request.ReadRootJsonElement();
        var companyId = Guid.Parse(document.GetObjectId());

        return document.GetRelation() switch
        {
            "owner" => [GetRequiredService<CompanyFixture>().GetCompanyOwner(companyId)],
            "member" => GetRequiredService<CompanyFixture>().GetCompanyMembers(companyId),
            _ => Array.Empty<string>()
        };
    }

    protected IEnumerable<string> CompanyOwner(Guid companyId)
    {
        return [GetRequiredService<CompanyFixture>().GetCompanyOwner(companyId)];
    }

    protected IEnumerable<string> CompanyMembers(Guid companyId)
    {
        return GetRequiredService<CompanyFixture>()
            .GetCompanyMembers(companyId);
    }

    protected List<string> UserCompanies(Guid userId)
    {
        return GetRequiredService<CompanyFixture>()
            .GetUserCompanies(userId)
            .Select(companyId => companyId.ToString())
            .ToList();
    }
}