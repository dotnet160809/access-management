namespace GB.AccessManagement.Tests.Endpoints.DeleteCompany;

public sealed class DeleteCompanyScenario(HttpClient client, int apiVersion)
{
    public Task<HttpResponseMessage> Execute(Guid companyId)
    {
        return client.DeleteAsync($"/v{apiVersion}/companies/{companyId}");
    }
}