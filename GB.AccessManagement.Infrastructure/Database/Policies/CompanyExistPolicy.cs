using GB.AccessManagement.Application;
using GB.AccessManagement.Domain.Companies.Exceptions;
using GB.AccessManagement.Domain.Companies.Policies;
using GB.AccessManagement.Domain.Services;
using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Infrastructure.Database.Policies;

public sealed class CompanyExistPolicy : ICompanyExistPolicy, ITransientService
{
    private readonly IReadCompany _repository;

    public CompanyExistPolicy(IReadCompany repository)
    {
        _repository = repository;
    }

    public async Task EnsureCompanyExists(CompanyId id)
    {
        if (!await _repository.Exist(id))
        {
            throw new NonExistentCompanyException(id);
        }
    }
}