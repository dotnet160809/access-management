namespace GB.AccessManagement.Domain.ValueObjects;

public sealed record ObjectId
{
    private readonly string _value;

    private ObjectId(string value)
    {
        value = value.Trim();

        if (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value))
        {
            throw new ArgumentNullException(nameof(value));
        }

        _value = value;
    }

    public override string ToString()
    {
        return _value;
    }

    public static implicit operator ObjectId(string value)
    {
        return new ObjectId(value);
    }
}