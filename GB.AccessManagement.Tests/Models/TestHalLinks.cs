using GB.HttpRequestMessagesStub;

namespace GB.AccessManagement.Tests.Models;

public sealed class TestHalLinks : Dictionary<string, TestHalLink>
{
    public override int GetHashCode()
    {
        unchecked
        {
            var hashCode = 0;

            foreach (var item in this.OrderBy(item => item.Key))
            {
                hashCode = (hashCode * 397) ^ item.Key.GetOrdinalHashCode();
                hashCode = (hashCode * 397) ^ item.Value.GetHashCode();
            }

            return hashCode;
        }
    }

    public override bool Equals(object? obj)
    {
        if (obj is not Dictionary<string, TestHalLink> other || Count != other.Count)
        {
            return false;
        }

        var result = true;

        foreach (var item in this.OrderBy(item => item.Key))
        {
            result &= other.Any(otherItem => item.Key.OrdinalEquals(otherItem.Key)
                                             && item.Value.Equals(otherItem.Value));

            if (!result)
            {
                break;
            }
        }

        return result;
    }
}