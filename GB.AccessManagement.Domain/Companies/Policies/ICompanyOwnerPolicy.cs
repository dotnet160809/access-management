using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Domain.Companies.Policies;

public interface ICompanyOwnerPolicy
{
    Task EnsureUserIsCompanyOwner(UserId userId, CompanyId companyId);
}