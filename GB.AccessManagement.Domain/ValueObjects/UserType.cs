namespace GB.AccessManagement.Domain.ValueObjects;

public sealed record UserType
{
    private readonly string _value;

    private UserType(string value)
    {
        value = value.Trim();

        if (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value))
        {
            throw new ArgumentNullException(nameof(value));
        }

        _value = value;
    }

    public override string ToString()
    {
        return _value;
    }

    public static implicit operator UserType(string value)
    {
        return new UserType(value);
    }

    public static implicit operator string(UserType objectType)
    {
        return objectType._value;
    }
}