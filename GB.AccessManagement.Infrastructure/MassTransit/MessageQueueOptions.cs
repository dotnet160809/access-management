using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace GB.AccessManagement.Infrastructure.MassTransit;

public sealed record MessageQueueOptions
{
    [Required] public string Host { get; init; } = string.Empty;

    [Required] public ushort Port { get; init; }

    [Required] public string VirtualHost { get; init; } = string.Empty;

    [Required] public string Username { get; init; } = string.Empty;

    [Required] public string Password { get; init; } = string.Empty;

    [Required] public string AuthenticationKey { get; init; } = string.Empty;

    public string Hash(string value)
    {
        return Convert.ToBase64String(KeyDerivation.Pbkdf2(
            password: value,
            salt: HashedAuthenticationKey,
            prf: KeyDerivationPrf.HMACSHA256,
            iterationCount: 100000,
            numBytesRequested: 256 / 8));
    }

    public bool Match(string value, string? hashedValue)
    {
        return string.Equals(Hash(value), hashedValue, StringComparison.Ordinal);
    }

    private byte[] HashedAuthenticationKey
    {
        get
        {
            var bytes = Encoding.UTF8.GetBytes(AuthenticationKey);

            return SHA256.HashData(bytes);
        }
    }
}