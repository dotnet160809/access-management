using System.Net.Http.Json;
using GB.AccessManagement.Domain;
using GB.AccessManagement.Domain.Services;
using GB.AccessManagement.Domain.ValueObjects;
using GB.AccessManagement.Infrastructure.OpenFga.Extensions;
using Microsoft.Extensions.Options;
using OpenFga.Sdk.Model;

namespace GB.AccessManagement.Infrastructure.OpenFga.Clients;

public sealed class OpenFgaWriteClient : AbstractOpenFgaClient,
    ICreateCompanyAccess,
    IDeleteCompanyAccesses,
    ITransientService
{
    private readonly ICountCompanyAccess _metrics;

    public OpenFgaWriteClient(
        IHttpClientFactory factory,
        IOptionsMonitor<OpenFgaOptions> options,
        ICountCompanyAccess metrics)
        : base(factory, options)
    {
        _metrics = metrics;
    }

    async Task ICreateCompanyAccess.AsRequested(Access[] accesses)
    {
        var request = new WriteRequest
        {
            Writes = new WriteRequestWrites { TupleKeys = accesses.ToTupleKeys() }
        };
        await Write(request);

        _metrics.WhenCreated();
    }

    public async Task DeleteMember(UserId userId, CompanyId companyId)
    {
        var accesses = new[]
        {
            new Access("user", userId.ToString(), "member", "company", companyId.ToString())
        };
        var request = new WriteRequest
        {
            Deletes = new WriteRequestDeletes { TupleKeys = accesses.ToTupleKeysWithoutCondition() }
        };
        await Write(request);

        _metrics.WhenDeleted();
    }

    async Task IDeleteCompanyAccesses.AsRequested(Access[] accesses)
    {
        var request = new WriteRequest
        {
            Deletes = new WriteRequestDeletes { TupleKeys = accesses.ToTupleKeysWithoutCondition() }
        };
        await Write(request);

        _metrics.WhenDeleted();
    }

    private async Task Write(WriteRequest request)
    {
        var response = await CreateClient().PostAsync($"/stores/{StoreId()}/write", JsonContent.Create(request));
        response.EnsureSuccessStatusCode();
    }
}