namespace GB.AccessManagement.Domain.Aggregates.Memos;

public enum EMemoState
{
    Created,
    Updated,
    Deleted,
    Unchanged,
}