using GB.AccessManagement.WebApi;

var builder = WebApplication.CreateBuilder(args);
var startup = new Startup(builder.Environment, builder.Configuration);

startup.ConfigureHost(builder.Host);
startup.ConfigureServices(builder.Services);

var app = builder.Build();
startup.Configure(app);

await app.RunAsync();