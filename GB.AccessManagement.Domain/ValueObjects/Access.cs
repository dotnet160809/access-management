namespace GB.AccessManagement.Domain.ValueObjects;

public sealed record Access(UserType UserType, UserId UserId, Relation Relation, ObjectType ObjectType, ObjectId ObjectId)
{
    public string User => $"{UserType}:{UserId}";

    public string Object => $"{ObjectType}:{ObjectId}";
}