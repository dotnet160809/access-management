using System.Reflection;

namespace GB.AccessManagement.Domain.Properties;

public static class DomainAssemblyInfo
{
    public static Assembly Assembly => typeof(DomainAssemblyInfo).Assembly;
}