using GB.AccessManagement.Application.Commands;
using GB.AccessManagement.Application.Queries;
using GB.AccessManagement.Domain.Events;
using GB.AccessManagement.Domain.Services;
using MediatR;

namespace GB.AccessManagement.Infrastructure;

public sealed class Dispatcher : ICommandDispatcher, IQueryDispatcher, IDomainEventPublisher, ITransientService
{
    private readonly IMediator _mediator;

    public Dispatcher(IMediator mediator)
    {
        _mediator = mediator;
    }

    public Task<TResponse> Dispatch<TResponse>(IQuery<TResponse> query, CancellationToken cancellationToken)
    {
        return _mediator.Send(query, cancellationToken);
    }

    public async Task Dispatch(ICommand command, CancellationToken cancellationToken)
    {
        await _mediator.Send(command, cancellationToken);
    }

    public Task<TResponse> Dispatch<TResponse>(ICommand<TResponse> command, CancellationToken cancellationToken)
    {
        return _mediator.Send(command, cancellationToken);
    }

    public async Task Publish<TEvent>(TEvent @event, CancellationToken cancellationToken) where TEvent : DomainEvent
    {
        await _mediator.Publish(@event, cancellationToken);
        @event.Commit();
    }
}