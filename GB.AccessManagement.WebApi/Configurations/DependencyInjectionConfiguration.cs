using System.Reflection;
using GB.AccessManagement.Application.Properties;
using GB.AccessManagement.Domain.Properties;
using GB.AccessManagement.Domain.Services;
using GB.AccessManagement.Infrastructure.Properties;
using Scrutor;

namespace GB.AccessManagement.WebApi.Configurations;

public static class DependencyInjectionConfiguration
{
    private static readonly Assembly[] Assemblies =
    [
        DomainAssemblyInfo.Assembly,
        ApplicationAssemblyInfo.Assembly,
        InfrastructureAssemblyInfo.Assembly,
        typeof(Startup).Assembly
    ];

    public static IServiceCollection ConfigureDependencyInjection(this IServiceCollection services)
    {
        return services
            .AddMediatR(FromApplication)
            .Scan(selector => _ = selector
                .FromAssemblies(Assemblies)
                .AddTransientClasses()
                .AddSingletonClasses());
    }

    private static void FromApplication(MediatRServiceConfiguration configuration)
    {
        _ = configuration.RegisterServicesFromAssembly(ApplicationAssemblyInfo.Assembly);
    }

    private static IImplementationTypeSelector AddTransientClasses(this IImplementationTypeSelector selector)
    {
        return selector
            .AddClasses(classes => classes.AssignableTo<ITransientService>())
            .AsImplementedInterfaces()
            .WithTransientLifetime()
            .AddClasses(classes => classes.AssignableTo<ISelfTransientService>())
            .AsSelf()
            .WithTransientLifetime();
    }

    private static IImplementationTypeSelector AddSingletonClasses(this IImplementationTypeSelector selector)
    {
        return selector
            .AddClasses(classes => classes.AssignableTo<ISingletonService>())
            .AsImplementedInterfaces()
            .WithSingletonLifetime()
            .AddClasses(classes => classes.AssignableTo<ISelfSingletonService>())
            .AsSelf()
            .WithSingletonLifetime();
    }
}