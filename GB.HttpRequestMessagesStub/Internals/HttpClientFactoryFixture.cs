using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.Http;
using Microsoft.Extensions.Options;

namespace GB.HttpRequestMessagesStub.Internals;

internal sealed class HttpClientFactoryFixture : IHttpClientFactory, IStubHttpRequestMessages
{
    private readonly IOptionsMonitor<HttpClientFactoryOptions> _factoryOptions;
    private readonly IOptionsMonitor<HttpRequestMessageDelegatingHandlerOptions> _delegatingOptions;
    private readonly IOptionsMonitor<HttpRequestMessageAssertionOptions> _assertionOptions;

    public HttpClientFactoryFixture(
        IOptionsMonitor<HttpClientFactoryOptions> factoryOptions,
        IOptionsMonitor<HttpRequestMessageDelegatingHandlerOptions> delegatingOptions,
        IOptionsMonitor<HttpRequestMessageAssertionOptions> assertionOptions)
    {
        _factoryOptions = factoryOptions;
        _delegatingOptions = delegatingOptions;
        _assertionOptions = assertionOptions;
    }

    public HttpClient CreateClient(string name)
    {
        var delegatingHandler = new HttpRequestMessageDelegatingHandler(_delegatingOptions, _assertionOptions);
        var client = new HttpClient(delegatingHandler);

        foreach (var httpClientAction in _factoryOptions.Get(name).HttpClientActions)
        {
            httpClientAction.Invoke(client);
        }
        
        return client;
    }

    [SuppressMessage("ReSharper", "InvertIf")]
    public IBuildHttpResponseMessages StubHttpRequest(string host, string pathAndQuery, HttpMethod method, bool clearPreviousStubs = true)
    {
        var key = new HttpRequestMessageKey(host, pathAndQuery, method);

        if (clearPreviousStubs)
        {
            _ = _delegatingOptions.CurrentValue.TryRemove(key, out _);
            _ = _assertionOptions.CurrentValue.TryRemove(key, out _);
        }

        return new HttpResponseMessageBuilder(this, _delegatingOptions, key);
    }

    public IAssertHttpRequestMessages AssertHttpRequest(string host, string pathAndQuery, HttpMethod method)
    {
        var key = new HttpRequestMessageKey(host, pathAndQuery, method);

        return new HttpRequestMessageAssertions(_assertionOptions, key);
    }
}