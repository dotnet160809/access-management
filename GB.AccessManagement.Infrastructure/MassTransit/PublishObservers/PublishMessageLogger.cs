using MassTransit;

namespace GB.AccessManagement.Infrastructure.MassTransit.PublishObservers;

public sealed class PublishMessageLogger : IPublishObserver
{
    private readonly ILogPublishMessage _logger;

    public PublishMessageLogger(ILogPublishMessage factory)
    {
        _logger = factory;
    }

    public Task PrePublish<T>(PublishContext<T> context) where T : class
    {
        return Task.CompletedTask;
    }

    public Task PostPublish<T>(PublishContext<T> context) where T : class
    {
        _logger.MessagePublished(context.Message);

        return Task.CompletedTask;
    }

    public Task PublishFault<T>(PublishContext<T> context, Exception exception) where T : class
    {
        _logger.UnableToPublishMessage(context.Message);

        return Task.CompletedTask;
    }
}