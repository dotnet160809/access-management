using GB.AccessManagement.Application.Companies.AddCompanyMember;
using GB.AccessManagement.Domain.Companies.Messages;
using GB.AccessManagement.Infrastructure.MassTransit.Consumers;
using GB.AccessManagement.Tests.Extensions;
using GB.AccessManagement.Tests.Fixtures.OpenFga;
using MassTransit;
using Microsoft.Extensions.DependencyInjection;

namespace GB.AccessManagement.Tests.Consumers.CompanyMemberAdded;

public sealed class CompanyMemberAddedScenarioBuilder : AbstractConsumerScenarioBuilder<CompanyMemberAddedScenarioBuilder>
{
    public CompanyMemberAddedScenarioBuilder CreateMessageContext(out ConsumeContext<ICompanyMemberAddedMessage> context)
    {
        var message = new CompanyMemberAddedMessage(NewCompanyId(), NewUserId().ToString());

        return CreateMessageContext(message, out context);
    }

    public CompanyMemberAddedScenarioBuilder CreateForbiddenMessageContext(out ConsumeContext<ICompanyMemberAddedMessage> context)
    {
        var message = new CompanyMemberAddedMessage(NewCompanyId(), NewUserId().ToString());

        return CreateForbiddenMessageContext(message, out context);
    }

    public CompanyMemberAddedScenarioBuilder CreateAuthenticatedMessageContext(out ConsumeContext<ICompanyMemberAddedMessage> context)
    {
        var message = new CompanyMemberAddedMessage(NewCompanyId(), NewUserId().ToString());
        
        return CreateAuthenticatedMessageContext(message, out context);
    }

    public CompanyMemberAddedScenario Build()
    {
        _ = GetRequiredService<OpenFgaFixture>().WriteReturnsOk();

        return new CompanyMemberAddedScenario(GetRequiredService<CompanyMemberAddedMessageConsumer>());
    }

    protected override void ConfigureServices(IServiceCollection services)
    {
        _ = services
            .StubOpenFga()
            .SubstituteCompanyAccessMetrics();
    }
}