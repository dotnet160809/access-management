using FluentMigrator.Runner;
using GB.AccessManagement.Infrastructure.Database;
using Microsoft.EntityFrameworkCore;

namespace GB.AccessManagement.WebApi.Configurations.Database;

public static class DatabaseConfiguration
{
    public static IServiceCollection ConfigureDatabase(this IServiceCollection services, IConfiguration configuration)
    {
        var connectionString = configuration.GetValue<string>("ConnectionStrings:Postgres");

        return services
            .AddDbContext<CompanyDbContext>(builder => builder
                .UseNpgsql(
                    connectionString,
                    options => options
                        .EnableRetryOnFailure()
                        .CommandTimeout(15)))
            .AddFluentMigratorCore()
            .ConfigureRunner(runner => runner
                .AddPostgres()
                .WithGlobalConnectionString(connectionString)
                .ScanIn(typeof(CompanyDbContext).Assembly));
    }
}