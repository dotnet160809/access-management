using OpenFga.Sdk.Model;

namespace GB.AccessManagement.Infrastructure.OpenFga.Visitors.Extensions;

internal static class ExpandResponseExtension
{
    public static async Task<string[]> Accept(this ExpandResponse response, IVisitExpandResponse visitor)
    {
        return response.Tree is not null
            ? await visitor.Visit(response.Tree)
            : [];
    }
}