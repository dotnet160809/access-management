using GB.AccessManagement.Application.Commands;
using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Application.Companies.AddCompanyChild;

public sealed record AddCompanyChildCommand(CompanyId ParentCompanyId, CompanyId ChildCompanyId) : ICommand;