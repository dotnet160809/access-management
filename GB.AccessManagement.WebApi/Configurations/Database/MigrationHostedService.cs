using FluentMigrator.Runner;
using GB.AccessManagement.Domain.Services;

namespace GB.AccessManagement.WebApi.Configurations.Database;

public sealed class MigrationHostedService : IHostedService, ISingletonService
{
    private readonly IServiceScopeFactory _factory;

    public MigrationHostedService(IServiceScopeFactory factory)
    {
        _factory = factory;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        using var scope = _factory.CreateScope();
        scope.ServiceProvider
            .GetRequiredService<IMigrationRunner>()
            .MigrateUp();
        
        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }
}