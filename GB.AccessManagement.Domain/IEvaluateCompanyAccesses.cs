using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Domain;

public interface IEvaluateCompanyAccesses
{
    Task<bool> ForOwner(UserId userId, CompanyId companyId)
    {
        return WithRelation(userId, "owner", companyId);
    }

    Task<bool> ForMember(UserId userId, CompanyId companyId)
    {
        return WithRelation(userId, "member", companyId);
    }
    
    Task<bool> WithRelation(UserId userId, Relation relation, CompanyId companyId);
}