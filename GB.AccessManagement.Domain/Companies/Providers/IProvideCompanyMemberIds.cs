using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Domain.Companies.Providers;

public interface IProvideCompanyMemberIds
{
    Task<UserId[]> ListMembersForCompany(CompanyId companyId);
}