using GB.AccessManagement.WebApi.Configurations.ProblemDetails;

namespace GB.AccessManagement.Tests.Fixtures;

public sealed class TestOutputHelperExceptionPublisher : IPublishException
{
    private readonly List<Action<Exception>> _subscribers = [];

    public void Subscribe(Action<Exception> subscriber)
    {
        _subscribers.Add(subscriber);
    }
    
    public void Publish(Exception exception)
    {
        foreach (var observer in _subscribers)
        {
            observer.Invoke(exception);
        }
    }
}