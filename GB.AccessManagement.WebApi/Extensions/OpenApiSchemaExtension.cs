using GB.AccessManagement.WebApi.Configurations.Swagger.SchemaFilters;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace GB.AccessManagement.WebApi.Extensions;

public static class OpenApiSchemaExtension
{
    public static OpenApiSchema AddLink(this OpenApiSchema schema, string linkName, SchemaFilterContext context)
    {
        if (!schema.Properties.TryGetValue("_links", out var halLinksPresentation))
        {
            return schema;
        }

        new HalLinkSchemaFilter().Apply(halLinksPresentation, linkName, context);

        return schema;

    }
    
    public static OpenApiSchema AddSelfLink(this OpenApiSchema schema, SchemaFilterContext context)
    {
        return schema.AddLink("self", context);
    }
    
    public static bool DoesNotMatchTitle(this OpenApiSchema schema, string title)
    {
        return !schema.MatchesTitle(title);
    }
    
    private static bool MatchesTitle(this OpenApiSchema schema, string title)
    {
        return schema.Title is not null
               && schema.Title.Equals(title, StringComparison.OrdinalIgnoreCase);
    }
}