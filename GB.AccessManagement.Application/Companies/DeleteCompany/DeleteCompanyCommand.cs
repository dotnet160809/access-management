using GB.AccessManagement.Application.Commands;
using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Application.Companies.DeleteCompany;

public sealed record DeleteCompanyCommand(CompanyId Id) : ICommand;