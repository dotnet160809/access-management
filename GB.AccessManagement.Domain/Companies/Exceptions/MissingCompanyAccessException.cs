using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Domain.Companies.Exceptions;

[Serializable]
public sealed class MissingCompanyAccessException : DomainException
{
    private const string MessagePattern = "User '{0}' is not {1} of company '{2}'.";
    
    public MissingCompanyAccessException(UserId userId, Relation relation, CompanyId companyId)
        : base(string.Format(MessagePattern, userId, relation, companyId)) { }
}