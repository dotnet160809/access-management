using GB.AccessManagement.Domain.Events;
using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Domain.Companies.Events;

public sealed record CompanyMemberAddedEvent(CompanyId CompanyId, UserId MemberId) : DomainEvent;