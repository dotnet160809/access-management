using GB.AccessManagement.Infrastructure.OpenFga;
using GB.AccessManagement.Tests.Assertions;
using GB.AccessManagement.Tests.Extensions;
using GB.AccessManagement.Tests.Models;
using GB.HttpRequestMessagesStub;
using NFluent;
using NSubstitute;
using Xunit.Abstractions;

namespace GB.AccessManagement.Tests.Consumers.CompanyMemberAdded;

public sealed class CompanyMemberAddedConsumerShould : AbstractFeatureShould<CompanyMemberAddedScenarioBuilder>
{
    public CompanyMemberAddedConsumerShould(ITestOutputHelper outputHelper, CompanyMemberAddedScenarioBuilder builder) : base(outputHelper, builder)
    {
    }

    [Fact]
    public async Task NotCreateAnyAuthorizationWhenConsumingUnauthenticatedMessages()
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateMessageContext(out var context)
            .Build();

        // Act
        await scenario.Execute(context);

        // Assert
        Check.That(Authorizations()).HasNotBeenCreated();
    }

    [Fact]
    public async Task NotCreateAnyAuthorizationWhenConsumingForbiddenMessages()
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateForbiddenMessageContext(out var context)
            .Build();

        // Act
        await scenario.Execute(context);

        // Assert
        Check.That(Authorizations()).HasNotBeenCreated();
    }

    [Fact]
    public async Task CreateExpectedAuthorizationWhenConsumingAuthenticatedMessage()
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateAuthenticatedMessageContext(out var context)
            .Build();

        // Act
        await scenario.Execute(context);

        // Assert
        var message = context.Message;
        await Authorizations().HasBeenCreated(ForCompanyMember(message.CompanyId, message.MemberId));
    }

    [Fact]
    public async Task IncreaseMetric()
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateAuthenticatedMessageContext(out var context)
            .Build();

        // Act
        await scenario.Execute(context);

        // Assert
        Metrics<ICountCompanyAccess>()
            .Received()
            .WhenCreated();
    }

    private static HttpRequestAssertionDelegate ForCompanyMember(Guid companyId, string memberId)
    {
        return async content =>
        {
            var document = await content.ReadAsJsonRootElement();
            Check.That(document).HasCreatedTupleKeys(new TestTupleKey($"user:{memberId}", "member", $"company:{companyId}"));
        };
    }
}