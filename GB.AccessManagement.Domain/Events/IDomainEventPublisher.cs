namespace GB.AccessManagement.Domain.Events;

public interface IDomainEventPublisher
{
    async Task Publish(DomainEvent[] events, CancellationToken cancellationToken)
    {
        foreach (var @event in events)
        {
            await Publish(@event, cancellationToken);
        }
    }

    Task Publish<TEvent>(TEvent @event, CancellationToken cancellationToken) where TEvent : DomainEvent;
}