using GB.AccessManagement.Domain.Services;
using Microsoft.EntityFrameworkCore;

namespace GB.AccessManagement.Infrastructure.Database;

public sealed class CompanyDbContext : DbContext, ISelfTransientService
{
    public DbSet<CompanyDao> Companies => Set<CompanyDao>();

    public CompanyDbContext(DbContextOptions options) : base(options) { }
}