using System.Text.Json;
using GB.AccessManagement.Tests.Models;

namespace GB.AccessManagement.Tests.Extensions;

public static class JsonElementExtension
{
    public static bool HasHalLinksProperty(this JsonElement element)
    {
        return element.TryGetProperty("_links", out _);
    }

    public static int HalLinksCount(this JsonElement element)
    {
        return element
            .GetProperty("_links")
            .AsHalLinks()
            .Count;
    }

    public static bool HasHalLinkProperty(this JsonElement element, string linkName)
    {
        return element
            .GetProperty("_links")
            .TryGetProperty(linkName, out _);
    }

    public static bool HasHalLinkHrefProperty(this JsonElement element, string linkName)
    {
        return element
            .GetProperty("_links")
            .GetProperty(linkName)
            .TryGetProperty("href", out _);
    }

    public static string GetHalLinkHrefProperty(this JsonElement element, string linkName)
    {
        return element
            .GetProperty("_links")
            .GetProperty(linkName)
            .GetProperty("href")
            .GetString() ?? string.Empty;
    }

    public static bool HasHalLinkMethodProperty(this JsonElement element, string linkName)
    {
        return element
            .GetProperty("_links")
            .GetProperty(linkName)
            .TryGetProperty("method", out _);
    }

    public static string GetHalLinkMethodProperty(this JsonElement element, string linkName)
    {
        return element
            .GetProperty("_links")
            .GetProperty(linkName)
            .GetProperty("method")
            .GetString() ?? string.Empty;
    }

    public static string GetObjectId(this JsonElement element)
    {
        if (element.TryGetProperty("tuple_key", out var tupleKey))
        {
            return tupleKey.GetProperty("object").GetString().ExtractId();
        }

        return element.TryGetProperty("object", out var @object)
            ? @object.GetString().ExtractId()
            : string.Empty;
    }

    public static string GetRelation(this JsonElement element)
    {
        if (element.TryGetProperty("tuple_key", out var tupleKey))
        {
            return tupleKey.GetProperty("relation").GetString() ?? string.Empty;
        }

        return element.TryGetProperty("relation", out var relation)
            ? relation.GetString() ?? string.Empty
            : string.Empty;
    }

    public static string GetUserId(this JsonElement element)
    {
        if (element.TryGetProperty("tuple_key", out var tupleKey))
        {
            return tupleKey.TryGetProperty("user", out var user)
                ? user.GetString().ExtractId()
                : tupleKey.GetProperty("object").GetString().ExtractId();
        }

        return element.TryGetProperty("user", out var @object)
            ? @object.GetString().ExtractId()
            : string.Empty;
    }

    private static TestHalLinks AsHalLinks(this JsonElement element)
    {
        return element.Deserialize<TestHalLinks>() ?? new TestHalLinks();
    }
}