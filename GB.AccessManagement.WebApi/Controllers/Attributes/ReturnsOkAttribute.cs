using Swashbuckle.AspNetCore.Annotations;

namespace GB.AccessManagement.WebApi.Controllers.Attributes;

public sealed class ReturnsOkAttribute(Type responseType, string? contentType = SupportedContentTypes.Json) : SwaggerResponseAttribute(StatusCodes.Status200OK, type: responseType, contentTypes: contentType);