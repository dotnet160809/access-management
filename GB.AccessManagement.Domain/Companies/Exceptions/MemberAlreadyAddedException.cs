using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Domain.Companies.Exceptions;

[Serializable]
public sealed class MemberAlreadyAddedException : DomainException
{
    private const string MessagePattern = "User '{0}' already is member of company '{1}'.";
    
    public MemberAlreadyAddedException(UserId memberId, CompanyId companyId)
        : base(string.Format(MessagePattern, memberId, companyId)) { }
}