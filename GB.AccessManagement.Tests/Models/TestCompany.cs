using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;
using GB.HttpRequestMessagesStub;

namespace GB.AccessManagement.Tests.Models;

public sealed record TestCompany(
    [property: JsonPropertyName("id")] Guid Id,
    [property: JsonPropertyName("name")] string Name,
    [property: JsonPropertyName("parentCompanyId")] Guid? ParentCompanyId)
{
    [JsonPropertyName("_links")]
    public TestHalLinks Links { get; init; } = new();

    public void AddLinks(TestHalLinks links)
    {
        foreach (var link in links)
        {
            Links.Add(link.Key, link.Value);
        }
    }

    [SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
    public override int GetHashCode()
    {
        unchecked
        {
            var hashCode = Id.GetHashCode();
            hashCode = (hashCode * 397) ^ Name.GetOrdinalHashCode();
            hashCode = (hashCode * 397) ^ (ParentCompanyId is not null ? ParentCompanyId.Value.GetHashCode() : 0);

            return (hashCode * 397) ^ Links.GetHashCode();
        }
    }

    public bool Equals(TestCompany? other)
    {
        return other is not null
               && Id == other.Id
               && Name.OrdinalEquals(other.Name)
               && ParentCompanyId.Equals(other.ParentCompanyId)
               && Links.Equals(other.Links);
    }
}