using Swashbuckle.AspNetCore.Annotations;

namespace GB.AccessManagement.WebApi.Controllers.Attributes;

public sealed class ReturnsNoContentAttribute() : SwaggerResponseAttribute(StatusCodes.Status204NoContent);