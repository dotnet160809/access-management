using System.Diagnostics;

namespace GB.AccessManagement.WebApi.Configurations.OpenTelemetry;

public static class AccessManagementActivity
{
    public static readonly ActivitySource Source = CreateActivitySource();

    private static ActivitySource CreateActivitySource()
    {
        var assembly = typeof(Startup).Assembly;
        var serviceName = assembly.FullName?
            .Split(',')
            .First() ?? "GB.AccessManagement.WebApi";
        var serviceVersion = assembly.FullName?
            .Split(',')
            .ElementAt(1)
            .Replace("Version=", string.Empty) ?? "1.0.0.0";

        return new ActivitySource(serviceName, serviceVersion);
    }
}