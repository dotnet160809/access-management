using GB.AccessManagement.Infrastructure.Database;
using GB.AccessManagement.Infrastructure.OpenFga;
using GB.AccessManagement.Tests.Assertions;
using GB.AccessManagement.Tests.Extensions;
using GB.AccessManagement.Tests.Models;
using GB.HttpRequestMessagesStub;
using NFluent;
using NSubstitute;
using Xunit.Abstractions;

namespace GB.AccessManagement.Tests.Consumers.CompanyMemberRemoved;

public sealed class CompanyMemberRemovedConsumerShould : AbstractFeatureShould<CompanyMemberRemovedScenarioBuilder>
{
    public CompanyMemberRemovedConsumerShould(ITestOutputHelper outputHelper,
        CompanyMemberRemovedScenarioBuilder builder) : base(outputHelper, builder)
    {
    }

    [Fact]
    public async Task NotRemoveAnyAuthorizationWhenConsumingUnauthenticatedMessage()
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateMessageContext(out var context)
            .Build();

        // Act
        await scenario.Execute(context);

        // Assert
        Check.That(Authorizations()).HasNotBeenRemoved();
    }

    [Fact]
    public async Task NotRemoveAnyAuthorizationWhenConsumingForbiddenMessage()
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateForbiddenMessageContext(out var context)
            .Build();

        // Act
        await scenario.Execute(context);

        // Assert
        Check.That(Authorizations()).HasNotBeenRemoved();
    }

    [Fact]
    public async Task RemoveExpectedAuthorizationWhenConsumingAuthenticatedMessage()
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateAuthenticatedMessageContext(out var context)
            .Build();

        // Act
        await scenario.Execute(context);

        // Assert
        var message = context.Message;
        await Authorizations().HasBeenCreated(ForCompanyMember(message.CompanyId, message.MemberId));
    }

    [Fact]
    public async Task IncreaseMetric()
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateAuthenticatedMessageContext(out var context)
            .Build();

        // Act
        await scenario.Execute(context);

        // Assert
        Metrics<ICountCompanyAccess>()
            .Received()
            .WhenDeleted();
    }

    private static HttpRequestAssertionDelegate ForCompanyMember(Guid companyId, string memberId)
    {
        return async content =>
        {
            var document = await content.ReadAsJsonRootElement();
            Check.That(document).HasRemovedTupleKeys(new TestTupleKey($"user:{memberId}", "member", $"company:{companyId}"));
        };
    }
}