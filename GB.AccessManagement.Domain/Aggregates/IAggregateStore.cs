using GB.AccessManagement.Domain.Aggregates.Memos;

namespace GB.AccessManagement.Domain.Aggregates;

public interface IAggregateStore<TAggregate, in TAggregateId, in TMemo>
    where TAggregate : AggregateRoot<TAggregate, TAggregateId, TMemo>
    where TAggregateId : notnull
    where TMemo : IAggregateMemo
{
    Task Save(TAggregate aggregate, CancellationToken cancellationToken);

    Task<TAggregate> Load(TAggregateId id);
}