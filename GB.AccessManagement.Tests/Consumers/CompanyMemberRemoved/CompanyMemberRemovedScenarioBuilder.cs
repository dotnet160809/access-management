using GB.AccessManagement.Application.Companies.RemoveCompanyMember;
using GB.AccessManagement.Domain.Companies.Messages;
using GB.AccessManagement.Infrastructure.MassTransit.Consumers;
using GB.AccessManagement.Tests.Extensions;
using GB.AccessManagement.Tests.Fixtures.OpenFga;
using MassTransit;
using Microsoft.Extensions.DependencyInjection;

namespace GB.AccessManagement.Tests.Consumers.CompanyMemberRemoved;

public sealed class CompanyMemberRemovedScenarioBuilder : AbstractConsumerScenarioBuilder<CompanyMemberRemovedScenarioBuilder>
{
    public CompanyMemberRemovedScenarioBuilder CreateMessageContext(out ConsumeContext<ICompanyMemberRemovedMessage> context)
    {
        var message = new CompanyMemberRemovedMessage(NewCompanyId(), NewUserId().ToString());

        return CreateMessageContext(message, out context);
    }
    
    public CompanyMemberRemovedScenarioBuilder CreateForbiddenMessageContext(out ConsumeContext<ICompanyMemberRemovedMessage> context)
    {
        var message = new CompanyMemberRemovedMessage(NewCompanyId(), NewUserId().ToString());

        return CreateForbiddenMessageContext(message, out context);
    }
    
    public CompanyMemberRemovedScenarioBuilder CreateAuthenticatedMessageContext(out ConsumeContext<ICompanyMemberRemovedMessage> context)
    {
        var message = new CompanyMemberRemovedMessage(NewCompanyId(), NewUserId().ToString());

        return CreateAuthenticatedMessageContext(message, out context);
    }

    public CompanyMemberRemovedScenario Build()
    {
        _ = GetRequiredService<OpenFgaFixture>().WriteReturnsOk();
        var consumer = GetRequiredService<CompanyMemberRemovedMessageConsumer>();

        return new CompanyMemberRemovedScenario(consumer);
    }

    protected override void ConfigureServices(IServiceCollection services)
    {
        _ = services
            .StubOpenFga()
            .SubstituteCompanyAccessMetrics();
    }
}