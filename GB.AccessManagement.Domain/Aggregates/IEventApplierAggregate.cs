using GB.AccessManagement.Domain.Aggregates.Memos;
using GB.AccessManagement.Domain.Events;

namespace GB.AccessManagement.Domain.Aggregates;

public interface IEventApplierAggregate<in TEvent, in TMemo>
    where TEvent : DomainEvent
    where TMemo : IAggregateMemo
{
    void Apply(TEvent @event, TMemo memo);
}