using System.Net.Http.Json;

namespace GB.AccessManagement.Tests.Endpoints.AddCompanyMember;

public sealed class AddCompanyMemberScenario(HttpClient client, int apiVersion)
{
    public Task<HttpResponseMessage> Execute(Guid companyId, Guid memberId)
    {
        return Execute(companyId, memberId.ToString());
    }

    public Task<HttpResponseMessage> Execute(Guid companyId, string? memberId)
    {
        return client.PostAsync($"/v{apiVersion}/companies/{companyId}/members", JsonContent.Create(new { memberId }));
    }
}