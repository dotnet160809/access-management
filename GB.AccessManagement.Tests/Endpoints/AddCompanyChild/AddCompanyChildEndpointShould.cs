using System.Net;
using GB.AccessManagement.Tests.Assertions;
using NFluent;
using Xunit.Abstractions;

namespace GB.AccessManagement.Tests.Endpoints.AddCompanyChild;

public sealed class AddCompanyChildEndpointShould : AbstractFeatureShould<AddCompanyChildScenarioBuilder>
{
    public AddCompanyChildEndpointShould(ITestOutputHelper outputHelper, AddCompanyChildScenarioBuilder builder)
        : base(outputHelper, builder)
    {
    }

    [Theory]
    [InlineData(1)]
    public async Task ReturnUnauthorizedWhenUserIsNotAuthentication(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var parent)
            .CreateCompany(out var child)
            .Build(apiVersion);

        // Act
        var response = await scenario.Execute(parent.Id, child.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.Unauthorized);
    }

    [Theory]
    [InlineData(1, false, false)]
    [InlineData(1, false, true)]
    [InlineData(1, true, false)]
    public async Task ReturnForbiddenWhenUserIsNotOwnerOfBothCompanies(
        int apiVersion,
        bool isUserParentCompanyOwner,
        bool isUserChildCompanyOwner)
    {
        // Arrange
        _ = ScenarioBuilder
            .CreateCompany(out var parent)
            .CreateCompany(out var child)
            .CreateUser(out var user);

        _ = isUserParentCompanyOwner
            ? ScenarioBuilder.AddCompanyOwner(parent.Id, user.Id)
            : ScenarioBuilder.AddCompanyMember(parent.Id, user.Id);

        _ = isUserChildCompanyOwner
            ? ScenarioBuilder.AddCompanyOwner(child.Id, user.Id)
            : ScenarioBuilder.AddCompanyMember(child.Id, user.Id);

        var scenario = ScenarioBuilder
            .AuthenticateUser(user)
            .Build(apiVersion);

        // Act
        var response = await scenario.Execute(parent.Id, child.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.Forbidden);
    }

    [Theory]
    [InlineData(1)]
    public async Task ReturnUnprocessableEntityWhenCompanyIsAlreadyAddedAsChild(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var parent)
            .CreateCompany(parent.Id, out var child)
            .CreateUser(out var user)
            .AddCompanyOwner(parent.Id, user.Id)
            .AddCompanyOwner(child.Id, user.Id)
            .AuthenticateUser(user)
            .Build(apiVersion);

        // Act
        var response = await scenario.Execute(parent.Id, child.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.UnprocessableEntity);
    }

    [Theory]
    [InlineData(1)]
    public async Task UpdateStoredChildCompany(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var parent)
            .CreateCompany(out var child)
            .CreateUser(out var user)
            .AddCompanyOwner(parent.Id, user.Id)
            .AddCompanyOwner(child.Id, user.Id)
            .AuthenticateUser(user)
            .Build(apiVersion);

        // Act
        var response = await scenario.Execute(parent.Id, child.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.OK);
        
        Check.That(Companies()).HasSingleCompanyMatching(company => company.Id == child.Id && company.ParentCompanyId == parent.Id);
    }
}