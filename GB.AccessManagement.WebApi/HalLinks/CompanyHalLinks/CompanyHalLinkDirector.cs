using GB.AccessManagement.Application.Companies.GetCompanyMembers;
using GB.AccessManagement.Application.Presentations;
using GB.AccessManagement.Application.Queries;
using GB.AccessManagement.Domain;
using GB.AccessManagement.Domain.Companies.Providers;
using GB.AccessManagement.Domain.Services;

namespace GB.AccessManagement.WebApi.HalLinks.CompanyHalLinks;

public sealed class CompanyHalLinkDirector : ISelfTransientService
{
    private readonly IQueryDispatcher _dispatcher;
    private readonly IEvaluateCompanyAccesses _companyAccesses;
    private readonly IProvideCompanyParentId _companyParent;
    private readonly IProvideCompanyChildrenIds _companyChildren;
    private readonly CompanyHalLinkBuilder _builder;

    public CompanyHalLinkDirector(
        IQueryDispatcher dispatcher,
        IEvaluateCompanyAccesses companyAccesses,
        IProvideCompanyParentId companyParent,
        IProvideCompanyChildrenIds companyChildren,
        CompanyHalLinkBuilder builder)
    {
        _dispatcher = dispatcher;
        _companyAccesses = companyAccesses;
        _companyParent = companyParent;
        _companyChildren = companyChildren;
        _builder = builder;
    }

    public async Task<CompanyPresentation> MakeHalLinks(CompanyPresentation presentation, string userId, CancellationToken cancellationToken)
    {
        if (!await _companyAccesses.ForMember(userId, presentation.Id.ToString()))
        {
            return presentation;
        }

        _ = _builder
            .BuildGetCompanyLink(presentation)
            .BuildGetOwnerLink(presentation);

        if (await HasParent(presentation.Id))
        {
            _ = _builder.BuildGetParentLink(presentation);
        }

        if (await HasAnyChild(presentation.Id))
        {
            _ = _builder.BuildGetChildrenLink(presentation);
        }

        if (await _companyAccesses.ForOwner(userId, presentation.Id.ToString()))
        {
            _ = _builder.BuildAddMemberLink(presentation);
        }

        return await MakeMembersLinks(presentation, cancellationToken);
    }

    private async Task<bool> HasParent(Guid companyId)
    {
        var parentCompanyId = await _companyParent.FindParentForCompany(companyId);

        return parentCompanyId is not null;
    }

    private async Task<bool> HasAnyChild(Guid companyId)
    {
        var childrenCompanyIds = await _companyChildren.ForCompany(companyId);

        return childrenCompanyIds.Length > 0;
    }

    private async Task<CompanyPresentation> MakeMembersLinks(CompanyPresentation presentation, CancellationToken cancellationToken)
    {
        var members = await GetCompanyMembers(presentation.Id, cancellationToken);

        if (members.Total <= 0)
        {
            return presentation;
        }

        if (members.Items.Length > 0)
        {
            _ = _builder.BuildGetMembersLink(presentation);
        }

        return presentation;
    }

    private Task<CompanyMembersPresentation> GetCompanyMembers(Guid companyId, CancellationToken cancellationToken)
    {
        CompanyMembersQuery query = new(companyId);

        return _dispatcher.Dispatch(query, cancellationToken);
    }
}