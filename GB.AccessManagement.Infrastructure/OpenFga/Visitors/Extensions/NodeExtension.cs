using OpenFga.Sdk.Model;

namespace GB.AccessManagement.Infrastructure.OpenFga.Visitors.Extensions;

internal static class NodeExtension
{
    public static async Task<string[]> Accept(this Node? node, IVisitExpandResponse visitor)
    {
        return node is not null
            ? await visitor.Visit(node)
            : [];
    }
}