namespace GB.AccessManagement.WebApi.Controllers.Responses;

[Serializable]
public sealed record CompanyCreatedResponse(Guid Id, string Name, Guid? ParentCompanyId);