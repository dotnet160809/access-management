namespace GB.AccessManagement.WebApi.Controllers.Attributes;

public sealed class ReturnsForbiddenAttribute() : ProblemResponseTypeAttribute(StatusCodes.Status403Forbidden);