using OpenFga.Sdk.Model;

namespace GB.AccessManagement.Infrastructure.OpenFga.Visitors.Extensions;

internal static class ComputedListExtension
{
    public static async Task<string[]> Accept(this ICollection<Computed> list, IVisitExpandResponse visitor)
    {
        return list.Count != 0
            ? await visitor.Visit(list)
            : [];
    }
}