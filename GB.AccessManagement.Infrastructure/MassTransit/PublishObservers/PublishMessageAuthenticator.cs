using GB.AccessManagement.Domain.Messages;
using MassTransit;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;

namespace GB.AccessManagement.Infrastructure.MassTransit.PublishObservers;

public sealed class PublishMessageAuthenticator : IPublishObserver
{
    private readonly IOptionsMonitor<MessageQueueOptions> _options;

    public PublishMessageAuthenticator(IOptionsMonitor<MessageQueueOptions> options)
    {
        _options = options;
    }

    public Task PrePublish<T>(PublishContext<T> context) where T : class
    {
        if (context.Message is IMessage message)
        {
            context.Headers.Set(HeaderNames.Authorization, _options.CurrentValue.Hash(message.Id.ToString()));
        }

        return Task.CompletedTask;
    }

    public Task PostPublish<T>(PublishContext<T> context) where T : class
    {
        return Task.CompletedTask;
    }

    public Task PublishFault<T>(PublishContext<T> context, Exception exception) where T : class
    {
        return Task.CompletedTask;
    }
}