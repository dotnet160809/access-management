namespace GB.AccessManagement.Tests.Endpoints.GetCompanyOwner;

public sealed class GetCompanyOwnerScenario(HttpClient client, int apiVersion)
{
    public Task<HttpResponseMessage> Execute(Guid companyId)
    {
        return client.GetAsync($"/v{apiVersion}/companies/{companyId}/owner");
    }
}