using FluentMigrator;

namespace GB.AccessManagement.Infrastructure.Database.Migrations;

[TimestampedMigration(2023, 12, 19, 9, 10)]
public sealed class RemoveCompanyOwnerColumnMigration : ForwardOnlyMigration
{
    public override void Up()
    {
        Delete
            .Column(CompanyDao.OwnerIdColumnName)
            .FromTable(CompanyDao.TableName);
    }
}