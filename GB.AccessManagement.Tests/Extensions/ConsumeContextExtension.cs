using MassTransit;
using MassTransit.Serialization;
using Microsoft.Net.Http.Headers;
using NSubstitute;

namespace GB.AccessManagement.Tests.Extensions;

public static class ConsumeContextExtension
{
    public static ConsumeContext<TMessage> WithAuthorizationHeader<TMessage>(this ConsumeContext<TMessage> context, string headerValue) where TMessage : class
    {
        context.Headers.Returns(new DictionarySendHeaders(new Dictionary<string, object?>
        {
            [HeaderNames.Authorization] = headerValue
        }));

        return context;
    }
}