using System.Security.Claims;

namespace GB.AccessManagement.WebApi.Extensions;

public static class ClaimsPrincipalExtension
{
    public static Guid Id(this ClaimsPrincipal principal)
    {
        var claim = principal.FindFirstValue(ClaimTypes.NameIdentifier);

        return claim is not null && Guid.TryParse(claim, out var userId)
            ? userId
            : throw new InvalidCastException("User is either not authenticated or ID is missing from claims");
    }
}