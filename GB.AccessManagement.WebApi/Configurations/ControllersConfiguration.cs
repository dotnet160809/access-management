using Microsoft.AspNetCore.Mvc.Infrastructure;

namespace GB.AccessManagement.WebApi.Configurations;

public static class ControllersConfiguration
{
    public static IServiceCollection ConfigureControllers(this IServiceCollection services)
    {
        return services
            .AddRouting()
            .AddControllers()
            .Services
            .AddTransient<IActionContextAccessor, ActionContextAccessor>();
    }
}