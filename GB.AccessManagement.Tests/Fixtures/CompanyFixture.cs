using GB.AccessManagement.Infrastructure.Database;
using GB.AccessManagement.Infrastructure.Database.Extensions;
using GB.AccessManagement.Tests.Models;

namespace GB.AccessManagement.Tests.Fixtures;

public sealed class CompanyFixture(CompanyDbContext dbContext)
{
    private readonly Dictionary<Guid, ICollection<Guid>> _companyMembers = new();

    public void AddCompany(TestCompany company)
    {
        dbContext.Companies.Add(new CompanyDao
        {
            Id = company.Id,
            Name = company.Name,
            ParentCompanyId = company.ParentCompanyId
        });
        dbContext.SaveChanges();
    }

    public void AddCompanyOwner(Guid companyId, Guid ownerId)
    {
        var company = dbContext.Companies.ForCompany(companyId).Single();
        company.OwnerId = ownerId;
        dbContext.SaveChanges();
    }

    public void AddCompanyMember(Guid companyId, Guid memberId)
    {
        if (_companyMembers.TryGetValue(companyId, out var members))
        {
            members.Add(memberId);

            return;
        }

        members = new List<Guid> { memberId };
        _companyMembers[companyId] = members;
    }

    public void RemoveCompanyMember(Guid companyId, Guid memberId)
    {
        if (_companyMembers.TryGetValue(companyId, out var members))
        {
            members.Remove(memberId);
        }
    }

    public IEnumerable<string> GetCompanyMembers(Guid companyId)
    {
        return _companyMembers.TryGetValue(companyId, out var members)
            ? members.Select(member => member.ToString())
            : [];
    }

    public string GetCompanyOwner(Guid companyId)
    {
        return dbContext.Companies
            .ForCompany(companyId)
            .Single()
            .OwnerId.ToString();
    }

    public IEnumerable<Guid> GetUserCompanies(Guid userId)
    {
        return _companyMembers
            .Where(pair => pair.Value.Contains(userId))
            .Select(pair => pair.Key)
            .Distinct();
    }

    public bool IsCompanyMember(Guid companyId, Guid userId)
    {
        return _companyMembers.TryGetValue(companyId, out var members)
               && members.Contains(userId);
    }

    public bool IsCompanyOwner(Guid companyId, Guid userId)
    {
        return dbContext.Companies
            .ForCompany(companyId)
            .Single()
            .OwnerId == userId;
    }
}