using GB.AccessManagement.Domain.Services;
using GB.AccessManagement.Infrastructure.OpenFga;
using Microsoft.Extensions.Options;

namespace GB.AccessManagement.WebApi.Configurations.HostedServices;

public sealed class OpenFgaHostedService : IHostedService, ISingletonService
{
    private readonly IServiceScopeFactory _factory;
    private readonly IOptionsMonitor<OpenFgaOptions> _options;

    public OpenFgaHostedService(IServiceScopeFactory factory, IOptionsMonitor<OpenFgaOptions> options)
    {
        _factory = factory;
        _options = options;
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        using var scope = _factory.CreateScope();
        var storeInitializer = scope.ServiceProvider.GetRequiredService<StoreInitializer>();
        _options.CurrentValue.StoreId = await storeInitializer.GetOrCreateStoreId();
        
        var authorizationModelInitializer = scope.ServiceProvider.GetRequiredService<AuthorizationModelInitializer>();

        if (!_options.CurrentValue.AuthorizationModel.ForceUpdate
            && await authorizationModelInitializer.HasStoreAnyAuthorizationModel())
        {
            return;
        }

        await authorizationModelInitializer.InitializeAuthorizationModel();
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }
}