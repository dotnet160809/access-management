namespace GB.AccessManagement.Domain.ValueObjects;

public sealed record Relation
{
    private readonly string _value;

    private Relation(string value)
    {
        value = value.Trim();

        if (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value))
        {
            throw new ArgumentNullException(nameof(value));
        }

        _value = value;
    }

    public override string ToString()
    {
        return _value;
    }

    public static implicit operator Relation(string value)
    {
        return new Relation(value);
    }

    public static implicit operator string(Relation objectType)
    {
        return objectType._value;
    }
}