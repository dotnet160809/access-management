namespace GB.AccessManagement.Domain.Aggregates.Memos;

public interface IAggregateMemo
{
    EMemoState State { get; set; }
}