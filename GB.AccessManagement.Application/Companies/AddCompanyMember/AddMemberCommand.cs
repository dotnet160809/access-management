using GB.AccessManagement.Application.Commands;
using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Application.Companies.AddCompanyMember;

public sealed record AddMemberCommand : ICommand
{
    internal readonly CompanyId CompanyId;
    internal readonly UserId MemberId;

    public AddMemberCommand(Guid companyId, Guid memberId)
    {
        CompanyId = companyId;
        MemberId = memberId;
    }
}