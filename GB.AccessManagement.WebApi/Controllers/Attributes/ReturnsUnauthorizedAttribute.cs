namespace GB.AccessManagement.WebApi.Controllers.Attributes;

public sealed class ReturnsUnauthorizedAttribute() : ProblemResponseTypeAttribute(StatusCodes.Status401Unauthorized);