using System.Net;
using System.Net.Http.Json;
using GB.AccessManagement.Infrastructure.OpenFga;
using GB.HttpRequestMessagesStub;
using Microsoft.Extensions.Options;
using OpenFga.Sdk.Model;

namespace GB.AccessManagement.Tests.Fixtures.OpenFga;

public sealed class OpenFgaFixture(IStubHttpRequestMessages fixture, IOptionsMonitor<OpenFgaOptions> options) : IAssertOpenFga
{
    public OpenFgaFixture CheckReturns(Func<HttpRequestMessage, Task<HttpResponseMessage>> function)
    {
        _ = fixture
            .StubHttpRequest(Host(), $"/stores/{StoreId()}/check", HttpMethod.Post)
            .WithResponse(request => function(request));

        return this;
    }

    public OpenFgaFixture ExpandReturns(IEnumerable<string> userIds)
    {
        return ExpandReturns(_ => Task.FromResult(userIds));
    }

    public OpenFgaFixture ExpandReturns(Func<HttpRequestMessage, Task<IEnumerable<string>>> function)
    {
        _ = fixture
            .StubHttpRequest(Host(), $"/stores/{StoreId()}/expand", HttpMethod.Post)
            .WithResponse(async request =>
            {
                var userIds = await function(request);

                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = JsonContent.Create(new ExpandResponse
                    {
                        Tree = new UsersetTree
                        {
                            Root = new Node
                            {
                                Leaf = new Leaf
                                {
                                    Users = new Users
                                    {
                                        _Users = userIds.ToList()
                                    }
                                }
                            }
                        }
                    })
                };
            });

        return this;
    }

    public OpenFgaFixture ListObjectsReturns(IEnumerable<string> objectIds)
    {
        _ = fixture
            .StubHttpRequest(Host(), $"/stores/{StoreId()}/list-objects", HttpMethod.Post)
            .With200OkResponse(new ListObjectsResponse{Objects = objectIds.ToList()});

        return this;
    }

    public OpenFgaFixture ListObjectsReturns(Func<HttpRequestMessage, Task<IEnumerable<string>>> function)
    {
        _ = fixture
            .StubHttpRequest(Host(), $"/stores/{StoreId()}/list-objects", HttpMethod.Post)
            .WithResponse(async request =>
            {
                var objectIds = await function(request);

                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = JsonContent.Create(new ListObjectsResponse(objectIds.ToList()))
                };
            });

        return this;
    }

    public OpenFgaFixture WriteReturnsOk()
    {
        _ = fixture
            .StubHttpRequest(Host(), WritePath(), HttpMethod.Post)
            .With200OkResponse();

        return this;
    }

    public bool HasNotBeenCreated()
    {
        return fixture
            .AssertHttpRequest(Host(), WritePath(), HttpMethod.Post)
            .HasNotBeenSent();
    }

    public Task HasBeenCreated(HttpRequestAssertionDelegate assertion)
    {
        return fixture
            .AssertHttpRequest(Host(), WritePath(), HttpMethod.Post)
            .HasBeenSentOnce(assertion);
    }

    public Task HasBeenRemoved(HttpRequestAssertionDelegate assertion)
    {
        return fixture
            .AssertHttpRequest(Host(), WritePath(), HttpMethod.Post)
            .HasBeenSentOnce(assertion);
    }

    public bool HasNotBeenRemoved()
    {
        return fixture
            .AssertHttpRequest(Host(), WritePath(), HttpMethod.Post)
            .HasNotBeenSent();
    }

    private string Host()
    {
        return options.CurrentValue.BaseAddress;
    }

    private string WritePath()
    {
        return $"/stores/{StoreId()}/write";
    }

    private string StoreId()
    {
        return options.CurrentValue.StoreId;
    }
}