using Asp.Versioning;

namespace GB.AccessManagement.WebApi.Configurations;

public static class VersioningConfiguration
{
    public static IServiceCollection ConfigureVersioning(this IServiceCollection services)
    {
        return services
            .AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;
                options.DefaultApiVersion = new ApiVersion(1, 0);
                options.AssumeDefaultVersionWhenUnspecified = true;
            })
            .AddApiExplorer(options =>
            {
                options.GroupNameFormat = "'v'VVV";
                options.SubstituteApiVersionInUrl = true;
            })
            .Services;
    }
}