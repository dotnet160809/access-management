using GB.AccessManagement.Domain.ValueObjects;
using OpenFga.Sdk.Model;

namespace GB.AccessManagement.Infrastructure.OpenFga.Extensions;

public static class AccessExtension
{
    public static List<TupleKey> ToTupleKeys(this Access[] accesses)
    {
        return accesses.Select(ToTupleKey).ToList();
    }

    public static List<TupleKeyWithoutCondition> ToTupleKeysWithoutCondition(this Access[] accesses)
    {
        return accesses.Select(ToTupleKeyWithoutCondition).ToList();
    }

    private static TupleKey ToTupleKey(Access access)
    {
        return new TupleKey
        {
            Object = access.Object,
            Relation = access.Relation,
            User = access.User
        };
    }

    private static TupleKeyWithoutCondition ToTupleKeyWithoutCondition(Access access)
    {
        return new TupleKeyWithoutCondition
        {
            Object = access.Object,
            Relation = access.Relation,
            User = access.User
        };
    }
}