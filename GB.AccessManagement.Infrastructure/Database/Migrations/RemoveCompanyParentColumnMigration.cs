using FluentMigrator;

namespace GB.AccessManagement.Infrastructure.Database.Migrations;

[TimestampedMigration(2023, 12, 19, 9, 0)]
public sealed class RemoveCompanyParentColumnMigration : ForwardOnlyMigration
{
    public override void Up()
    {
        Delete
            .Column(CompanyDao.ParentIdColumnName)
            .FromTable(CompanyDao.TableName);
    }
}