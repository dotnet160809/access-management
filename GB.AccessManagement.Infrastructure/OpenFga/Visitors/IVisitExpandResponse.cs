using OpenFga.Sdk.Model;

namespace GB.AccessManagement.Infrastructure.OpenFga.Visitors;

internal interface IVisitExpandResponse
{
    Task<string[]> Visit(ExpandResponse response, bool expand);

    Task<string[]> Visit(UsersetTree tree);

    Task<string[]> Visit(Node node);

    Task<string[]> Visit(UsersetTreeDifference difference);

    Task<string[]> Visit(Leaf leaf);

    Task<string[]> Visit(IEnumerable<Computed> list);

    Task<string[]> Visit(Computed computed);

    Task<string[]> Visit(string value);

    Task<string[]> Visit(UsersetTreeTupleToUserset userSet);

    Task<string[]> Visit(Users users);
}