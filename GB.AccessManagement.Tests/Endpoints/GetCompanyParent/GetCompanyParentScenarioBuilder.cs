using GB.AccessManagement.Tests.Extensions;
using GB.AccessManagement.Tests.Fixtures.OpenFga;
using Microsoft.Extensions.DependencyInjection;

namespace GB.AccessManagement.Tests.Endpoints.GetCompanyParent;

public sealed class GetCompanyParentScenarioBuilder : AbstractEndpointScenarioBuilder<GetCompanyParentScenarioBuilder>
{
    public GetCompanyParentScenario Build(int apiVersion)
    {
        _ = GetRequiredService<OpenFgaFixture>()
            .CheckReturns(CompanyOwnerOrMembers)
            .ExpandReturns(async request =>
            {
                var document = await request.ReadRootJsonElement();
                var relation = document.GetRelation();
                var companyId = Guid.Parse(document.GetObjectId());

                return relation switch
                {
                    "owner" => CompanyOwner(companyId),
                    "member" => CompanyMembers(companyId),
                    _ => Array.Empty<string>()
                };
            })
            .ListObjectsReturns(async request =>
            {
                var document = await request.ReadRootJsonElement();
                var relation = document.GetRelation();
                var companyId = Guid.Parse(document.GetUserId());

                return relation switch
                {
                    "owner" => CompanyOwner(companyId),
                    "member" => CompanyMembers(companyId),
                    _ => Array.Empty<string>()
                };
            });

        return new GetCompanyParentScenario(CreateClient(), apiVersion);
    }

    protected override void ConfigureServices(IServiceCollection services)
    {
        _ = services
            .StubAuthentication()
            .AddCompanyFixture()
            .StubOpenFga();
    }
}