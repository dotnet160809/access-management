namespace GB.AccessManagement.Tests.Endpoints.GetCompanyParent;

public sealed class GetCompanyParentScenario(HttpClient client, int apiVersion)
{
    public Task<HttpResponseMessage> Execute(Guid companyId)
    {
        return client.GetAsync($"/v{apiVersion}/companies/{companyId}/parent");
    }
}