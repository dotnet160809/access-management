using GB.AccessManagement.Domain.Aggregates.Memos;

namespace GB.AccessManagement.Domain.Aggregates;

public interface IAggregateRootLoader<out TAggregate, TAggregateId, in TAggregateMemo>
    where TAggregate : AggregateRoot<TAggregate, TAggregateId, TAggregateMemo>
    where TAggregateId : notnull
    where TAggregateMemo : IAggregateMemo
{
    TAggregate Load(TAggregateMemo memo);
}