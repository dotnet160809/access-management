using GB.AccessManagement.Domain.Companies.Messages;
using GB.AccessManagement.Infrastructure.MassTransit.Consumers;
using MassTransit;

namespace GB.AccessManagement.Tests.Consumers.CompanyCreated;

public sealed class CompanyCreatedScenario(CompanyCreatedMessageConsumer consumer)
{
    public Task Execute(ConsumeContext<ICompanyCreatedMessage> context)
    {
        return consumer.Consume(context);
    }
}