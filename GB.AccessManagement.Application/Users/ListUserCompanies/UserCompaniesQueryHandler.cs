using GB.AccessManagement.Application.Presentations;
using GB.AccessManagement.Application.Queries;
using GB.AccessManagement.Domain.Companies.Providers;

namespace GB.AccessManagement.Application.Users.ListUserCompanies;

public sealed class UserCompaniesQueryHandler : IQueryHandler<UserCompaniesQuery, CompaniesPresentation>
{
    private readonly IProvideUserCompanyIds _provider;
    private readonly IReadCompany _repository;

    public UserCompaniesQueryHandler(IProvideUserCompanyIds provider, IReadCompany repository)
    {
        _provider = provider;
        _repository = repository;
    }

    public async Task<CompaniesPresentation> Handle(UserCompaniesQuery request, CancellationToken cancellationToken)
    {
        var companyIds = await _provider.ListCompaniesForUser(request.UserId);

        if (companyIds.Length == 0)
        {
            return new CompaniesPresentation([], 0);
        }
        
        var companies = await _repository.List(companyIds);

        return new CompaniesPresentation(companies, companies.Length);
    }
}