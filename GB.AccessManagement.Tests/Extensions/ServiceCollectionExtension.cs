using GB.AccessManagement.Infrastructure.Database;
using GB.AccessManagement.Infrastructure.OpenFga;
using GB.AccessManagement.Tests.Fixtures;
using GB.AccessManagement.Tests.Fixtures.OpenFga;
using GB.AccessManagement.WebApi.Configurations.Authentication;
using GB.HttpRequestMessagesStub;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using NSubstitute;

namespace GB.AccessManagement.Tests.Extensions;

public static class ServiceCollectionExtension
{
    public static IServiceCollection AddCompanyFixture(this IServiceCollection services)
    {
        return services.AddSingleton<CompanyFixture>();
    }

    public static IServiceCollection AddFixture<TFixture>(this IServiceCollection services) where TFixture : class
    {
        _ = services.AddSingleton<TFixture>();

        foreach (var @interface in typeof(TFixture).GetInterfaces())
        {
            _ = services
                .RemoveAll(@interface)
                .AddTransient(@interface, typeof(TFixture));
        }

        return services;
    }

    public static IServiceCollection StubAuthentication(this IServiceCollection services)
    {
        return services
            .RemoveAll<IValidateAuthenticationToken>()
            .AddSingleton<AuthenticationFixture>()
            .AddTransient<IValidateAuthenticationToken>(GetRequiredService<AuthenticationFixture>);
    }

    public static IServiceCollection StubOpenFga(this IServiceCollection services)
    {
        return services
            .StubHttpRequests()
            .AddTransient<OpenFgaFixture>();
    }

    public static IServiceCollection SubstituteCompanyMetrics(this IServiceCollection services)
    {
        return services
            .RemoveAll<ICountCompanies>()
            .AddSingleton(_ => Substitute.For<ICountCompanies>());
    }

    public static IServiceCollection SubstituteCompanyAccessMetrics(this IServiceCollection services)
    {
        return services
            .RemoveAll<ICountCompanyAccess>()
            .AddSingleton(_ => Substitute.For<ICountCompanyAccess>());
    }

    private static TService GetRequiredService<TService>(IServiceProvider provider) where TService : notnull
    {
        return provider.GetRequiredService<TService>();
    }
}