using System.Text.Json.Serialization;

namespace GB.AccessManagement.Application.Presentations;

[Serializable]
public abstract record HalPresentation<TPresentation> where TPresentation : HalPresentation<TPresentation>
{
    private readonly Dictionary<string, HalLink> _links = new();

    [JsonPropertyName("_links")]
    public HalLinks? Links => _links.Count != 0 ? new HalLinks(_links) : null;

    public TPresentation AddLink(string name, string href, HttpMethod httpMethod)
    {
        _links.Add(name, new HalLink(href, httpMethod));

        return (TPresentation)this;
    }

    public TPresentation AddSelfLink(string href, HttpMethod httpMethod)
    {
        return AddLink("self", href, httpMethod);
    }
}