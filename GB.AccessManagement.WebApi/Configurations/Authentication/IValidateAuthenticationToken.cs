using System.Security.Claims;

namespace GB.AccessManagement.WebApi.Configurations.Authentication;

public interface IValidateAuthenticationToken
{
    (bool isValid, ClaimsPrincipal? Principal) Validate(string[] headerValues);
}