using System.Net.Http.Json;

namespace GB.AccessManagement.Tests.Endpoints.AddCompanyChild;

public sealed class AddCompanyChildScenario(HttpClient client, int apiVersion)
{
    public Task<HttpResponseMessage> Execute(Guid companyId, Guid childId)
    {
        var content = JsonContent.Create(new
        {
            companyId = childId
        });

        return client.PostAsync($"/v{apiVersion}/companies/{companyId}/children", content);
    }
}