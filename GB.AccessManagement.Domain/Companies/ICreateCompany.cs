using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Domain.Companies;

public interface ICreateCompany
{
    Task<CompanyAggregate> Create(CompanyName name, UserId ownerId, CompanyId? parentCompanyId);
}