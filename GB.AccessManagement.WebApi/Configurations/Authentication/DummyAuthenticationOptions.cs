using Microsoft.AspNetCore.Authentication;

namespace GB.AccessManagement.WebApi.Configurations.Authentication;

public sealed class DummyAuthenticationOptions : AuthenticationSchemeOptions
{
    public const string AuthenticationScheme = "Bearer";
}