using System.Diagnostics.Metrics;
using GB.AccessManagement.Domain.Services;
using GB.AccessManagement.Infrastructure.OpenFga;

namespace GB.AccessManagement.WebApi.Configurations.OpenTelemetry.Metrics;

public sealed class CompanyAccessMetrics : ICountCompanyAccess, ISingletonService
{
    private readonly Meter _meter;
    private readonly Counter<int> _createdAccessesCounter;
    private readonly Counter<int> _deletedAccessesCounter;
    private readonly ObservableGauge<int> _accessesGauge;
    private int _totalAccesses;

    public string Name => _meter.Name;

    public CompanyAccessMetrics()
    {
        _meter = new Meter("accesses");
        _createdAccessesCounter = _meter.CreateCounter<int>($"{Name}.user.created");
        _deletedAccessesCounter = _meter.CreateCounter<int>($"{Name}.user.removed");
        _accessesGauge = _meter.CreateObservableGauge(
            $"{Name}.user.total",
            () => new[] { new Measurement<int>(_totalAccesses) });
    }

    public void WhenCreated()
    {
        _createdAccessesCounter.Add(1);
        _totalAccesses++;
    }

    public void WhenDeleted()
    {
        _deletedAccessesCounter.Add(1);
        _totalAccesses--;
    }
}