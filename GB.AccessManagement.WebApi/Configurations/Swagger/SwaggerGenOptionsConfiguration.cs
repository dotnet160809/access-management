using Asp.Versioning.ApiExplorer;
using GB.AccessManagement.Domain.Services;
using GB.AccessManagement.WebApi.Configurations.Authentication;
using GB.AccessManagement.WebApi.Configurations.Swagger.SchemaFilters;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace GB.AccessManagement.WebApi.Configurations.Swagger;

public sealed class SwaggerGenOptionsConfiguration : IConfigureOptions<SwaggerGenOptions>, ITransientService
{
    private readonly IApiVersionDescriptionProvider _provider;

    public SwaggerGenOptionsConfiguration(IApiVersionDescriptionProvider provider)
    {
        _provider = provider;
    }

    public void Configure(SwaggerGenOptions options)
    {
        foreach (var description in _provider.ApiVersionDescriptions)
        {
            options.SwaggerDoc(description.GroupName, new OpenApiInfo
            {
                Title = "Access Management",
                Description = ".Net application using OpenFGA for access management",
                Version = description.ApiVersion.ToString(),
                Contact = new OpenApiContact
                {
                    Name = "GitLab",
                    Url = new Uri("https://gitlab.com/dotnet160809/access-management")
                },
                License = new OpenApiLicense
                {
                    Name = "Geoffroy BRAUN",
                    Url = new Uri("https://gitlab.com/geoffroybraun/")
                }
            });
            options.AddSecurityDefinition(DummyAuthenticationOptions.AuthenticationScheme,
                new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.Http,
                    Scheme = DummyAuthenticationOptions.AuthenticationScheme,
                    In = ParameterLocation.Header
                });
            options.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Id = DummyAuthenticationOptions.AuthenticationScheme,
                            Type = ReferenceType.SecurityScheme
                        }
                    },
                    Array.Empty<string>()
                }
            });
            options.EnableAnnotations();
            options.DocumentFilter<AdditionalPropertiesDocumentFilter>();
            options.SchemaFilter<HalLinksSchemaFilter>();
            options.SchemaFilter<CompanyHalPresentationSchemaFilter>();
            options.SchemaFilter<CompaniesHalPresentationSchemaFilter>();
            options.SchemaFilter<CompanyMemberHalPresentationSchemaFilter>();
            options.SchemaFilter<CompanyMembersHalPresentationSchemaFilter>();
        }
    }
}