using GB.HttpRequestMessagesStub.Internals;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace GB.HttpRequestMessagesStub;

/// <summary>
/// Extends an <see cref="IServiceCollection"/> implementation
/// </summary>
public static class ServiceCollectionExtension
{
    /// <summary>
    /// Add all required classes to stub http requests
    /// </summary>
    /// <remarks>
    /// Replace all <see cref="IHttpClientFactory"/> implementations
    /// </remarks>
    /// <param name="services">The extended <see cref="IServiceCollection"/> implementation</param>
    /// <returns>The extended <see cref="IServiceCollection"/> implementation</returns>
    public static IServiceCollection StubHttpRequests(this IServiceCollection services)
    {
        return services
            .RemoveAll<IHttpClientFactory>()
            .AddOptions<HttpRequestMessageDelegatingHandlerOptions>()
            .Services
            .AddOptions<HttpRequestMessageAssertionOptions>()
            .Services
            .AddTransient<HttpClientFactoryFixture>()
            .AddTransient<IHttpClientFactory>(GetRequiredService<HttpClientFactoryFixture>)
            .AddTransient<IStubHttpRequestMessages>(GetRequiredService<HttpClientFactoryFixture>);
    }

    private static TService GetRequiredService<TService>(IServiceProvider provider) where TService : notnull
    {
        return provider.GetRequiredService<TService>();
    }
}