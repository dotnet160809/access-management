using System.Text.Json;
using GB.AccessManagement.Tests.Extensions;
using GB.AccessManagement.Tests.Models;
using Microsoft.AspNetCore.Http.Extensions;
using NFluent;
using NFluent.Extensibility;

namespace GB.AccessManagement.Tests.Assertions;

public static class HalLinksAssertion
{
    public static void HasHalLinks(this ICheck<JsonElement> check, TestHalLinks links)
    {
        var expectedCount = links.Count;
        var logic = ExtensibilityHelper
            .BeginCheck(check)
            .FailWhen(element => !element.HasHalLinksProperty(), "Missing '_links' property.")
            .FailWhen(element => element.HalLinksCount() != expectedCount, $"'_links' property does not contain {expectedCount} element(s).");

        foreach (var (name, link) in links)
        {
            var linkHref = link.Href;
            var linkMethod = link.Method;

            _ = logic
                .FailWhen(element => !element.HasHalLinkProperty(name), $"Missing '{name}' link.")
                .FailWhen(element => !element.HasHalLinkHrefProperty(name), $"Missing 'href' property for link '{name}'.")
                .FailWhen(
                    element => element.GetHalLinkHrefProperty(name).Equals(linkHref, StringComparison.OrdinalIgnoreCase) == false,
                    $"'href' property of link '{name}' does not match expected '{linkHref}' value.")
                .FailWhen(element => !element.HasHalLinkMethodProperty(name), $"Missing 'method' property for link '{name}'.")
                .FailWhen(
                    element => element.GetHalLinkMethodProperty(name).Equals(linkMethod, StringComparison.OrdinalIgnoreCase) == false,
                    $"'href' property of link '{name}' does not match expected '{linkMethod}' value.");
        }

        logic
            .OnNegate("HAL links assertion failed, either '_links' property is missing or links comparison failed.")
            .EndCheck();

        _ = ExtensibilityHelper.BeginCheck(check);
    }

    public static void HasHalPagination(this ICheck<JsonElement> check,
        string href,
        string method,
        int page = 1,
        int length = 100)
    {
        var paginationParameters = new QueryBuilder
        {
            { nameof(page), page.ToString() },
            { nameof(length), length.ToString() }
        };
        check.HasHalLinks(new TestHalLinks
        {
            ["self"] = new TestHalLink(href, method),
            ["first"] = new TestHalLink($"{href}{paginationParameters}", method),
            ["last"] = new TestHalLink($"{href}{paginationParameters}", method)
        });
    }
}