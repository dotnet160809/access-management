namespace GB.AccessManagement.Infrastructure.MassTransit.PublishObservers;

public interface ILogPublishMessage
{
    void MessagePublished<TMessage>(TMessage message) where TMessage : class;

    void UnableToPublishMessage<TMessage>(TMessage message) where TMessage : class;
}