using System.ComponentModel.DataAnnotations.Schema;
using GB.AccessManagement.Domain.Aggregates.Memos;
using GB.AccessManagement.Domain.Companies;
using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Infrastructure.Database;

[Table(TableName)]
public sealed record CompanyDao : ICompanyMemo
{
    public const string TableName = "Companies";
    public const string IndexColumnName = "index";
    public const string IdColumnName = "id";
    public const string NameColumnName = "name";
    public const string OwnerIdColumnName = "owner_id";
    public const string ParentIdColumnName = "parent_company_id";
    
    [Column(IndexColumnName)]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Index { get; init; }
    
    [Column(IdColumnName)]
    public Guid Id { get; set; }

    CompanyId ICompanyMemo.Id
    {
        get => Id;
        set => Id = (Guid)value;
    }
    
    [Column(NameColumnName)]
    public string Name { get; set; } = string.Empty;

    CompanyName ICompanyMemo.Name
    {
        get => Name;
        set => Name = value;
    }
    
    [Column(OwnerIdColumnName)]
    public Guid OwnerId { get; set; }

    UserId ICompanyMemo.OwnerId
    {
        get => OwnerId;
        set => OwnerId = (Guid)value;
    }
    
    [Column(ParentIdColumnName)]
    public Guid? ParentCompanyId { get; set; }

    CompanyId? ICompanyMemo.ParentCompanyId
    {
        get => ParentCompanyId;
        set
        {
            if (value != null)
            {
                ParentCompanyId = (Guid?)value;
            }
        }
    }
    
    [NotMapped]
    public IList<Guid> Children { get; set; } = new List<Guid>();

    ICollection<CompanyId> ICompanyMemo.Children
    {
        get => Children.Select(child => (CompanyId)child).ToList();
        set => Children = value.Select(child => (Guid)child).ToList();
    }

    [NotMapped]
    public IList<Guid> Members { get; set; } = new List<Guid>();

    ICollection<UserId> ICompanyMemo.Members
    {
        get => Members.Select(member => (UserId)member).ToList();
        set => Members = value.Select(member => (Guid)member).ToList();
    }

    EMemoState IAggregateMemo.State { get; set; }
}