using GB.AccessManagement.Domain.Companies.Messages;
using GB.AccessManagement.Domain.Messages;
using GB.AccessManagement.Infrastructure.MassTransit;
using GB.AccessManagement.Infrastructure.MassTransit.ConsumeObservers;
using GB.AccessManagement.Infrastructure.MassTransit.Consumers;
using GB.AccessManagement.Infrastructure.MassTransit.PublishObservers;
using MassTransit;
using Microsoft.Extensions.Options;
using Headers = RabbitMQ.Client.Headers;

namespace GB.AccessManagement.WebApi.Configurations;

public static class MessageQueueConfiguration
{
    public static IServiceCollection ConfigureMessageQueue(this IServiceCollection services)
    {
        return services
            .AddOptions<MessageQueueOptions>()
            .BindConfiguration("MessageQueue")
            .ValidateDataAnnotations()
            .Services
            .AddMassTransit(massTransit =>
            {
                _ = massTransit.AddConsumer<CompanyCreatedMessageConsumer>();
                _ = massTransit.AddConsumer<CompanyMemberAddedMessageConsumer>();
                _ = massTransit.AddConsumer<CompanyMemberRemovedMessageConsumer>();
                _ = massTransit.AddConsumer<CompanyDeletedMessageConsumer>();
                _ = massTransit.AddPublishObserver<PublishMessageAuthenticator>();
                _ = massTransit.AddPublishObserver<PublishMessageLogger>();
                _ = massTransit.AddConsumeObserver<ConsumeMessageLogger>();

                massTransit.UsingRabbitMq((context, busConfiguration) =>
                {
                    busConfiguration.BindHostOptions(context);
                    busConfiguration.UseMessageRetry(Exponential(TimeSpan.FromSeconds(10), TimeSpan.FromMilliseconds(50)));

                    busConfiguration.MessageWithVersionedExchangeName<ICompanyCreatedMessage>(1);
                    busConfiguration.MessageWithVersionedExchangeName<ICompanyMemberAddedMessage>(1);
                    busConfiguration.MessageWithVersionedExchangeName<ICompanyMemberRemovedMessage>(1);
                    busConfiguration.MessageWithVersionedExchangeName<ICompanyDeletedMessage>(1);

                    busConfiguration.ReceiveEndpoint(QueueName(1), endpoint =>
                    {
                        endpoint.SetQuorumQueue();
                        endpoint.ConfigureDeadLetterEndpoint(1);
                        endpoint.SingleActiveConsumer = false;
                        endpoint.ConcurrentMessageLimit = 5;

                        endpoint.ConfigureConsumer<CompanyCreatedMessageConsumer>(context, ConfigureConsumer);
                        endpoint.ConfigureConsumer<CompanyMemberAddedMessageConsumer>(context, ConfigureConsumer);
                        endpoint.ConfigureConsumer<CompanyMemberRemovedMessageConsumer>(context, ConfigureConsumer);
                        endpoint.ConfigureConsumer<CompanyDeletedMessageConsumer>(context, ConfigureConsumer);
                    });
                });
            });
    }
    private static void BindHostOptions(this IRabbitMqBusFactoryConfigurator busConfiguration, IServiceProvider provider)
    {
        var options = provider.GetRequiredService<IOptionsMonitor<MessageQueueOptions>>().CurrentValue;

        busConfiguration.Host(
            options.Host,
            options.Port,
            options.VirtualHost,
            hostConfiguration =>
            {
                hostConfiguration.Username(options.Username);
                hostConfiguration.Password(options.Password);
            });
    }

    private static void MessageWithVersionedExchangeName<TMessage>(this IRabbitMqBusFactoryConfigurator busConfiguration, int version)
        where TMessage : class, IMessage
    {
        busConfiguration.Message<TMessage>(configurator =>
        {
            configurator.SetEntityNameFormatter(new ExchangeNameFormatter<TMessage>(version));
        });
    }

    private static void ConfigureDeadLetterEndpoint(this IRabbitMqReceiveEndpointConfigurator endpointConfiguration, int version)
    {
        endpointConfiguration.BindDeadLetterQueue(DeadLetterQueueName(version), null, e => e.SetQuorumQueue());
        endpointConfiguration.SetQueueArgument(Headers.XOverflow, "reject-publish");
        endpointConfiguration.SetQueueArgument("x-delivery-limit", 2);
        endpointConfiguration.SetQueueArgument("x-dead-letter-strategy", "at-least-once");
    }

    private static string QueueName(int version)
    {
        return $"access-management-v{version}";
    }

    private static string DeadLetterQueueName(int version)
    {
        return $"{QueueName(version)}-dlx";
    }

    private static void ConfigureConsumer<T>(IConsumerConfigurator<T> configure) where T : class
    {
        configure.UseMessageRetry(Exponential(TimeSpan.FromSeconds(60), TimeSpan.FromMilliseconds(50)));
    }

    private static Action<IRetryConfigurator> Exponential(TimeSpan maxInterval, TimeSpan intervalDelta)
    {
        return retry => retry.Exponential(10, TimeSpan.Zero, maxInterval, intervalDelta);
    }
}