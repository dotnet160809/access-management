using GB.AccessManagement.Domain.Messages;
using MassTransit;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;

namespace GB.AccessManagement.Infrastructure.MassTransit.Consumers;

public abstract class AbstractConsumer<TMessage>(IOptionsMonitor<MessageQueueOptions> options) : IConsumer<TMessage>
    where TMessage : class
{
    public abstract Task Consume(ConsumeContext<TMessage> context);

    protected bool ContextIsNotAuthenticated<T>(ConsumeContext<T> context) where T : class
    {
        return !context.Headers.TryGetHeader(HeaderNames.Authorization, out var header)
               || header is not string headerValue
               || context.Message is not IMessage message
               || !options.CurrentValue.Match(message.Id.ToString(), headerValue);
    }
}