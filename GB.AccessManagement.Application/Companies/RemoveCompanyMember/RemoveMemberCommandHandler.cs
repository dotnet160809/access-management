using GB.AccessManagement.Application.Commands;

namespace GB.AccessManagement.Application.Companies.RemoveCompanyMember;

public sealed class RemoveMemberCommandHandler : ICommandHandler<RemoveMemberCommand>
{
    private readonly ICompanyStore _store;

    public RemoveMemberCommandHandler(ICompanyStore store)
    {
        _store = store;
    }

    public async Task Handle(RemoveMemberCommand command, CancellationToken cancellationToken)
    {
        var aggregate = await _store.Load(command.CompanyId);
        aggregate.RemoveMember(command.MemberId);
        await _store.Save(aggregate, cancellationToken);
    }
}