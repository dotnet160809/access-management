using GB.AccessManagement.Domain.Companies.Providers;
using GB.AccessManagement.Domain.Services;
using GB.AccessManagement.Domain.ValueObjects;
using GB.AccessManagement.Infrastructure.Database.Extensions;
using Microsoft.EntityFrameworkCore;

namespace GB.AccessManagement.Infrastructure.Database.Providers;

public sealed class CompanyParentIdProvider(CompanyDbContext dbContext) : IProvideCompanyParentId, ITransientService
{
    public async Task<CompanyId?> FindParentForCompany(CompanyId companyId)
    {
        var parentCompanyId = await dbContext.Companies
            .AsNoTracking()
            .ForCompany(companyId)
            .Select(company => company.ParentCompanyId)
            .SingleOrDefaultAsync();

        return parentCompanyId;
    }
}