using GB.AccessManagement.Application.Presentations;
using GB.AccessManagement.Domain.Services;
using GB.AccessManagement.WebApi.HalLinks.CompanyHalLinks;

namespace GB.AccessManagement.WebApi.HalLinks.CompaniesHalLinks;

public sealed class CompaniesHalLinkDirector : ISelfTransientService
{
    private readonly CompaniesHalLinkBuilder _builder;
    private readonly CompanyHalLinkDirector _director;

    public CompaniesHalLinkDirector(CompaniesHalLinkBuilder builder, CompanyHalLinkDirector director)
    {
        _builder = builder;
        _director = director;
    }

    public async Task<CompaniesPresentation> MakeHalLinks(CompaniesPresentation presentation, Guid companyId, string userId, CancellationToken cancellationToken)
    {
        _ = _builder
            .BuildGetCompaniesLink(presentation, companyId)
            .BuildPaginationLinks(presentation, companyId, 1, 100);

        foreach (var company in presentation.Items)
        {
            _ = await _director.MakeHalLinks(company, userId, cancellationToken);
        }

        return presentation;
    }

    public async Task<CompaniesPresentation> MakeHalLinks(CompaniesPresentation presentation, string userId, CancellationToken cancellationToken)
    {
        _ = _builder
            .BuildGetCompaniesLink(presentation, userId)
            .BuildPaginationLinks(presentation, userId, 1, 100);

        foreach (var company in presentation.Items)
        {
            _ = await _director.MakeHalLinks(company, userId, cancellationToken);
        }

        return presentation;
    }
}