using System.Text.Json;
using GB.AccessManagement.WebApi.Controllers.Attributes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Options;

namespace GB.AccessManagement.WebApi.Extensions;

public static class ControllerBaseExtension
{
    public static IActionResult Hal(this ControllerBase controller, [ActionResultObjectValue] object? value)
    {
        var serializerOptions = controller
            .HttpContext
            .RequestServices
            .GetRequiredService<IOptionsMonitor<JsonOptions>>()
            .CurrentValue
            .JsonSerializerOptions;
        var content = JsonSerializer.Serialize(value, serializerOptions);

        return controller.Content(content, SupportedContentTypes.HalJson);
    }
}