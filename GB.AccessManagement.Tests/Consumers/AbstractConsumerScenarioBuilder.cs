using GB.AccessManagement.Domain.Messages;
using GB.AccessManagement.Infrastructure.MassTransit;
using GB.AccessManagement.Tests.Extensions;
using MassTransit;
using NSubstitute;

namespace GB.AccessManagement.Tests.Consumers;

public abstract class AbstractConsumerScenarioBuilder<TBuilder> : AbstractScenarioBuilder<TBuilder>
    where TBuilder : AbstractConsumerScenarioBuilder<TBuilder>
{
    protected TBuilder CreateMessageContext<TMessage>(TMessage message, out ConsumeContext<TMessage> context) where TMessage : class
    {
        context = CreateMessageContext(message);

        return (TBuilder)this;
    }

    protected TBuilder CreateForbiddenMessageContext<TMessage>(TMessage message, out ConsumeContext<TMessage> context)
        where TMessage : class, IMessage
    {
        var headerValue = Options<MessageQueueOptions>().Hash($"not-{message.Id.ToString()}");
        context = CreateMessageContext(message).WithAuthorizationHeader(headerValue);

        return (TBuilder)this;
    }

    protected TBuilder CreateAuthenticatedMessageContext<TMessage>(TMessage message, out ConsumeContext<TMessage> context)
        where TMessage : class, IMessage
    {
        var headerValue = Options<MessageQueueOptions>().Hash(message.Id.ToString());
        context = CreateMessageContext(message).WithAuthorizationHeader(headerValue);

        return (TBuilder)this;
    }

    private static ConsumeContext<TMessage> CreateMessageContext<TMessage>(TMessage message) where TMessage : class
    {
        var substitute = Substitute.For<ConsumeContext<TMessage>>();
        substitute.Message.Returns(message);

        return substitute;
    }
}