using Swashbuckle.AspNetCore.Annotations;

namespace GB.AccessManagement.WebApi.Controllers.Attributes;

public sealed class ReturnsCreatedAttribute() : SwaggerResponseAttribute(StatusCodes.Status201Created);

public sealed class ReturnsCreatedAttribute<TResponse>() : SwaggerResponseAttribute(StatusCodes.Status201Created, type: typeof(TResponse));