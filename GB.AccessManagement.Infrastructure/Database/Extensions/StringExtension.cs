using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Infrastructure.Database.Extensions;

internal static class StringExtension
{
    public static IEnumerable<string> ExcludeNonUsers(this IEnumerable<string> values)
    {
        return values.Where(value => !value.Contains(':'));
    }

    public static IEnumerable<UserId> AsUserIds(this IEnumerable<string> values)
    {
        return values.Select(value => (UserId)value);
    }

    public static IEnumerable<CompanyId> AsCompanyIds(this IEnumerable<string> values)
    {
        return values.Select(value => (CompanyId)value);
    }
}