using GB.AccessManagement.Domain.Services;
using GB.AccessManagement.Infrastructure.OpenFga;
using Microsoft.Extensions.Options;
using OpenFga.Sdk.Model;

namespace GB.AccessManagement.WebApi.Configurations.HostedServices;

public sealed class StoreInitializer : ISelfTransientService
{
    private readonly IHttpClientFactory _factory;
    private readonly IOptionsMonitor<OpenFgaOptions> _options;

    public StoreInitializer(
        IHttpClientFactory factory,
        IOptionsMonitor<OpenFgaOptions> options)
    {
        _factory = factory;
        _options = options;
    }

    public async Task<string> GetOrCreateStoreId()
    {
        var client = CreateClient();
        var httpResponse = await client.GetAsync("/stores");
        httpResponse.EnsureSuccessStatusCode();

        var storesResponse = await httpResponse.Content.ReadFromJsonAsync<ListStoresResponse>()
                             ?? new ListStoresResponse();

        if (storesResponse.Stores.Any(StoreNamedAfterOptions))
        {
            return storesResponse
                .Stores
                .Single(StoreNamedAfterOptions)
                .Id;
        }

        httpResponse = await client.PostAsync("/stores", JsonContent.Create(new CreateStoreRequest
        {
            Name = _options.CurrentValue.StoreName
        }));
        httpResponse.EnsureSuccessStatusCode();

        var createStoreResponse = await httpResponse.Content.ReadFromJsonAsync<CreateStoreResponse>()
                                  ?? new CreateStoreResponse();

        return createStoreResponse.Id!;
    }

    private HttpClient CreateClient()
    {
        var client = _factory.CreateClient();
        client.BaseAddress = _options.CurrentValue.BaseUri;

        return client;
    }

    private bool StoreNamedAfterOptions(Store store)
    {
        return !string.IsNullOrEmpty(store.Name)
               && store.Name.Equals(_options.CurrentValue.StoreName, StringComparison.OrdinalIgnoreCase);
    }
}