using GB.AccessManagement.Domain.Companies.Providers;
using GB.AccessManagement.Domain.Services;
using GB.AccessManagement.Domain.ValueObjects;
using GB.AccessManagement.Infrastructure.OpenFga.Visitors;
using Microsoft.Extensions.Options;
using OpenFga.Sdk.Model;

namespace GB.AccessManagement.Infrastructure.OpenFga.Clients;

public sealed class OpenFgaExpandClient : AbstractOpenFgaClient,
    IProvideCompanyMemberIds,
    IProvideCompanyOwnerId,
    IExpandUserIds,
    ITransientService
{
    private readonly IVisitExpandResponse _visitor;
    
    public OpenFgaExpandClient(IHttpClientFactory factory, IOptionsMonitor<OpenFgaOptions> options) : base(factory, options)
    {
        _visitor = new ExpandResponseVisitor(this);
    }

    public async Task<UserId[]> ListMembersForCompany(CompanyId companyId)
    {
        var userIds = await Expand(CompanyRelation.ObjectType, companyId.ToString(), "member", true);

        return userIds.Select(id => (UserId)id).ToArray();
    }

    public async Task<UserId> GetOwnerForCompany(CompanyId companyId)
    {
        var ownerIds = await Expand(CompanyRelation.ObjectType, companyId.ToString(), "owner", false);

        return ownerIds.Single();
    }

    public async Task<CompanyId?> FindParentForCompany(CompanyId companyId)
    {
        var parentIds = await Expand(CompanyRelation.ObjectType,
            companyId.ToString(),
            "parent",
            false);

        return parentIds
            .Select(parentId => (CompanyId)parentId.ToString())
            .FirstOrDefault();
    }

    public async Task<string[]> Expand(ObjectType objectType, ObjectId objectId, Relation relation, bool expand)
    {
        var response = await Post<ExpandResponse>(ExpandPath(), new ExpandRequest
        {
            TupleKey = new ExpandRequestTupleKey
            {
                Object = $"{objectType}:{objectId}",
                Relation = relation.ToString()
            }
        });

        return await _visitor.Visit(response, expand);
    }

    private string ExpandPath()
    {
        return $"/stores/{StoreId()}/expand";
    }
}