using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Domain.Companies.Exceptions;

public sealed class CannotDeleteCompanyException : DomainException
{
    private const string MessagePattern = "Company '{0}' has at least one child company.";

    public CannotDeleteCompanyException(CompanyId companyId) : base(string.Format(MessagePattern, companyId))
    {
    }
}