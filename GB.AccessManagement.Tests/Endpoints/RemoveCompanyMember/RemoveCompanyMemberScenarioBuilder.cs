using GB.AccessManagement.Tests.Extensions;
using GB.AccessManagement.Tests.Fixtures.OpenFga;
using Microsoft.Extensions.DependencyInjection;

namespace GB.AccessManagement.Tests.Endpoints.RemoveCompanyMember;

public sealed class RemoveCompanyMemberScenarioBuilder : AbstractEndpointScenarioBuilder<RemoveCompanyMemberScenarioBuilder>
{
    public RemoveCompanyMemberScenario Build(int apiVersion)
    {
        _ = GetRequiredService<OpenFgaFixture>()
            .CheckReturns(CompanyOwnerOrMembers)
            .ExpandReturns(CompanyOwnerOrMemberIds)
            .ListObjectsReturns([]);

        return new RemoveCompanyMemberScenario(CreateClient(), apiVersion);
    }

    protected override void ConfigureServices(IServiceCollection services)
    {
        _ = services
            .StubAuthentication()
            .AddCompanyFixture()
            .StubOpenFga();
    }
}