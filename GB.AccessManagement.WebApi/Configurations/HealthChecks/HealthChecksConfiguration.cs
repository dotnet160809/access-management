using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Npgsql;

namespace GB.AccessManagement.WebApi.Configurations.HealthChecks;

public static class HealthChecksConfiguration
{
    public static IServiceCollection ConfigureHealthChecks(this IServiceCollection services)
    {
        return services
            .AddOptions<JsonOptions>()
            .Configure(options => options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()))
            .Services
            .AddHealthChecks()
            .AddCheck("self", () => HealthCheckResult.Healthy())
            .AddNpgSql(ForPostgres, timeout: TimeSpan.FromSeconds(3), tags: ["db"], name: "postgres-db")
            .AddCheck<OpenFgaHealthCheck>("openfga")
            .Services;
    }

    private static string ForPostgres(IServiceProvider provider)
    {
        var connectionString = provider.GetRequiredService<IConfiguration>().GetValue<string>("ConnectionStrings:Postgres");
        var connectionStringBuilder = new NpgsqlConnectionStringBuilder(connectionString)
        {
            CommandTimeout = 1,
            Timeout = 1
        };

        return connectionStringBuilder.ToString();
    }
}