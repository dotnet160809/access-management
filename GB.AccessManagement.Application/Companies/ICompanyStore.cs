using GB.AccessManagement.Domain.Aggregates;
using GB.AccessManagement.Domain.Companies;
using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Application.Companies;

public interface ICompanyStore : IAggregateStore<CompanyAggregate, CompanyId, ICompanyMemo>;