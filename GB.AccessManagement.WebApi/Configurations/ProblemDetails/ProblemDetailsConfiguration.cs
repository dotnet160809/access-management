using GB.AccessManagement.Domain;
using Hellang.Middleware.ProblemDetails;
using ProblemDetailsFactory = Microsoft.AspNetCore.Mvc.Infrastructure.ProblemDetailsFactory;

namespace GB.AccessManagement.WebApi.Configurations.ProblemDetails;

public static class ProblemDetailsConfiguration
{
    public static IServiceCollection ConfigureExceptions(this IServiceCollection services)
    {
        return services.AddProblemDetails(options =>
        {
            options.Map<DomainException>(ToProblemDetails);
            options.Map<Exception>(ToProblemDetails);
        });
    }

    private static Microsoft.AspNetCore.Mvc.ProblemDetails ToProblemDetails(HttpContext context, DomainException exception)
    {
        PublishException(context, exception);

        return context.RequestServices
            .GetRequiredService<ProblemDetailsFactory>()
            .CreateProblemDetails(
                context,
                statusCode: StatusCodes.Status422UnprocessableEntity,
                title: exception.Title,
                type: exception.GetType().Name,
                detail: exception.Message);
    }

    private static Microsoft.AspNetCore.Mvc.ProblemDetails ToProblemDetails(HttpContext context, Exception exception)
    {
        PublishException(context, exception);

        return context.RequestServices
            .GetRequiredService<ProblemDetailsFactory>()
            .CreateProblemDetails(
                context,
                StatusCodes.Status500InternalServerError,
                title: exception.Message,
                detail: exception.StackTrace);
    }

    private static void PublishException(HttpContext context, Exception exception)
    {
        context.RequestServices
            .GetService<IPublishException>()?
            .Publish(exception);
    }
}