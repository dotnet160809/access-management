namespace GB.AccessManagement.Domain.ValueObjects;

public sealed record CompanyName
{
    private readonly string _value;

    private CompanyName(string value)
    {
        value = value.Trim();

        if (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value))
        {
            throw new ArgumentNullException(nameof(value));
        }

        _value = value;
    }

    public static implicit operator CompanyName(string value)
    {
        return new CompanyName(value);
    }

    public static implicit operator string(CompanyName companyName)
    {
        return companyName._value;
    }
}