using GB.AccessManagement.WebApi.Extensions;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace GB.AccessManagement.WebApi.Configurations.Swagger.SchemaFilters;

public sealed class HalLinksSchemaFilter : ISchemaFilter
{
    public void Apply(OpenApiSchema schema, SchemaFilterContext context)
    {
        if (schema.DoesNotMatchTitle(nameof(HalLinks)))
        {
            return;
        }

        schema.AdditionalProperties = null;
        schema.AdditionalPropertiesAllowed = false;
    }
}