using MediatR;

namespace GB.AccessManagement.Domain.Events;

public abstract record DomainEvent : INotification
{
    internal bool HasBeenCommitted { get; private set; }

    public void Commit()
    {
        HasBeenCommitted = true;
    }
}