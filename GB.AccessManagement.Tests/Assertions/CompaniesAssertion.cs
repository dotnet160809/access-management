using GB.AccessManagement.Tests.Models;
using NFluent;
using NFluent.Extensibility;

namespace GB.AccessManagement.Tests.Assertions;

public static class CompaniesAssertion
{
    public static void HasSingleCompanyMatching(this ICheck<IEnumerable<TestCompany>> check, string companyName)
    {
        ExtensibilityHelper.BeginCheck(check)
            .FailWhen(companies => companies.All(NotMatching(companyName)), $"No company found with name '{companyName}'.")
            .OnNegate("No company with expected name found.")
            .EndCheck();

        _ = ExtensibilityHelper.BuildCheckLink(check);
    }

    public static void HasSingleCompanyMatching(this ICheck<IEnumerable<TestCompany>> check,
        Predicate<TestCompany> predicate)
    {
        ExtensibilityHelper.BeginCheck(check)
            .FailWhen(companies => companies.All(NotMatching(predicate)), "No company found matching provided predicate.")
            .OnNegate("Company search failed, either the company has not been found or the predicate failed.")
            .EndCheck();

        _ = ExtensibilityHelper.BuildCheckLink(check);
    }

    public static void HasNoCompanyMatching(this ICheck<IEnumerable<TestCompany>> check, string companyName)
    {
        ExtensibilityHelper.BeginCheck(check)
            .FailWhen(companies => companies.Any(Matching(companyName)), $"Company named '{companyName}' has been found.")
            .OnNegate("The expected company has been found.")
            .EndCheck();

        _ = ExtensibilityHelper.BuildCheckLink(check);
    }

    private static Func<TestCompany, bool> NotMatching(string companyName)
    {
        return company => !Matching(companyName)(company);
    }

    private static Func<TestCompany, bool> NotMatching(Predicate<TestCompany> predicate)
    {
        return company => !Matching(predicate)(company);
    }

    private static Func<TestCompany, bool> Matching(string companyName)
    {
        return company => company.Name.Equals(companyName, StringComparison.Ordinal);
    }

    private static Func<TestCompany, bool> Matching(Predicate<TestCompany> predicate)
    {
        return company => predicate(company);
    }
}