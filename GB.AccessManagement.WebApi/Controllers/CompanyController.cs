using Asp.Versioning;
using GB.AccessManagement.Application.Commands;
using GB.AccessManagement.Application.Companies.AddCompanyChild;
using GB.AccessManagement.Application.Companies.AddCompanyMember;
using GB.AccessManagement.Application.Companies.CreateCompany;
using GB.AccessManagement.Application.Companies.DeleteCompany;
using GB.AccessManagement.Application.Companies.GetCompanyMembers;
using GB.AccessManagement.Application.Companies.RemoveCompanyMember;
using GB.AccessManagement.Application.Presentations;
using GB.AccessManagement.Application.Queries;
using GB.AccessManagement.Domain;
using GB.AccessManagement.Infrastructure.Database;
using GB.AccessManagement.Infrastructure.Database.Extensions;
using GB.AccessManagement.WebApi.Configurations.Authorization.Attributes;
using GB.AccessManagement.WebApi.Controllers.Attributes;
using GB.AccessManagement.WebApi.Controllers.Requests;
using GB.AccessManagement.WebApi.Controllers.Responses;
using GB.AccessManagement.WebApi.Extensions;
using GB.AccessManagement.WebApi.HalLinks.CompaniesHalLinks;
using GB.AccessManagement.WebApi.HalLinks.CompanyHalLinks;
using GB.AccessManagement.WebApi.HalLinks.CompanyMemberHalLinks;
using GB.AccessManagement.WebApi.HalLinks.CompanyMembersHalLinks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GB.AccessManagement.WebApi.Controllers;

[ApiController]
[Route("v{version:apiVersion}/companies")]
[ApiVersion("1"), Tags("Companies")]
[ReturnsUnauthorized, ReturnsForbidden, ReturnsInternalServerError]
public sealed class CompanyController : ControllerBase
{
    [HttpPost]
    [Authorize]
    [ConsumesJson]
    [ReturnsCreated<CompanyCreatedResponse>, ReturnsBadRequest, ReturnsUnprocessableEntity]
    public async Task<IActionResult> Create(
        [FromBody] CreateCompanyRequest request,
        [FromServices] ICommandDispatcher dispatcher,
        CancellationToken cancellationToken)
    {
        CreateCompanyCommand command = new(request.Name, User.Id(), request.ParentCompanyId);
        var companyId = await dispatcher.Dispatch(command, cancellationToken);
        var response = new CompanyCreatedResponse((Guid)companyId, request.Name, request.ParentCompanyId);

        return Created($"v1/companies/{companyId}", response);
    }

    [HttpGet("{companyId:guid}")]
    [CompanyMemberAuthorize("companyId")]
    [ReturnsOk(typeof(CompanyPresentation), SupportedContentTypes.HalJson)]
    [ReturnsBadRequest, ReturnsNotFound, ReturnsUnprocessableEntity]
    public async Task<IActionResult> Get(
        [FromRoute] Guid companyId,
        [FromServices] CompanyDbContext dispatcher,
        [FromServices] CompanyHalLinkDirector director,
        CancellationToken cancellationToken)
    {
        var company = await dispatcher.Companies
            .AsNoTracking()
            .ForCompany(companyId)
            .AsPresentations()
            .SingleOrDefaultAsync(cancellationToken);

        return company is not null
            ? this.Hal(await director.MakeHalLinks(company, User.Id().ToString(), cancellationToken))
            : NotFound();
    }

    [HttpGet("{companyId:guid}/owner")]
    [CompanyMemberAuthorize("companyId")]
    [ReturnsOk(typeof(CompanyMemberPresentation), SupportedContentTypes.HalJson)]
    [ReturnsBadRequest, ReturnsNotFound, ReturnsUnprocessableEntity]
    public async Task<IActionResult> Owner(
        [FromRoute] Guid companyId,
        [FromServices] CompanyDbContext dbContext,
        [FromServices] CompanyMemberHalLinkDirector director,
        CancellationToken cancellationToken)
    {
        var owner = await dbContext.Companies
            .AsNoTracking()
            .ForCompany(companyId)
            .Select(company => new CompanyMemberPresentation(company.OwnerId))
            .SingleAsync(cancellationToken);

        return this.Hal(await director.MakeHalLinks(owner, cancellationToken));
    }

    [HttpPost("{companyId:guid}/members")]
    [CompanyOwnerAuthorize("companyId")]
    [ConsumesJson]
    [ReturnsCreated]
    [ReturnsBadRequest, ReturnsNotFound, ReturnsUnprocessableEntity]
    public async Task<IActionResult> AddMember(
        [FromRoute] Guid companyId,
        [FromBody] AddMemberRequest request,
        [FromServices] ICommandDispatcher dispatcher,
        CancellationToken cancellationToken)
    {
        AddMemberCommand command = new(companyId, request.MemberId);
        await dispatcher.Dispatch(command, cancellationToken);

        return Created(Request.Path, null);
    }

    [HttpGet("{companyId:guid}/members")]
    [CompanyMemberAuthorize("companyId")]
    [ReturnsOk(typeof(CompanyMembersPresentation), SupportedContentTypes.HalJson)]
    [ReturnsBadRequest, ReturnsNotFound, ReturnsUnprocessableEntity]
    public async Task<IActionResult> Members(
        [FromRoute] Guid companyId,
        [FromServices] IQueryDispatcher dispatcher,
        [FromServices] CompanyMembersHalLinkDirector director,
        CancellationToken cancellationToken)
    {
        CompanyMembersQuery query = new(companyId);
        var members = await dispatcher.Dispatch(query, cancellationToken);

        return this.Hal(await director.MakeHalLinks(members, companyId, cancellationToken));
    }

    [HttpDelete("{companyId:guid}/members/{memberId:guid}")]
    [CompanyOwnerAuthorize("companyId")]
    [ReturnsNoContent]
    [ReturnsBadRequest, ReturnsNotFound, ReturnsUnprocessableEntity]
    public async Task<IActionResult> RemoveMember(
        [FromRoute] Guid companyId,
        [FromRoute] Guid memberId,
        [FromServices] ICommandDispatcher dispatcher,
        CancellationToken cancellationToken)
    {
        RemoveMemberCommand command = new(companyId, memberId);
        await dispatcher.Dispatch(command, cancellationToken);

        return NoContent();
    }

    [HttpGet("{companyId:guid}/parent")]
    [CompanyMemberAuthorize("companyId")]
    [ReturnsOk(typeof(CompanyPresentation), SupportedContentTypes.HalJson)]
    [ReturnsBadRequest, ReturnsNotFound, ReturnsUnprocessableEntity]
    public async Task<IActionResult> Parent(
        [FromRoute] Guid companyId,
        [FromServices] CompanyDbContext dbContext,
        [FromServices] CompanyHalLinkDirector director,
        CancellationToken cancellationToken)
    {
        var parentCompany = await dbContext.Companies
            .AsNoTracking()
            .Where(parent => parent.Id == dbContext.Companies
                .AsNoTracking()
                .Single(company => company.Id == companyId)
                .ParentCompanyId)
            .AsPresentations()
            .SingleOrDefaultAsync(cancellationToken);

        return parentCompany is not null
            ? this.Hal(await director.MakeHalLinks(parentCompany, User.Id().ToString(), cancellationToken))
            : NotFound();
    }

    [HttpGet("{companyId:guid}/children")]
    [CompanyMemberAuthorize("companyId")]
    [ReturnsOk(typeof(CompaniesPresentation), SupportedContentTypes.HalJson)]
    [ReturnsBadRequest, ReturnsNotFound]
    public async Task<IActionResult> Children(
        [FromRoute] Guid companyId,
        [FromServices] CompanyDbContext dispatcher,
        [FromServices] CompaniesHalLinkDirector director,
        CancellationToken cancellationToken)
    {
        var companyChildren = await dispatcher.Companies
            .AsNoTracking()
            .Where(company => company.ParentCompanyId == companyId)
            .AsPresentations()
            .ToArrayAsync(cancellationToken);
        var presentation = new CompaniesPresentation(companyChildren, companyChildren.Length);

        return this.Hal(await director.MakeHalLinks(presentation, companyId, User.Id().ToString(), cancellationToken));
    }

    [HttpPost("{companyId:guid}/children")]
    [CompanyOwnerAuthorize("companyId")]
    public async Task<IActionResult> AddChild(
        [FromRoute] Guid companyId,
        [FromBody] AddCompanyChildRequest request,
        [FromServices] IEvaluateCompanyAccesses accesses,
        [FromServices] ICommandDispatcher dispatcher,
        CancellationToken cancellationToken)
    {
        if (!await accesses.ForOwner(User.Id(), request.CompanyId))
        {
            return Forbid();
        }
        
        await dispatcher.Dispatch(new AddCompanyChildCommand(companyId, request.CompanyId), cancellationToken);

        return Ok();
    }

    [HttpDelete("{companyId:guid}")]
    [CompanyOwnerAuthorize("companyId")]
    [ReturnsNoContent]
    [ReturnsBadRequest, ReturnsNotFound, ReturnsUnprocessableEntity]
    public async Task<IActionResult> Delete(
        [FromRoute] Guid companyId,
        [FromServices] ICommandDispatcher dispatcher,
        CancellationToken cancellationToken)
    {
        var command = new DeleteCompanyCommand(companyId);
        await dispatcher.Dispatch(command, cancellationToken);

        return NoContent();
    }
}