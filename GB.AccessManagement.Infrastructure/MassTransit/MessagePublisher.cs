using GB.AccessManagement.Domain.Messages;
using GB.AccessManagement.Domain.Services;
using MassTransit;

namespace GB.AccessManagement.Infrastructure.MassTransit;

public sealed class MessagePublisher : IPublishMessage, ITransientService
{
    private readonly IPublishEndpoint _endpoint;

    public MessagePublisher(IPublishEndpoint endpoint)
    {
        _endpoint = endpoint;
    }

    public Task Publish<TMessage>(TMessage message, CancellationToken cancellationToken) where TMessage : class, IMessage
    {
        if (!typeof(TMessage).IsInterface)
        {
            throw new ArgumentException($"Type '{typeof(TMessage).Name}' is not an interface.");
        }

        return _endpoint.Publish(message, typeof(TMessage), cancellationToken);
    }
}