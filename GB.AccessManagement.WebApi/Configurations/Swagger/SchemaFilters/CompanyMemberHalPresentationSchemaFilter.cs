using GB.AccessManagement.Application.Presentations;
using GB.AccessManagement.WebApi.Extensions;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace GB.AccessManagement.WebApi.Configurations.Swagger.SchemaFilters;

public sealed class CompanyMemberHalPresentationSchemaFilter : ISchemaFilter
{
    public void Apply(OpenApiSchema schema, SchemaFilterContext context)
    {
        if (schema.DoesNotMatchTitle(nameof(CompanyMemberPresentation)))
        {
            return;
        }

        _ = schema.AddLink("get_companies", context);
    }
}