using System.ComponentModel.DataAnnotations;

namespace GB.AccessManagement.WebApi.Controllers.Requests;

public sealed class AddCompanyChildRequest
{
    [Required]
    public Guid CompanyId { get; init; }
}