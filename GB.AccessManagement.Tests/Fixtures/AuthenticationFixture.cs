using System.Security.Claims;
using GB.AccessManagement.Tests.Models;
using GB.AccessManagement.WebApi.Configurations.Authentication;

namespace GB.AccessManagement.Tests.Fixtures;

public sealed class AuthenticationFixture : IValidateAuthenticationToken
{
    private readonly Dictionary<Guid, ICollection<Claim>> _stubbedUsers = new();

    public void StubUser(TestUser user)
    {
        _stubbedUsers[user.Id] = new List<Claim>
        {
            new(ClaimTypes.NameIdentifier, user.Id.ToString()),
            new(ClaimTypes.Email, user.Email)
        };
    }

    public (bool isValid, ClaimsPrincipal? Principal) Validate(string[] headerValues)
    {
        return Guid.TryParse(headerValues.Last(), out var userId)
               && _stubbedUsers.TryGetValue(userId, out var stubbedUserClaims)
            ? Success(stubbedUserClaims)
            : Failure();
    }

    private static (bool, ClaimsPrincipal) Success(IEnumerable<Claim> claims)
    {
        var principal = new ClaimsPrincipal(new ClaimsIdentity(claims, DummyAuthenticationOptions.AuthenticationScheme));

        return (true, principal);
    }

    private static (bool, ClaimsPrincipal?) Failure()
    {
        return (false, null);
    }
}