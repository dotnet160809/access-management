namespace GB.AccessManagement.Domain.ValueObjects;

public sealed record ObjectType
{
    private readonly string _value;

    private ObjectType(string value)
    {
        value = value.Trim();

        if (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value))
        {
            throw new ArgumentNullException(nameof(value));
        }

        _value = value;
    }

    public override string ToString()
    {
        return _value;
    }

    public static implicit operator ObjectType(string value)
    {
        return new ObjectType(value);
    }

    public static implicit operator string(ObjectType objectType)
    {
        return objectType._value;
    }
}