using GB.AccessManagement.Domain.Services;
using GB.AccessManagement.WebApi.Configurations.Authentication;
using Microsoft.AspNetCore.Authorization;

namespace GB.AccessManagement.WebApi.Configurations.Authorization;

public sealed class CompanyAccessAuthorizationPolicyProvider : IAuthorizationPolicyProvider, ISingletonService
{
    public Task<AuthorizationPolicy?> GetPolicyAsync(string policyName)
    {
        if (!policyName.StartsWith(CompanyAccessAuthorizeAttribute.PolicyPrefix))
        {
            return GetFallbackPolicyAsync();
        }

        var policyBuilder = new AuthorizationPolicyBuilder();
        policyBuilder.AddRequirements(BuildRequirement(policyName));

        return Task.FromResult<AuthorizationPolicy?>(policyBuilder.Build());
    }

    public Task<AuthorizationPolicy> GetDefaultPolicyAsync()
    {
        var authorizationPolicy = new AuthorizationPolicyBuilder(DummyAuthenticationOptions.AuthenticationScheme)
            .RequireAuthenticatedUser()
            .Build();
        
        return Task.FromResult(authorizationPolicy);
    }

    public Task<AuthorizationPolicy?> GetFallbackPolicyAsync()
    {
        return Task.FromResult<AuthorizationPolicy?>(default);
    }

    private static CompanyAccessAuthorizationRequirement BuildRequirement(string policyName)
    {
        var policyValues = policyName.Split(':');
        var routeParameterName = policyValues[1];
        var relation = policyValues[2];

        return new CompanyAccessAuthorizationRequirement(routeParameterName, relation);
    }
}