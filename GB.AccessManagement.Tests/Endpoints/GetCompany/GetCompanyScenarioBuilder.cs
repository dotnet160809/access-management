using GB.AccessManagement.Tests.Extensions;
using GB.AccessManagement.Tests.Fixtures;
using Microsoft.Extensions.DependencyInjection;

namespace GB.AccessManagement.Tests.Endpoints.GetCompany;

public sealed class GetCompanyScenarioBuilder : AbstractEndpointScenarioBuilder<GetCompanyScenarioBuilder>
{
    public GetCompanyScenarioBuilder RemoveCompanyMember(Guid companyId, Guid userId)
    {
        GetRequiredService<CompanyFixture>().RemoveCompanyMember(companyId, userId);

        return this;
    }

    public GetCompanyScenario Build(int apiVersion)
    {
        _ = OpenFga()
            .CheckReturns(CompanyOwnerOrMembers)
            .ExpandReturns([])
            .ListObjectsReturns([]);

        return new GetCompanyScenario(CreateClient(), apiVersion);
    }

    protected override void ConfigureServices(IServiceCollection services)
    {
        _ = services
            .StubAuthentication()
            .AddCompanyFixture()
            .StubOpenFga();
    }
}