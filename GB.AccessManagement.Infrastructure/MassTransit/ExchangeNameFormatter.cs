using GB.AccessManagement.Domain.Messages;
using MassTransit;

namespace GB.AccessManagement.Infrastructure.MassTransit;

public sealed class ExchangeNameFormatter<TMessage> : IMessageEntityNameFormatter<TMessage>
    where TMessage : class, IMessage
{
    private readonly int _version;

    public ExchangeNameFormatter(int version)
    {
        _version = version;
    }

    public string FormatEntityName()
    {
        return $"AccessManagement:v{_version}:{typeof(TMessage).Name}";
    }
}