using GB.AccessManagement.Domain.Companies.Providers;
using GB.AccessManagement.Domain.Services;
using GB.AccessManagement.Domain.ValueObjects;
using Microsoft.EntityFrameworkCore;

namespace GB.AccessManagement.Infrastructure.Database.Providers;

public sealed class CompanyChildrenIdsProvider(CompanyDbContext dbContext) : IProvideCompanyChildrenIds, ITransientService
{
    public async Task<CompanyId[]> ForCompany(CompanyId companyId)
    {
        var childrenIds = await dbContext.Companies
            .AsNoTracking()
            .Where(company => company.ParentCompanyId == (Guid)companyId)
            .Select(company => company.Id)
            .ToArrayAsync();

        return childrenIds.Select(id => (CompanyId)id).ToArray();
    }
}