using GB.AccessManagement.Application.Presentations;
using GB.AccessManagement.WebApi.Extensions;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace GB.AccessManagement.WebApi.Configurations.Swagger.SchemaFilters;

public sealed class CompanyMembersHalPresentationSchemaFilter : ISchemaFilter
{
    public void Apply(OpenApiSchema schema, SchemaFilterContext context)
    {
        if (schema.DoesNotMatchTitle(nameof(CompanyMembersPresentation)))
        {
            return;
        }

        _ = schema
            .AddSelfLink(context)
            .AddLink("first", context)
            .AddLink("previous", context)
            .AddLink("next", context)
            .AddLink("last", context);
    }
}