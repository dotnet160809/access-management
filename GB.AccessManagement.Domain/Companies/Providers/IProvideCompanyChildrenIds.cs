using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Domain.Companies.Providers;

public interface IProvideCompanyChildrenIds
{
    Task<CompanyId[]> ForCompany(CompanyId companyId);
}