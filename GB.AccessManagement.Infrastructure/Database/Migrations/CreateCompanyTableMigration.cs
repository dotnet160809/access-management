using FluentMigrator;

namespace GB.AccessManagement.Infrastructure.Database.Migrations;

[TimestampedMigration(2023, 12, 13, 12, 0)]
public class CreateCompanyTableMigration : ForwardOnlyMigration
{
    public override void Up()
    {
        Create
            .Table(CompanyDao.TableName)
            .WithColumn(CompanyDao.IndexColumnName).AsInt64().Identity().NotNullable()
            .WithColumn(CompanyDao.IdColumnName).AsGuid().NotNullable()
            .WithColumn(CompanyDao.NameColumnName).AsString(255).NotNullable()
            .WithColumn(CompanyDao.OwnerIdColumnName).AsGuid().NotNullable()
            .WithColumn(CompanyDao.ParentIdColumnName).AsGuid().Nullable();
    }
}