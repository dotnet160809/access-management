using System.Net;
using GB.AccessManagement.Domain.Companies.Messages;
using GB.AccessManagement.Infrastructure.Database;
using GB.AccessManagement.Infrastructure.MassTransit;
using GB.AccessManagement.Tests.Assertions;
using MassTransit.Testing;
using NFluent;
using NSubstitute;
using Xunit.Abstractions;

namespace GB.AccessManagement.Tests.Endpoints.DeleteCompany;

public sealed class DeleteCompanyEndpointShould : AbstractFeatureShould<DeleteCompanyScenarioBuilder>
{
    public DeleteCompanyEndpointShould(ITestOutputHelper outputHelper, DeleteCompanyScenarioBuilder builder)
        : base(outputHelper, builder)
    {
    }

    [Theory]
    [InlineData(1)]
    public async Task ReturnUnauthorizedWhenUserIsNotAuthenticated(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var company)
            .Build(apiVersion);

        // Act
        var response = await scenario.Execute(company.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.Unauthorized);
    }

    [Theory]
    [InlineData(1, false)]
    [InlineData(1, true)]
    public async Task ReturnForbiddenWhenUserIsNotCompanyOwner(int apiVersion, bool isUserCompanyMember)
    {
        // Arrange
        _ = ScenarioBuilder
            .CreateCompany(out var company)
            .CreateUser(out var user);

        if (isUserCompanyMember)
        {
            _ = ScenarioBuilder.AddCompanyMember(company.Id, user.Id);
        }
        
        var scenario = ScenarioBuilder
            .AuthenticateUser(user)
            .Build(apiVersion);

        // Act
        var response = await scenario.Execute(company.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.Forbidden);
    }

    [Theory]
    [InlineData(1)]
    public async Task ReturnUnprocessableEntityWhenCompanyHasChildren(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var company)
            .CreateCompany(company.Id, out _)
            .CreateUser(out var user)
            .AddCompanyOwner(company.Id, user.Id)
            .AuthenticateUser(user)
            .Build(apiVersion);
        
        // Act
        var response = await scenario.Execute(company.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.UnprocessableEntity);
    }

    [Theory]
    [InlineData(1)]
    public async Task DeleteExpectedCompany(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var company)
            .CreateUser(out var user)
            .AddCompanyOwner(company.Id, user.Id)
            .AuthenticateUser(user)
            .Build(apiVersion);
        
        // Act
        var response = await scenario.Execute(company.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.NoContent);
        Check.That(Companies()).HasNoCompanyMatching(company.Name);
    }

    [Theory]
    [InlineData(1)]
    public async Task PublishAuthenticatedMessage(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var company)
            .CreateUser(out var user)
            .AddCompanyOwner(company.Id, user.Id)
            .AuthenticateUser(user)
            .Build(apiVersion);
        
        // Act
        _ = await scenario.Execute(company.Id);

        // Assert
        var options = Options<MessageQueueOptions>();
        Check.That(PublishedMessages()).HasAuthenticatedMessage(ForCompany(company.Id), options);
    }

    [Theory]
    [InlineData(1)]
    public async Task IncreaseMetric(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var company)
            .CreateUser(out var user)
            .AddCompanyOwner(company.Id, user.Id)
            .AuthenticateUser(user)
            .Build(apiVersion);
        
        // Act
        _ = await scenario.Execute(company.Id);
        
        // Assert
        Metrics<ICountCompanies>()
            .Received()
            .WhenDeleted();
    }

    private static Predicate<IPublishedMessage<ICompanyDeletedMessage>> ForCompany(Guid companyId)
    {
        return message => message.MessageObject is ICompanyDeletedMessage messageObject
                          && messageObject.CompanyId == companyId;
    }
}