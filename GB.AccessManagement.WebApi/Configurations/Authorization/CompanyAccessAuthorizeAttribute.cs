using Microsoft.AspNetCore.Authorization;

namespace GB.AccessManagement.WebApi.Configurations.Authorization;

public abstract class CompanyAccessAuthorizeAttribute : AuthorizeAttribute
{
    public const string PolicyPrefix = "Access";
    
    protected CompanyAccessAuthorizeAttribute(string routeParameterName, string relation)
    {
        Policy = $"{PolicyPrefix}:{routeParameterName}:{relation}";
    }
}