namespace GB.AccessManagement.Tests.Endpoints.GetCompany;

public sealed class GetCompanyScenario(HttpClient client, int apiVersion)
{
    public Task<HttpResponseMessage> Execute(Guid companyId)
    {
        return client.GetAsync($"/v{apiVersion}/companies/{companyId}");
    }
}