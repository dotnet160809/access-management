namespace GB.AccessManagement.Application.Commands;

public interface ICommandDispatcher
{
    Task Dispatch(ICommand command, CancellationToken cancellationToken);

    Task<TResponse> Dispatch<TResponse>(ICommand<TResponse> command, CancellationToken cancellationToken);
}