using Swashbuckle.AspNetCore.Annotations;

namespace GB.AccessManagement.Application.Presentations;

[Serializable]
[SwaggerSchema(Title = nameof(CompaniesPresentation))]
public sealed record CompaniesPresentation : PaginatedHalPresentation<CompaniesPresentation, CompanyPresentation>
{
    public CompaniesPresentation(CompanyPresentation[] items, int total) : base(items, total)
    {
    }
}