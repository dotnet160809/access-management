using System.Text.Json;
using GB.AccessManagement.Domain.Services;
using GB.AccessManagement.Infrastructure.OpenFga;
using Microsoft.Extensions.Options;
using OpenFga.Sdk.Model;

namespace GB.AccessManagement.WebApi.Configurations.HostedServices;

public sealed class AuthorizationModelInitializer : ISelfTransientService
{
    private readonly IOptionsMonitor<OpenFgaOptions> _options;
    private readonly IHttpClientFactory _factory;

    public AuthorizationModelInitializer(
        IOptionsMonitor<OpenFgaOptions> options,
        IHttpClientFactory factory)
    {
        _options = options;
        _factory = factory;
    }

    public async Task<bool> HasStoreAnyAuthorizationModel()
    {
        var client = CreateClient();

        var httpResponse = await client.GetAsync($"/stores/{_options.CurrentValue.StoreId}/authorization-models");
        httpResponse.EnsureSuccessStatusCode();

        var response = await httpResponse.Content.ReadFromJsonAsync<ReadAuthorizationModelsResponse>()
                       ?? new ReadAuthorizationModelsResponse();

        return response.AuthorizationModels.Count != 0;
    }

    public async Task InitializeAuthorizationModel()
    {
        var authorizationModel = await GetAuthorizationModel();
        
        var client = CreateClient();
        var request = new WriteAuthorizationModelRequest
        {
            AdditionalProperties = authorizationModel.AdditionalProperties,
            SchemaVersion = authorizationModel.SchemaVersion,
            TypeDefinitions = authorizationModel.TypeDefinitions
        };
        
        var httpResponse = await client.PostAsync($"/stores/{_options.CurrentValue.StoreId}/authorization-models", JsonContent.Create(request));
        httpResponse.EnsureSuccessStatusCode();
    }

    private HttpClient CreateClient()
    {
        var client = _factory.CreateClient();
        client.BaseAddress = _options.CurrentValue.BaseUri;

        return client;
    }

    private async Task<AuthorizationModel> GetAuthorizationModel()
    {
        var authorizationModelFilePath = Path.Combine(
            AppDomain.CurrentDomain.BaseDirectory,
            _options.CurrentValue.AuthorizationModel.FolderPath,
            _options.CurrentValue.AuthorizationModel.FileName);
        var serializedAuthorizationModel = await File.ReadAllTextAsync(authorizationModelFilePath);

        return JsonSerializer.Deserialize<AuthorizationModel>(serializedAuthorizationModel)
               ?? new AuthorizationModel();
    }
}