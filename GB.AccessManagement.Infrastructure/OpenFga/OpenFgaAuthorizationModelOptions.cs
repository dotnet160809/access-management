using System.ComponentModel.DataAnnotations;

namespace GB.AccessManagement.Infrastructure.OpenFga;

public sealed record OpenFgaAuthorizationModelOptions
{
    [Required]
    public string FolderPath { get; init; } = string.Empty;

    [Required]
    public string FileName { get; init; } = string.Empty;
    
    public bool ForceUpdate { get; init; }
}