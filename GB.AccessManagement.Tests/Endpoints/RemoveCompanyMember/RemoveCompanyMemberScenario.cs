namespace GB.AccessManagement.Tests.Endpoints.RemoveCompanyMember;

public sealed class RemoveCompanyMemberScenario(HttpClient client, int apiVersion)
{
    public Task<HttpResponseMessage> Execute(Guid companyId, Guid memberId)
    {
        return client.DeleteAsync($"/v{apiVersion}/companies/{companyId}/members/{memberId}");
    }
}