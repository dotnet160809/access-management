using System.Collections.Concurrent;

namespace GB.HttpRequestMessagesStub.Internals;

internal sealed class HttpRequestMessageDelegatingHandlerOptions : ConcurrentDictionary<HttpRequestMessageKey, HttpResponseMessageDelegate>;