using GB.AccessManagement.Domain.Events;
using MediatR;

namespace GB.AccessManagement.Application;

public interface IDomainEventHandler<in TEvent> : INotificationHandler<TEvent> where TEvent : DomainEvent;