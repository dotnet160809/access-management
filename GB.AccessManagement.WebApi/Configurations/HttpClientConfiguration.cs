using System.Net;
using GB.AccessManagement.Infrastructure.OpenFga;
using Microsoft.Extensions.Options;
using Polly;
using Polly.Extensions.Http;
using Polly.Timeout;

namespace GB.AccessManagement.WebApi.Configurations;

public static class HttpClientConfiguration
{
    public static IServiceCollection ConfigureHttpClients(this IServiceCollection services)
    {
        return services
            .AddHttpContextAccessor()
            .AddOptions<OpenFgaOptions>()
            .BindConfiguration("OpenFga")
            .ValidateDataAnnotations()
            .Services
            .AddHttpClient(OpenFgaOptions.HttpClientName)
            .ConfigureHttpClient((provider, client) =>
            {
                var options = provider.GetRequiredService<IOptionsMonitor<OpenFgaOptions>>().CurrentValue;
                client.BaseAddress = options.BaseUri;
            })
            .ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler { AutomaticDecompression = DecompressionMethods.Deflate })
            .AddPolicyHandler(ConfigurePolicyHandlers)
            .Services;
    }

    private static IAsyncPolicy<HttpResponseMessage> ConfigurePolicyHandlers(HttpRequestMessage _)
    {
        return HttpPolicyExtensions
            .HandleTransientHttpError()
            .Or<TimeoutRejectedException>()
            .WaitAndRetryAsync(
                3,
                retryAttempt => TimeSpan.FromMilliseconds(Math.Pow(2, retryAttempt)))
            .WrapAsync(Policy.TimeoutAsync(TimeSpan.FromMilliseconds(10000)));
    }
}