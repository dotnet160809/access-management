using ConfigurationSubstitution;
using GB.AccessManagement.WebApi.Configurations;
using GB.AccessManagement.WebApi.Configurations.Authentication;
using GB.AccessManagement.WebApi.Configurations.Authorization;
using GB.AccessManagement.WebApi.Configurations.Database;
using GB.AccessManagement.WebApi.Configurations.HealthChecks;
using GB.AccessManagement.WebApi.Configurations.OpenTelemetry;
using GB.AccessManagement.WebApi.Configurations.ProblemDetails;
using GB.AccessManagement.WebApi.Configurations.Swagger;
using GB.AccessManagement.WebApi.Extensions;
using Hellang.Middleware.ProblemDetails;

namespace GB.AccessManagement.WebApi;

public sealed class Startup
{
    private readonly IHostEnvironment _environment;
    private readonly IConfiguration _configuration;

    public Startup(IHostEnvironment environment, IConfiguration configuration)
    {
        _environment = environment;
        _configuration = configuration;
    }

    public void ConfigureHost(IHostBuilder builder)
    {
        _ = builder
            .ConfigureLogging(logging =>
            {
                _ = logging.ClearProviders();

                if (_environment.IsDevelopment())
                {
                    _ = logging.AddSimpleConsole();

                    return;
                }

                _ = logging.AddJsonConsole();
            })
            .ConfigureHostConfiguration(host => host.EnableSubstitutions());
    }

    public void ConfigureServices(IServiceCollection services)
    {
        _ = services
            .ConfigureAuthentication()
            .ConfigureAuthorizations()
            .ConfigureControllers()
            .ConfigureDatabase(_configuration)
            .ConfigureDependencyInjection()
            .ConfigureExceptions()
            .ConfigureHealthChecks()
            .ConfigureHttpClients()
            .ConfigureMessageQueue()
            .ConfigureSwagger()
            .ConfigureTracing()
            .ConfigureVersioning();
    }

    public void Configure(WebApplication app)
    {
        _ = app
            .UseProblemDetails()
            .UseOpenTelemetryPrometheusScrapingEndpoint()
            .UseHealthCheckEndpoints()
            .UseSwagger()
            .UseSwaggerUI()
            .UseRouting()
            .UseAuthentication()
            .UseAuthorization()
            .UseEndpoints(endpoints => endpoints.MapControllers());
    }
}