using GB.AccessManagement.Application.Presentations;
using GB.AccessManagement.WebApi.Extensions;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace GB.AccessManagement.WebApi.Configurations.Swagger.SchemaFilters;

public sealed class CompanyHalPresentationSchemaFilter : ISchemaFilter
{
    public void Apply(OpenApiSchema schema, SchemaFilterContext context)
    {
        if (schema.DoesNotMatchTitle(nameof(CompanyPresentation)))
        {
            return;
        }

        _ = schema
            .AddSelfLink(context)
            .AddLink("get_owner", context)
            .AddLink("get_members", context)
            .AddLink("add_member", context)
            .AddLink("get_parent", context)
            .AddLink("get_children", context);
    }
}