using System.Collections.Concurrent;
using GB.AccessManagement.Domain;
using GB.AccessManagement.Domain.Companies.Messages;
using GB.AccessManagement.Domain.Companies.Providers;
using GB.AccessManagement.Domain.Services;
using GB.AccessManagement.Domain.ValueObjects;
using MassTransit;
using Microsoft.Extensions.Options;

namespace GB.AccessManagement.Infrastructure.MassTransit.Consumers;

public sealed class CompanyDeletedMessageConsumer(
    IOptionsMonitor<MessageQueueOptions> options,
    IProvideCompanyOwnerId ownerIdProvider,
    IProvideCompanyParentId parentIdProvider,
    IProvideCompanyChildrenIds childrenIdProvider,
    IProvideCompanyMemberIds membersIdProvider,
    IDeleteCompanyAccesses removeAccesses)
    : AbstractConsumer<ICompanyDeletedMessage>(options), ISelfTransientService
{
    public override async Task Consume(ConsumeContext<ICompanyDeletedMessage> context)
    {
        if (ContextIsNotAuthenticated(context))
        {
            return;
        }

        var companyId = context.Message.CompanyId;
        var accesses = new ConcurrentBag<Access>();

        Task[] tasks =
        [
            ownerIdProvider.GetOwnerForCompany(companyId).ContinueWith(AddOwnerAccess(accesses, companyId.ToString())),
            parentIdProvider.FindParentForCompany(companyId).ContinueWith(AddParentAccess(accesses, companyId.ToString())),
            childrenIdProvider.ForCompany(companyId).ContinueWith(AddChildAccesses(accesses, companyId.ToString())),
            membersIdProvider.ListMembersForCompany(companyId).ContinueWith(AddMemberAccesses(accesses, companyId.ToString()))
        ];
        await Task.WhenAll(tasks);

        await removeAccesses.AsRequested(accesses.ToArray());
    }

    private static Action<Task<UserId>> AddOwnerAccess(ConcurrentBag<Access> accesses, string companyId)
    {
        return task =>
        {
            var access = new Access("user", task.Result, "owner", "company", companyId);
            accesses.Add(access);
        };
    }

    private static Action<Task<CompanyId?>> AddParentAccess(ConcurrentBag<Access> accesses, string companyId)
    {
        return task =>
        {
            var parentCompanyId = task.Result?.ToString();

            if (string.IsNullOrEmpty(parentCompanyId))
            {
                return;
            }

            var access = new Access("company", parentCompanyId, "parent", "company", companyId);
            accesses.Add(access);
        };
    }

    private static Action<Task<CompanyId[]>> AddChildAccesses(ConcurrentBag<Access> accesses, string companyId)
    {
        return task =>
        {
            var childrenIds = task.Result;

            if (childrenIds.Length == 0)
            {
                return;
            }

            var childrenAccesses = childrenIds.Select(id => new Access("company", companyId, "parent", "company", id.ToString()));

            foreach (var childAccess in childrenAccesses)
            {
                accesses.Add(childAccess);
            }
        };
    }

    private static Action<Task<UserId[]>> AddMemberAccesses(ConcurrentBag<Access> accesses, string companyId)
    {
        return task =>
        {
            var memberIds = task.Result;

            if (memberIds.Length == 0)
            {
                return;
            }

            var memberAccesses = memberIds.Select(id => new Access("user", id.ToString(), "member", "company", companyId));

            foreach (var memberAccess in memberAccesses)
            {
                accesses.Add(memberAccess);
            }
        };
    }
}