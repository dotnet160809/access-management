using GB.AccessManagement.Domain.Companies.Events;
using GB.AccessManagement.Domain.Companies.Policies;
using GB.AccessManagement.Domain.Services;
using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Domain.Companies;

public sealed partial class CompanyAggregate
{
    public sealed class CompanyCreator : ICreateCompany, ITransientService
    {
        private readonly ICompanyExistPolicy _companyExistPolicy;
        private readonly ICompanyOwnerPolicy _companyOwnerPolicy;

        public CompanyCreator(ICompanyExistPolicy companyExistPolicy, ICompanyOwnerPolicy companyOwnerPolicy)
        {
            _companyExistPolicy = companyExistPolicy;
            _companyOwnerPolicy = companyOwnerPolicy;
        }

        public async Task<CompanyAggregate> Create(CompanyName name, UserId ownerId, CompanyId? parentCompanyId)
        {
            if (parentCompanyId is not null)
            {
                await _companyExistPolicy.EnsureCompanyExists(parentCompanyId);
                await _companyOwnerPolicy.EnsureUserIsCompanyOwner(ownerId, parentCompanyId);
            }
            
            var aggregate = new CompanyAggregate(Guid.NewGuid(), name, ownerId, parentCompanyId);
            aggregate.StoreEvent(new CompanyCreatedEvent(aggregate.Id, aggregate._name, aggregate._ownerId, aggregate._parentCompanyId));

            return aggregate;
        }
    }
}