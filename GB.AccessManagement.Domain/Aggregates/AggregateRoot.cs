using GB.AccessManagement.Domain.Aggregates.Memos;
using GB.AccessManagement.Domain.Events;

namespace GB.AccessManagement.Domain.Aggregates;

public abstract class AggregateRoot<TAggregate, TAggregateId, TMemo>
    where TAggregate : AggregateRoot<TAggregate, TAggregateId, TMemo>
    where TAggregateId : notnull
    where TMemo : IAggregateMemo
{
    private readonly HashSet<DomainEvent> _storedEvents = new();

    public TAggregateId Id { get; protected set; } = default!;

    public DomainEvent[] UncommittedEvents => _storedEvents.Where(@event => !@event.HasBeenCommitted).ToArray();
    
    public void StoreEvent(DomainEvent domainEvent)
    {
        _storedEvents.Add(domainEvent);
    }

    public void Save<TEvent>(TEvent @event, TMemo memo) where TEvent : DomainEvent
    {
        if (this is not IEventApplierAggregate<TEvent, TMemo> eventApplier)
        {
            throw new MissingMethodException(
                nameof(IEventApplierAggregate<TEvent, TMemo>),
                nameof(IEventApplierAggregate<TEvent, TMemo>.Apply));
        }
            
        eventApplier.Apply(@event, memo);
    }
}