using GB.AccessManagement.Domain.ValueObjects;

namespace GB.AccessManagement.Domain.Companies.Policies;

public interface ICompanyExistPolicy
{
    Task EnsureCompanyExists(CompanyId id);
}