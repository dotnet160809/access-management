using System.Net;
using GB.AccessManagement.Domain.Companies.Messages;
using GB.AccessManagement.Infrastructure.MassTransit;
using GB.AccessManagement.Tests.Assertions;
using GB.HttpRequestMessagesStub;
using MassTransit.Testing;
using Microsoft.Net.Http.Headers;
using NFluent;
using Xunit.Abstractions;

namespace GB.AccessManagement.Tests.Endpoints.AddCompanyMember;

public sealed class AddCompanyMemberEndpointShould : AbstractFeatureShould<AddCompanyMemberScenarioBuilder>
{
    public AddCompanyMemberEndpointShould(ITestOutputHelper outputHelper, AddCompanyMemberScenarioBuilder builder)
        : base(outputHelper, builder)
    {
    }

    [Theory]
    [InlineData(1)]
    public async Task ReturnUnauthorizedWhenNoAuthenticationIsProvided(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var company)
            .CreateUser(out var user)
            .Build(apiVersion);

        // Act
        var response = await scenario.Execute(company.Id, user.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.Unauthorized);
    }

    [Theory]
    [InlineData(1, false)]
    [InlineData(1, true)]
    public async Task ReturnForbiddenWhenUserIsNotCompanyOwner(int apiVersion, bool addUserToCompany)
    {
        // Arrange
        _ = ScenarioBuilder
            .CreateCompany(out var company)
            .CreateUser(out var user)
            .CreateUser(out var other);

        if (addUserToCompany)
        {
            _ = ScenarioBuilder.AddCompanyMember(company.Id, user.Id);
        }

        var scenario = ScenarioBuilder
            .AuthenticateUser(user)
            .Build(apiVersion);

        // Act
        var response = await scenario.Execute(company.Id, other.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.Forbidden);
    }

    [Theory]
    [InlineData(1, null)]
    [InlineData(1, "")]
    [InlineData(1, " ")]
    [InlineData(1, "not-a-valid-member-id")]
    public async Task ReturnBadRequestWhenProvidingInvalidMemberId(int apiVersion, string? memberId)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var company)
            .CreateUser(out var owner)
            .AddCompanyOwner(company.Id, owner.Id)
            .AuthenticateUser(owner)
            .Build(apiVersion);

        // Act
        var response = await scenario.Execute(company.Id, memberId);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.BadRequest);
    }

    [Theory]
    [InlineData(1)]
    public async Task ReturnUnprocessableEntityWhenUserAlreadyIsCompanyMember(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var company)
            .CreateUser(out var owner)
            .AddCompanyOwner(company.Id, owner.Id)
            .CreateUser(out var member)
            .AddCompanyMember(company.Id, member.Id)
            .AuthenticateUser(owner)
            .Build(apiVersion);

        // Act
        var response = await scenario.Execute(company.Id, member.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.UnprocessableEntity);
    }

    [Theory]
    [InlineData(1)]
    public async Task ReturnCreatedWhenUserHasBeenSuccessfullyAdded(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var company)
            .CreateUser(out var owner)
            .AddCompanyOwner(company.Id, owner.Id)
            .CreateUser(out var member)
            .AuthenticateUser(owner)
            .Build(apiVersion);

        // Act
        var response = await scenario.Execute(company.Id, member.Id);

        // Assert
        Check.That(response).HasStatusCode(HttpStatusCode.Created);
        Check.That(response).HasHeaderValue(HeaderNames.Location, $"/v{apiVersion}/companies/{company.Id}/members");
    }

    [Theory]
    [InlineData(1)]
    public async Task PublishAuthenticatedMessage(int apiVersion)
    {
        // Arrange
        var scenario = ScenarioBuilder
            .CreateCompany(out var company)
            .CreateUser(out var owner)
            .AddCompanyOwner(company.Id, owner.Id)
            .CreateUser(out var member)
            .AuthenticateUser(owner)
            .Build(apiVersion);
        
        // Act
        _ = await scenario.Execute(company.Id, member.Id);

        // Assert
        var options = Options<MessageQueueOptions>();
        Check.That(PublishedMessages()).HasAuthenticatedMessage(ForCompanyMember(company.Id, member.Id), options);
    }

    private static Predicate<IPublishedMessage<ICompanyMemberAddedMessage>> ForCompanyMember(Guid companyId, Guid memberId)
    {
        return message => message.MessageObject is ICompanyMemberAddedMessage messageObject
                                 && messageObject.CompanyId == companyId
                                 && messageObject.MemberId.OrdinalEquals(memberId.ToString());
    }
}