using Swashbuckle.AspNetCore.Annotations;

namespace GB.AccessManagement.Application.Presentations;

[Serializable]
[SwaggerSchema(Title = nameof(CompanyPresentation))]
public sealed record CompanyPresentation(Guid Id, string Name, Guid? ParentCompanyId) : HalPresentation<CompanyPresentation>;