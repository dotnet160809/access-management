using FluentMigrator;

namespace GB.AccessManagement.Infrastructure.Database.Migrations;

[TimestampedMigration(2024, 3, 11, 18, 15)]
public sealed class AddCompanyOwnerColumnMigration : ForwardOnlyMigration
{
    public override void Up()
    {
        Alter
            .Table(CompanyDao.TableName)
            .AddColumn(CompanyDao.OwnerIdColumnName)
            .AsGuid()
            .Nullable();
    }
}